﻿#define BRANDING

using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace JeffreyPlease
{

#if BRANDING

    class Branding
    {
        public Branding(JeffreysClub club)
        {
            Store.ObjectAdded += Store_ObjectAdded;
        }

        void Store_ObjectAdded(object objectAdded)
        {
            if (objectAdded is Settings.SettingsManager)
            {
                Settings.SettingsManager sm = (Settings.SettingsManager)objectAdded;
                sm.Initialize += sm_Initialize;
            }
            else if (objectAdded is UI.ThemeManager)
            {
                UI.ThemeManager tm = (UI.ThemeManager)objectAdded;
                tm.AddTheme(JSONdotNET.JSON.Parse("{\"name\":\"Flat Bosch\",\"description\":\"A simple Bosch theme based on the flat design\",\"author\":\"Patrick Schmidt\"\"width\":550,\"background\":\"#003B6A\",\"paddingColor\":\"#003B6A\",\"padding\":5,\"searchPadding\":5,\"resultTopMargin\":5,\"resultIconMargin\":5,\"opacity\":1,\"searchFontFamily\":\"Verdana Regular\",\"resultFontFamily\":\"Verdana Regular\",\"resultSubFontFamily\":\"Verdana Regular\",\"shortcutFontFamily\":\"Verdana Regular\",\"selectedResultFontFamily\":\"Verdana Regular\",\"selectedResultSubFontFamily\":\"Verdana Regular\",\"selectedShortcutFontFamily\":\"Verdana Regular\",\"scrollbarColor\":\"#0A71B4\",\"scrollbarPaddingRight\":0,\"resultBackground\":\"#003B6A\",\"searchBackground\":\"white\",\"borderColorTop\":\"#003B6A\",\"borderColorBottom\":\"#003B6A\",\"selectedResultTextColor\":[255],\"selectedResultBackground\":\"#003B6A\",\"selectedResultSubtextColor\":\"#A2BAD2\",\"shortcutColor\":\"#A2BAD2\",\"selectedShortcutColor\":\"white\",\"resultTextColor\":\"#A2BAD2\",\"resultSubtextColor\":\"#A2BAD2\",\"cornerRoundness\":0,\"innerCornerRoundness\":0 }"));

                Store.ObjectAdded -= Store_ObjectAdded;
            }
        }

        void sm_Initialize(Settings.SettingsManager instance, JSONdotNET.JSONObject initialSettings)
        {
            Store.Get<Settings.SettingsManager>().MigrateJSON(
                (new JSONdotNET.JSONObject()).Add("repositories",
                    (new JSONdotNET.JSONArray()).Add(
                        (new JSONdotNET.JSONObject()).Add("name", "bosch_web_beta").Add("url", "http://abtz0816.abt.de.bosch.com/jeffrey-plugin-channel.json")
                    )
                ).Add("theme", "Flat Bosch"));
        }
    }

#endif

    /// <summary>
    /// Nothing but the static class containing the entry point......
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// Version number of the program
        /// This is useful for plugin comparisons
        /// </summary>
        public static int VERSION = 2200;

        /// <summary>Instance of JeffreysClub which holds the TrayIcon and manages to launch and focus "Jeffrey"</summary>
        private static JeffreysClub club;

        [STAThread]
        public static void Main()
        {
            // Check if there is already an other instance of JeffreyPlease running
            if (Process.GetProcessesByName(Path.GetFileNameWithoutExtension(Assembly.GetEntryAssembly().Location)).Count() > 1)
            {
                MessageBox.Show("Jeffrey is already serving you, try pressing Alt + Space.");
                Process.GetCurrentProcess().Kill();
                return;
            }

            // Create all related UI stuff and run the application
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Program.club = new JeffreysClub();
            JeffreyLauncher.Register(Program.club);

#if BRANDING
            new Branding(Program.club);
#endif

            Program.club.Start();
            Application.ThreadException += Application_ThreadException;
            Application.Run();
            Program.club.Stop();
        }

        private static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            try
            {
                Program.club.Stop();
            }
            catch (Exception) { /* Unhandled */ }
        }
    }
}

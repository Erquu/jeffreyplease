﻿using JeffreyPlease.Settings;
using System;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace JeffreyPlease.Utils
{
    public static class Tools
    {
        public const int SW_SHOW = 5;
        public const int CS_DROPSHADOW = 0x20000;

        private const int SW_HIDE = 0;
        private const int SW_SHOWNORMAL = 1;
        private const int SW_NORMAL = 1;
        private const int SW_SHOWMINIMIZED = 2;
        private const int SW_SHOWMAXIMIZED = 3;
        private const int SW_MAXIMIZE = 3;
        private const int SW_SHOWNOACTIVATE = 4;
        private const int SW_MINIMIZE = 6;
        private const int SW_SHOWMINNOACTIVE = 7;
        private const int SW_SHOWNA = 8;
        private const int SW_RESTORE = 9;
        private const int SW_SHOWDEFAULT = 10;
        private const int SW_MAX = 10;

        private const uint SPI_GETFOREGROUNDLOCKTIMEOUT = 0x2000;
        private const uint SPI_SETFOREGROUNDLOCKTIMEOUT = 0x2001;
        private const int SPIF_SENDCHANGE = 0x2;


        [DllImport("user32.dll")]
        private static extern bool GetCursorPos(ref Point lpPoint);

        [DllImport("user32.dll")]
        public static extern void SwitchToThisWindow(IntPtr hWnd, bool t);
        [DllImport("user32.dll")]
        public static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        [DllImport("user32.dll")]
        private static extern bool SetForegroundWindow(IntPtr hWnd);
        [DllImport("user32.dll")]
        private static extern bool ShowWindowAsync(IntPtr hWnd, int nCmdShow);
        [DllImport("user32.dll")]
        private static extern bool IsIconic(IntPtr hWnd);
        [DllImport("user32.dll", SetLastError = true)]
        private static extern bool SystemParametersInfo(uint uiAction, uint uiParam, IntPtr pvParam, uint fWinIni);
        [DllImport("user32.dll", SetLastError = true)]
        private static extern uint GetWindowThreadProcessId(IntPtr hWnd, IntPtr lpdwProcessId);
        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();
        [DllImport("user32.dll")]
        private static extern bool AttachThreadInput(uint idAttach, uint idAttachTo, bool fAttach);
        [DllImport("user32.dll")]
        static extern bool BringWindowToTop(IntPtr hWnd);

        [DllImport("user32.dll")] 
        private static extern int GetWindowText(IntPtr hWnd, StringBuilder lpString, Int32 nMaxCount);
        [DllImport("user32.dll")]
        private static extern int GetWindowThreadProcessId(IntPtr hWnd, ref Int32 lpdwProcessId);
        [DllImport("user32.dll")]
        public static extern IntPtr GetParent(IntPtr hWnd);

        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        public static extern IntPtr CreateRoundRectRgn(int nLeftRect, int nTopRect, int nRightRect, int nBottomRect, int nWidthEllipse, int nHeightEllipse);

        /// <summary>
        /// Use this to enforce focus of the window with the given handle
        /// </summary>
        /// <param name="hWnd">The window handle</param>
        public static void Focus(IntPtr hWnd)
        {
            if (IsIconic(hWnd))
                ShowWindowAsync(hWnd, SW_RESTORE);
            else
                ShowWindowAsync(hWnd, SW_SHOW);

            SetForegroundWindow(hWnd);

            IntPtr foregroundWindow = GetForegroundWindow();
            IntPtr Dummy = IntPtr.Zero;

            uint foregroundThreadId = GetWindowThreadProcessId(foregroundWindow, Dummy);
            uint thisThreadId = GetWindowThreadProcessId(hWnd, Dummy);

            if (AttachThreadInput(thisThreadId, foregroundThreadId, true))
            {
                BringWindowToTop(hWnd);
                SetForegroundWindow(hWnd);
                AttachThreadInput(thisThreadId, foregroundThreadId, false);
            }

            if (GetForegroundWindow() != hWnd)
            {
                IntPtr Timeout = IntPtr.Zero;
                SystemParametersInfo(SPI_GETFOREGROUNDLOCKTIMEOUT, 0, Timeout, 0);
                SystemParametersInfo(SPI_SETFOREGROUNDLOCKTIMEOUT, 0, Dummy, SPIF_SENDCHANGE);
                BringWindowToTop(hWnd);
                SetForegroundWindow(hWnd);
                SystemParametersInfo(SPI_SETFOREGROUNDLOCKTIMEOUT, 0, Timeout, SPIF_SENDCHANGE);
            }
        }

        /// <summary>
        /// Checks if the given path is a directory
        /// </summary>
        /// <param name="path">The path of the directory to be checked</param>
        /// <returns>true if the path is a directory and it exists</returns>
        public static bool IsDirectory(string path)
        {
            try
            {
                FileAttributes attr = File.GetAttributes(path);
                return ((attr & FileAttributes.Directory) == FileAttributes.Directory);
            }
            catch (Exception) { /* UNHANDLED */ }
            return false;
        }

        /// <summary>
        /// Resolves a windows link (.lnk) file to it's target
        /// </summary>
        /// <param name="path">The path to the link file</param>
        /// <returns>The path to the links target or null in case of an error</returns>
        public static string ResolveShortcut(string path)
        {
            if (File.Exists(path))
            {
                IWshRuntimeLibrary.IWshShell shell = new IWshRuntimeLibrary.IWshShell_Class();

                try
                {
                    IWshRuntimeLibrary.IWshShortcut lnk = (IWshRuntimeLibrary.IWshShortcut)shell.CreateShortcut(path);
                    return lnk.TargetPath;
                }
                catch (COMException ex)
                {
                    if (ex.ErrorCode != -2147352567)
                    {
                        Store.Get<ErrorLog>().HandleError(ex);
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Extracts the icon from whatever file you pass in
        /// </summary>
        /// <param name="path">The path of the file to extract the icon from</param>
        /// <returns>The icon or null in case of an error</returns>
        public static Icon ExtractIcon(string path)
        {
            try
            {
                if (".lnk".Equals(Path.GetExtension(path), StringComparison.InvariantCultureIgnoreCase))
                {
                    return JeffreyPlease.Utils.ExtractIcon.Extract(new FileInfo(path));
                }
                else
                {
                    return Icon.ExtractAssociatedIcon(path);
                }
            }
            catch (ArgumentException) { /* UNHANDLED */ }
            catch (Exception ex) { Store.Get<ErrorLog>().HandleError(ex); }
            return null;
        }

        /// <summary>
        /// Calculates the SHA1 of a file
        /// </summary>
        /// <param name="path">The path to the file</param>
        /// <returns>The SHA1-Hash for the file</returns>
        public static string FileSHA1(string path)
        {
            string result = null;

            Stream fileStream = Store.Get<SettingsManager>().OpenRead(path);
            result = FileSHA1(fileStream);
            fileStream.Close();

            return result;
        }
        /// <summary>
        /// Calculates the SHA1 of a stream
        /// </summary>
        /// <param name="stream">The stream to use to create the hash</param>
        /// <returns>The SHA1-Hash</returns>
        public static string FileSHA1(Stream stream)
        {
            SHA1CryptoServiceProvider SHA1 = new SHA1CryptoServiceProvider();
            byte[] hash;
            string result = String.Empty;
            string tmp;

            if (stream != null)
            {
                hash = SHA1.ComputeHash(stream);
                for (int i = 0; i < hash.Length; i++)
                {
                    tmp = Convert.ToString(hash[i], 16);
                    if (tmp.Length == 1)
                        tmp = "0" + tmp;
                    result += tmp;
                }
            }
            return result;
        }
        /// <summary>
        /// Calculates the SHA1 of a stream
        /// </summary>
        /// <param name="stream">The stream to use to create the hash</param>
        /// <param name="offset">The offset of the stream to start reading at</param>
        /// <param name="count">The length of bytes to read</param>
        /// <returns>The SHA1-Hash</returns>
        public static string FileSHA1(Stream stream, long offset, long count)
        {
            SHA1CryptoServiceProvider SHA1 = new SHA1CryptoServiceProvider();
            byte[] hash;
            string result = String.Empty;
            string tmp;

            long currentOffset = stream.Position;
            stream.Seek(offset, SeekOrigin.Begin);

            byte[] buffer = new byte[count - offset];
            int length = stream.Read(buffer, 0, buffer.Length);

            stream.Seek(currentOffset, SeekOrigin.Begin);

            if (stream != null)
            {
                hash = SHA1.ComputeHash(buffer, 0, length);
                for (int i = 0; i < hash.Length; i++)
                {
                    tmp = Convert.ToString(hash[i], 16);
                    if (tmp.Length == 1)
                        tmp = "0" + tmp;
                    result += tmp;
                }
            }
            return result;
        }

        /// <summary>
        /// Returns the global mouse position
        /// </summary>
        /// <returns>The global mouse position</returns>
        public static Point GetMousePosition()
        {
            Point position = new Point();
            GetCursorPos(ref position);

            return position;
        }

        public static bool CheckVersion(string versionComparator)
        {
            if ("*".Equals(versionComparator))
            {
                return true;
            }

            Regex tokenRegex = new Regex(@"^([<>!=]{1,2})(\d+)", RegexOptions.IgnoreCase);
            Match m = tokenRegex.Match(versionComparator);

            if (m.Groups.Count == 3)
            {
                string op = m.Groups[1].Value;
                // The regex makes sure that it is a valid integer value, so no checks nescessary
                int version = Convert.ToInt32(m.Groups[2].Value);

                int jeffrey = Program.VERSION;

                switch (op)
                {
                    case "<":
                        return (jeffrey < version);
                    case ">":
                        return (jeffrey > version);
                    case "<=":
                        return (jeffrey <= version);
                    case ">=":
                        return (jeffrey >= version);
                    case "=":
                        return (jeffrey == version);
                }
            }

            return false;
        }
    }
}

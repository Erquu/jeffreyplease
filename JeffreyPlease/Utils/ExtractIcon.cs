﻿using System;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;

namespace JeffreyPlease.Utils
{
    internal static class ExtractIcon
    {
        private struct SHFILEINFO
        {
            public IntPtr hIcon;
            public IntPtr iIcon;
            public uint dwAttributes;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)]
            public string szDisplayName;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 80)]
            public string szTypeName;
        }

        [DllImport("shell32.dll")]
        private static extern IntPtr SHGetFileInfo(string pszPath, uint dwFileAttributes, ref SHFILEINFO psfi, uint cbSizeFileInfo, uint uFlags);
        [DllImport("Comctl32.dll")]
        private static extern IntPtr ImageList_GetIcon(IntPtr himl, IntPtr i, uint flags);
        [DllImport("user32.dll")]
        private static extern int DestroyIcon(IntPtr hIcon);

        private const uint SHGFI_LARGEICON = 0x0;
        private const uint SHGFI_SMALLICON = 0x1;

        private const uint SHGFI_ICON = 0x100;
        private const uint SHGFI_SYSICONINDEX = 0x4000;

        private const uint ILD_NORMAL = 0x0;

        public static Icon Extract(FileInfo iconFile)
        {
            if (iconFile.Exists) {
                SHFILEINFO iconFileInfo = new SHFILEINFO();
                IntPtr imageList = SHGetFileInfo(
                    iconFile.FullName,
                    0,
                    ref iconFileInfo,
                    (uint)Marshal.SizeOf(iconFileInfo),
                    SHGFI_ICON | SHGFI_SYSICONINDEX);

                IntPtr t = ImageList_GetIcon(imageList, iconFileInfo.iIcon, ILD_NORMAL);
                DestroyIcon(iconFileInfo.hIcon);
                DestroyIcon(iconFileInfo.iIcon);

                Icon icon = new Icon((Icon)Icon.FromHandle(t), new Size(64, 64));
                DestroyIcon(t);

                return icon;
            }
            return null;
        }
    }
}

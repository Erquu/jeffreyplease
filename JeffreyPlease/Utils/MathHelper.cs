﻿
namespace JeffreyPlease.Utils
{
    /// <summary>
    /// Additional Math functions the world couldn't live without
    /// </summary>
    public static class MathHelper
    {
        /// <summary>
        /// Checks if a given value is between an inclusive minimum and an exclusive maximum range
        /// </summary>
        /// <param name="min">The inclusive minimum value</param>
        /// <param name="value">The value to be checked</param>
        /// <param name="exclusiveMax">The exclusive maximum value</param>
        /// <returns>True if the value is between min and max</returns>
        public static bool Between(int min, int value, int exclusiveMax)
        {
            return (value >= min && value < exclusiveMax);
        }

        /// <summary>
        /// Clamps a given value to a given range
        /// </summary>
        /// <param name="min">The lowest possible value</param>
        /// <param name="value">The value to be checked</param>
        /// <param name="exclusiveMax">The exlusive highes possible value</param>
        /// <returns>An integer, clamped to the given range</returns>
        public static int Clamp(int min, int value, int exclusiveMax)
        {
            return (value < min ? min : (value >= exclusiveMax ? exclusiveMax - 1 : value));
        }
        /// <summary>
        /// Clamps a given value to a given range
        /// </summary>
        /// <param name="min">The lowest possible value</param>
        /// <param name="value">The value to be checked</param>
        /// <param name="exclusiveMax">The exlusive highes possible value</param>
        /// <returns>A float, clamped to the given range</returns>
        public static float Clamp(float min, float value, float exclusiveMax)
        {
            return (value < min ? min : (value >= exclusiveMax ? exclusiveMax - 1.0f : value));
        }
        /// <summary>
        /// Clamps a given value to a given range
        /// </summary>
        /// <param name="min">The lowest possible value</param>
        /// <param name="value">The value to be checked</param>
        /// <param name="exclusiveMax">The exlusive highes possible value</param>
        /// <returns>An double, clamped to the given range</returns>
        public static double Clamp(double min, double value, double exclusiveMax)
        {
            return (value < min ? min : (value >= exclusiveMax ? exclusiveMax - 1.0d : value));
        }

        /// <summary>
        /// Clamps a given value to a given range
        /// </summary>
        /// <param name="min">The lowest possible value</param>
        /// <param name="value">The value to be checked</param>
        /// <param name="exclusiveMax">The inclusive highes possible value</param>
        /// <returns>An double, clamped to the given range</returns>
        public static double InclusiveClamp(double min, double value, double max)
        {
            return (value < min ? min : (value > max ? max : value));
        }
    }
}

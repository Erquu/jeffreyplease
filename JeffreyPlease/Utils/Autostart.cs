﻿using Microsoft.Win32;
using System.Windows.Forms;

namespace JeffreyPlease.Utils
{
    public static class Autostart
    {
        public static bool IsAutoStarting
        {
            get
            {
                RegistryKey key = Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Run");
                bool autostarted = (key.GetValue("JeffreyPlease") != null);
                key.Close();
                return autostarted;
            }
        }

        public static void EnableAutostart(bool enable)
        {
            RegistryKey key = Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Run", true);

            if (enable)
            {
                if (!IsAutoStarting)
                    key.SetValue("JeffreyPlease", Application.ExecutablePath, RegistryValueKind.String);
            }
            else
            {
                if (IsAutoStarting)
                    key.DeleteValue("JeffreyPlease");
            }
        }
    }
}

﻿using JeffreyPlease.Settings;
using JSONdotNET;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Utilities;

namespace JeffreyPlease.Utils
{
    /// <summary>
    /// Helper class for checking key sequences
    /// </summary>
    public class KeySequence
    {
        /// <summary>The key sequence which will be used</summary>
        internal Keys[] keySequence;
        /// <summary>The current status of the sequence</summary>
        private bool[] currentSeq;

        /// <summary>
        /// <para>An instance of the global keyboard hook</para>
        /// It is required for later changes on the sequence
        /// </summary>
        private GlobalKeyboardHook hook;

        /// <summary>...</summary>
        /// <param name="hook">Instance of GlobalKeyboardook</param>
        public KeySequence(GlobalKeyboardHook hook)
        {
            this.hook = hook;

            // Default Key Sequence
            keySequence = new Keys[] { (Keys)164, Keys.Space };
            UpdateSequence();
        }
        /// <summary>...</summary>
        /// <param name="hook">Instance of GlobalKeyboardook</param>
        /// <param name="settings">Instance of a SettingsManager if any settings are stored and need to be loaded</param>
        public KeySequence(GlobalKeyboardHook hook, SettingsManager settings) : this(hook)
        {
            JSONObject settingsObject = settings.SettingsObject;
            if (settingsObject.ContainsKey("openingSequence"))
            {
                JSONObject seqObj = settingsObject.Get("openingSequence");
                if (seqObj is JSONArray)
                {
                    List<Keys> keyList = new List<Keys>();

                    JSONArray sequence = (JSONArray)seqObj;
                    for (int i = 0; i < sequence.Length; i++)
                    {
                        string keyString = sequence[i].ToString();
                        Keys key = default(Keys);
                        bool set = false;

                        if ("alt".Equals(keyString.ToLower()))
                        {
                            set = true;
                            key = (Keys)164;
                        }
                        else
                        {
                            object keyObject = Enum.Parse(typeof(Keys), keyString);
                            if (keyObject != null && keyObject is Keys)
                            {
                                set = true;
                                key = (Keys)keyObject;
                            }
                        }
                        if (set)
                            keyList.Add(key);
                    }

                    keySequence = keyList.ToArray();
                    UpdateSequence();
                }
            }
        }

        /// <summary>
        /// <para>Should be called after changing the <code>keySequence</code></para>
        /// Updated the keyboard hook and the status sequnce
        /// </summary>
        private void UpdateSequence()
        {
            currentSeq = new bool[keySequence.Length];
            Array.Sort(keySequence);

            foreach (Keys key in keySequence)
            {
                hook.HookedKeys.Add(key);
            }
        }

        /// <summary>
        /// Register the given key in the sequence
        /// </summary>
        /// <param name="key">The key to be registerd</param>
        /// <returns>True if the sequence is complete</returns>
        public bool KeyDown(Keys key)
        {
            int index = Array.BinarySearch(keySequence, key);

            if (index >= 0)
                currentSeq[index] = true;

            bool fire = true;
            for (int i = 0; i < currentSeq.Length; i++)
            {
                if (!currentSeq[i])
                {
                    fire = false;
                    break;
                }
            }

            return fire;
        }

        /// <summary>
        /// Unregisters the given key from the sequence
        /// </summary>
        /// <param name="key">The key to be unregistered</param>
        public void KeyUp(Keys key)
        {
            int index = Array.BinarySearch(keySequence, key);
            if (index >= 0)
                currentSeq[index] = false;
        }
    }
}

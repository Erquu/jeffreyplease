﻿
namespace JeffreyPlease
{
    /// <summary>
    /// <para>The JeffreyLauncher provides static access</para>
    /// to the open and close functions
    /// </summary>
    public static class JeffreyLauncher
    {
        private delegate void VoidCallback();

        private static VoidCallback Open;
        private static VoidCallback Close;

        /// <summary>
        /// Registers a JeffreysClub instance for static access
        /// </summary>
        /// <param name="jc">The JeffreysClub instance to register</param>
        internal static void Register(JeffreysClub jc)
        {
            Open = jc.CallJeffrey;
            Close = jc.HideJeffrey;
        }

        /// <summary>
        /// Opens the registered JeffreysClub window
        /// </summary>
        public static void Call()
        {
            Open();
        }

        /// <summary>
        /// Hides the registered JeffreysClub window
        /// </summary>
        public static void Hide()
        {
            Close();
        }
    }
}

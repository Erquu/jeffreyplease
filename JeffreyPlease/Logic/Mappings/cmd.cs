﻿
namespace JeffreyPlease.Logic.Mappings
{
    /// <summary>
    /// AppMapping for files with the extension ".cmd"
    /// </summary>
    public class cmd : exe
    {
        public override string Extension { get { return "cmd"; } }
    }
}

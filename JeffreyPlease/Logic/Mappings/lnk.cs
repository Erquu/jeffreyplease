﻿using JeffreyPlease.Logic.Apps;
using System.IO;

namespace JeffreyPlease.Logic.Mappings
{
    /// <summary>
    /// AppMapping for files with the extension ".lnk" (Shortcuts)
    /// </summary>
    public class lnk : AppMapping
    {
        public override string Extension
        {
            get { return "lnk"; }
        }

        public override App GetApp(FileInfo file)
        {
            return new ShortcutExecutable(file);
        }
    }
}

﻿
namespace JeffreyPlease.Logic.Mappings
{
    /// <summary>
    /// AppMapping for files with the extension ".bat"
    /// </summary>
    public class bat : exe
    {
        public override string Extension { get { return "bat"; } }
    }
}

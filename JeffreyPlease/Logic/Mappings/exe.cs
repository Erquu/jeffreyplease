﻿using JeffreyPlease.Logic.Apps;

namespace JeffreyPlease.Logic.Mappings
{
    /// <summary>
    /// AppMapping for files with the extension ".exe"
    /// </summary>
    public class exe : AppMapping
    {
        public override string Extension {  get { return "exe"; } }

        public override App GetApp(System.IO.FileInfo file)
        {
            return new DefaultExecutable(file);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading;
using JeffreyPlease.Plugin;
using JeffreyPlease.Settings;

namespace JeffreyPlease.Logic.Loaders
{
    /// <summary>
    /// Class for loading Plugins
    /// </summary>
    public class PluginLoader
    {
        /// <summary>The plugin type to be loaded</summary>
        private Type extensionType = typeof(JeffreyExtension);
        /// <summary>List of already loaded plugins</summary>
        private List<string> filesLoaded = new List<string>();

        /// <summary>Starts the loading of all (not yet loaded) plugins</summary>
        public void Load()
        {
            Thread loadThread = new Thread(new ThreadStart(LoadPlugins));
            loadThread.Name = "PluginLoader Thread";
            loadThread.Start();
        }
        /// <summary>Loads a single Plugin (if not yet loaded)</summary>
        /// <param name="plugin">The FileInfo of the plugin</param>
        /// <returns>The instance of the plugin mainclass</returns>
        public JeffreyExtension Load(FileInfo plugin)
        {
            JeffreyExtension ext = null;

            if (filesLoaded.BinarySearch(plugin.Name) < 0)
            {
                try
                {
                    Assembly assembly = Assembly.LoadFile(plugin.FullName);
                    foreach (Type type in assembly.GetTypes())
                    {
                        if (type.IsPublic && !type.IsAbstract && !type.IsInterface)
                        {
                            Type typeInterface = type.BaseType;

                            if (typeInterface != null && extensionType.FullName.Equals(typeInterface.FullName))
                            {
                                try
                                {
                                    ext = (JeffreyExtension)Activator.CreateInstance(type);
                                    filesLoaded.Add(plugin.Name);
                                    Debug.WriteLine("Plugin Loader: " + ext);
                                }
                                catch (Exception ex) { Store.Get<ErrorLog>().HandleError(ex); }
                            }
                        }
                    }
                }
                catch (Exception ex) { Store.Get<ErrorLog>().HandleError(ex); }
            }

            return ext;
        }

        /// <summary>
        /// Searches and loads all plugins from the plugin folder
        /// </summary>
        private void LoadPlugins()
        {
            SettingsManager settings = Store.Get<SettingsManager>();
            DirectoryInfo pluginDir = settings.PluginDir;

            if (pluginDir.Exists)
            {
                foreach (FileInfo plugin in pluginDir.GetFiles("*.dll"))
                {
                    Load(plugin);
                }
            }
        }
    }
}

﻿using JeffreyPlease.Logic.Apps.Static;
using JeffreyPlease.Settings;
using JSONdotNET;
using System;
using System.IO;
using System.Threading;

namespace JeffreyPlease.Logic.Loaders
{
    /// <summary>
    /// Class for loading apps
    /// </summary>
    public class AppLoader
    {
        /// <summary>The main search context</summary>
        private AppStore store;
        /// <summary>The app loader config</summary>
        private AppLoaderConfig config;

        public AppLoader(AppStore store) : this(store, AppLoaderConfig.DEFAULT) { }
        public AppLoader(AppStore store, AppLoaderConfig config)
        {
            this.store = store;
            this.config = config;

            this.store.StoreCleared += new StoreClearedCallback(LoadCustomApps);
        }

        /// <summary>Adds an app to the store</summary>
        /// <param name="app">The app to be added to the search context</param>
        public void Add(App app)
        {
            this.store.Add(app);
        }

        /// <summary>
        /// <para>Recreate the index</para>
        /// This causes the deletion of all found apps and starts a new search for apps
        /// </summary>
        public void ReIndex()
        {
            store.Clear();

            string programDir = Environment.GetFolderPath(Environment.SpecialFolder.StartMenu);
            LoadDir(new DirectoryInfo(programDir), true);
            
            //programDir = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
            //LoadDir(new DirectoryInfo(programDir), true);
            //programDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), @"Microsoft\Internet Explorer\Quick Launch\User Pinned\TaskBar");
            //LoadDir(new DirectoryInfo(programDir), true);

            string[] indexedLinks = IndexLoader.Load();
            foreach (string link in indexedLinks)
            {
                try
                {
                    Load(new FileInfo(link));
                }
                catch (Exception) { /* Unhandled */ }
            }

            JSONObject settings = Store.Get<SettingsManager>().SettingsObject;
            if (settings != null && settings.ContainsKey("searchPaths"))
            {
                JSONObject searchPathsObject = settings.Get("searchPaths");
                if (searchPathsObject is JSONArray)
                {
                    JSONArray paths = (JSONArray)searchPathsObject;
                    for (int i = 0; i < paths.Length; i++)
                    {
                        JSONObject pathObject = paths[i];
                        string path = null;
                        bool deep = false;

                        if (pathObject.ContainsKey("path"))
                            path = pathObject.GetString("path");
                        if (pathObject.ContainsKey("recursive"))
                            deep = pathObject.GetBool("recursive");

                        FileAttributes attributes = File.GetAttributes(path);
                        if ((attributes & FileAttributes.Directory) == FileAttributes.Directory)
                            LoadDir(new DirectoryInfo(path), deep);
                        else
                            Load(new FileInfo(path));
                    }
                }
            }

            new Thread(new ThreadStart(store.CacheCurrentState)).Start();
        }

        /// <summary>
        /// Loads all apps from the cache and all custom apps used be jeffrey
        /// </summary>
        public void Load()
        {
            /*
            string pathVar = Environment.GetEnvironmentVariable("PATH");
            string[] paths = pathVar.Split(';');

            foreach (string path in paths)
            {
                LoadDir(new DirectoryInfo(path), false);
            }
            */
            
            //
            // LOAD CUSTOM PROMS
            //
            LoadCustomApps();

            //
            // LOAD STARTMENU APPS
            //
            SettingsManager settings = Store.Get<SettingsManager>();
            FileInfo cacheFile = new FileInfo(Path.Combine(settings.SettingsDir.FullName, "appcache.json"));
            if (cacheFile.Exists)
            {
                JSONObject cacheObj = settings.GetFile(cacheFile.FullName);
                if (cacheObj is JSONArray)
                {
                    JSONArray cache = (JSONArray)cacheObj;
                    for (int i = 0; i < cache.Length; i++)
                    {
                        JSONObject cacheEntry = cache[i];
                        if (cacheEntry.ContainsKey("path"))
                            Load(new FileInfo(cacheEntry.GetString("path")));
                    }
                }
            }
            else
            {
                ReIndex();
            }
        }

        /// <summary>Loads custom apps...</summary>
        private void LoadCustomApps()
        {
            Add(new CloseApp());
            Add(new LockWindowsApp());
            Add(new LiveStyleApp());
            Add(new ThemeBrowserApp());
            Add(new InstallPluginApp());
            Add(new RecreateIndexApp(this));
            Add(new OpenSettingsApp());
        }

        /// <summary>Searches in a given directory for apps</summary>
        /// <param name="dir">The directory to search in</param>
        /// <param name="deep">Indicates if the directory should be search recursively</param>
        private void LoadDir(DirectoryInfo dir, bool deep)
        {
            if (dir.Exists)
            {
                try
                {
                    foreach (DirectoryInfo subDir in dir.GetDirectories())
                    {
                        LoadDir(subDir, deep);
                    }
                    foreach (FileInfo file in dir.GetFiles())
                    {
                        Load(file);
                    }
                }
                catch (UnauthorizedAccessException ex) { Store.Get<ErrorLog>().HandleError(ex); }
            }
        }

        /// <summary>
        /// Creates an App from a file (if it's valid)
        /// </summary>
        /// <param name="file">The file to be converted</param>
        private void Load(FileInfo file)
        {
            if (file.Exists && AppMapping.IsValid(file) && (this.config.Validate(file.FullName) || this.config.Validate(file.Name)))
            {
                this.store.Add(AppMapping.Get(file));
            }
        }
    }
}

﻿using JeffreyPlease.Settings;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Diagnostics;
using System.IO;

namespace JeffreyPlease.Logic.Loaders
{
    /// <summary>
    /// Loads apps using the windows index
    /// </summary>
    public static class IndexLoader
    {
        public static string[] Load()
        {
            List<string> apps = new List<string>();

            try
            {
                string connectionString = "Provider=Search.CollatorDSO;Extended Properties=\"Application=Windows\"";
                OleDbConnection connection = new OleDbConnection(connectionString);

                // Check out the link for all Properties
                // http://msdn.microsoft.com/en-us/library/windows/desktop/bb419046%28v=vs.85%29.aspx
                //string query = @"SELECT System.ItemPathDisplay FROM SystemIndex WHERE System.ItemTypeText='Shortcut' OR System.ItemTypeText='Verknüpfung'";
                //string query = @"SELECT System.ItemFolderPathDisplay, System.ItemPathDisplay, System.ItemAuthors, System.ItemName FROM SystemIndex";
                string query = @"SELECT System.ItemFolderPathDisplay, System.ItemPathDisplay, System.ItemName FROM SystemIndex";
                //string query = @"SELECT System.ItemPathDisplay FROM SystemIndex";
                OleDbCommand command = new OleDbCommand(query, connection);
                connection.Open();

                OleDbDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        try
                        {
                            string value = reader.GetString(i);
                            if (i == 1 && !apps.Contains(value))
                            {
                                if (value.ToLower().IndexOfAny(Path.GetInvalidPathChars()) != -1)
                                    continue;

                                FileAttributes attr = File.GetAttributes(value);
                                if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
                                {
                                    foreach (string f in Directory.EnumerateFiles(value))
                                    {
                                        if (f.EndsWith(".exe", StringComparison.InvariantCultureIgnoreCase) ||
                                            f.EndsWith(".lnk", StringComparison.InvariantCultureIgnoreCase))
                                        {
                                            apps.Add(f);
                                        }
                                    }
                                }
                                else if ((attr & FileAttributes.Normal) == FileAttributes.Normal)
                                {
                                    apps.Add(value);
                                }
                            }
                        }
                        catch (Exception e) { Debug.WriteLine(e.Message); }
                    }
                }

                connection.Close();
            }
            catch (Exception ex) { Store.Get<ErrorLog>().HandleError(ex); }

            return apps.ToArray();
        }
    }
}

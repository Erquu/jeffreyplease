﻿using JeffreyPlease.Settings;
using JSONdotNET;
using System;
using System.Text.RegularExpressions;

namespace JeffreyPlease.Logic.Loaders
{
    /// <summary>Config for the AppLoader</summary>
    public class AppLoaderConfig
    {
        /// <summary>The default config</summary>
        public static AppLoaderConfig DEFAULT = new AppLoaderConfig();

        /// <summary>Creates a new Config instance using the settings from the given settingsmanager</summary>
        /// <param name="settingsManager">The SettingsManager to load from</param>
        /// <returns>The loaded config instance</returns>
        public static AppLoaderConfig CreateConfig(SettingsManager settingsManager)
        {
            AppLoaderConfig cfg = DEFAULT;
            JSONObject settings = settingsManager.SettingsObject;

            Regex excludePattern = null;
            string[] exclude = null;

            if (settings.ContainsKey("excludePattern") && settings.Get("excludePattern") is JSONTextNode)
            {
                try
                {
                    excludePattern = new Regex(settings.GetString("excludePattern"), RegexOptions.Compiled | RegexOptions.IgnoreCase);
                }
                catch (ArgumentOutOfRangeException e3) { Store.Get<ErrorLog>().HandleError(e3); }
                catch (ArgumentNullException e2) { Store.Get<ErrorLog>().HandleError(e2); }
                catch (ArgumentException e1) { Store.Get<ErrorLog>().HandleError(e1); }
            }
            if (settings.ContainsKey("exclude") && settings.Get("exclude") is JSONArray)
            {
                JSONArray excludeArray = (JSONArray)settings.Get("exclude");
                int l = excludeArray.Length;
                exclude = new string[l];
                for (int i = 0; i < l; i++)
                {
                    exclude[i] = excludeArray[i].ToString();
                }
            }

            return new AppLoaderConfig(excludePattern, exclude);
        }

        /// <summary>Regex pattern to exclude files</summary>
        private Regex excludePattern;
        /// <summary>Names of files or filepaths to be excluded</summary>
        private string[] exclude;

        /// <summary>
        /// Creates a new instance of AppLoaderConfig
        /// </summary>
        public AppLoaderConfig()
        {
            this.excludePattern = null;
            this.exclude = null;
        }
        /// <summary>
        /// Creates a new instance of AppLoaderConfig with custom exclude settings
        /// </summary>
        /// <param name="excludePattern">The exclude pattern, can be null</param>
        /// <param name="exclude">An array of names to be excluded, can be null</param>
        public AppLoaderConfig(Regex excludePattern, string[] exclude)
        {
            this.excludePattern = excludePattern;
            this.exclude = exclude;
        }

        /// <summary>Validates a file if should not be excluded</summary>
        /// <param name="s">The file-name/-path to check</param>
        /// <returns>True if valid</returns>
        public bool Validate(string s)
        {
            if (exclude != null && Array.BinarySearch(exclude, s) >= 0)
                return false;
            if (excludePattern != null && excludePattern.IsMatch(s))
                return false;
            return true;
        }
    }

}

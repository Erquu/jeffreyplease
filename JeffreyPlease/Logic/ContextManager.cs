﻿using JeffreyPlease.Logic.Apps.SearchContexts;
using System.Collections.Generic;

namespace JeffreyPlease.Logic
{
    /// <summary>
    /// The context manager keeps track of the SearchContexts'
    /// </summary>
    public class ContextManager : ISearchContext
    {
        /// <summary>Gets the default SearchContext</summary>
        public ISearchContext DefaultContext { get; private set; }
        /// <summary>Gets the currently active SearchContext</summary>
        public ISearchContext ActiveContext { get; private set; }
        
        /// <summary>
        /// Holds the eventhandler for the currently active context
        /// </summary>
        private SearchCompleteEventHandler searchCompleteHandler;

        /// <summary>
        /// Creates a new instance of the ContextManager
        /// </summary>
        /// <param name="defaultContext">The SearchContext used as default</param>
        public ContextManager(ISearchContext defaultContext)
        {
            this.DefaultContext = defaultContext;
            this.ActiveContext = defaultContext;
            
            this.searchCompleteHandler = new SearchCompleteEventHandler(defaultContext_SearchComplete);

            this.DefaultContext.SearchComplete += searchCompleteHandler;
        }

        /// <summary>
        /// Sets the active SearchContext to the default SearchContext
        /// </summary>
        public void ResetContext()
        {
            SetActiveContext(this.DefaultContext);
        }

        /// <summary>
        /// Sets a given SearchContext as the active context
        /// </summary>
        /// <param name="context">The context to be set active</param>
        public void SetActiveContext(ISearchContext context)
        {
            if (context != ActiveContext)
            {
                ISearchContext oldContext = ActiveContext;
                if (context != DefaultContext && ActiveContext != DefaultContext)
                {
                    ActiveContext.SearchComplete -= searchCompleteHandler;
                }
                ActiveContext = context;
                if (ActiveContext != DefaultContext)
                    ActiveContext.SearchComplete += searchCompleteHandler;

                FireSearchComplete(ActiveContext.ProvideSearchObjects().ToArray());

                if (oldContext.Deactivated != null)
                    oldContext.Deactivated();
                if (ActiveContext.Activated != null)
                    ActiveContext.Activated();
            }
        }

        /// <summary>Starts searching in the active SearchContext</summary>
        /// <param name="pattern">The search pattern</param>
        public override void Search(string pattern)
        {
            this.ActiveContext.Search(pattern);
        }

        /// <summary>Provides the objects to search in</summary>
        /// <returns>The objects to search in</returns>
        public override List<App> ProvideSearchObjects()
        {
            return ActiveContext.ProvideSearchObjects();
        }

        void defaultContext_SearchComplete(SearchResultEventArgs ev)
        {
            FireSearchComplete(ev);
        }
    }
}

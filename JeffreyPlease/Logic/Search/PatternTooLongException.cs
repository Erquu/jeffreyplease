﻿using System;

namespace JeffreyPlease.Logic.Search
{
    /// <summary>
    /// <para>Exception that can be thrown by the BitapSearch</para>
    /// if the search pattern exceeds the configured pattern length limit
    /// </summary>
    public class PatternTooLongException : Exception
    {
        public PatternTooLongException() : base("Pattern length is too long") { }
    }
}

﻿
namespace JeffreyPlease.Logic.Search
{
    /// <summary>
    /// The SearchEngineManager enables other classes to modify the used search engine
    /// </summary>
    public class SearchEngineManager
    {
        /// <summary>The default search engine, this will be used if no custom search engine is set</summary>
        private ISearchEngine defaultEngine;
        /// <summary>Indicates if the custom search engine should be used if it is defined</summary>
        private bool useCustomEngine = true;
        /// <summary>The custom searchengine</summary>
        private ISearchEngine customEngine = null;

        public SearchEngineManager()
        {
            // Initializes the default search engine
            this.defaultEngine = new BitapSearcher(BitapSearcherOptions.DEFAULT);
        }

        /// <summary>
        /// Returns the search engine that is currently active
        /// </summary>
        public ISearchEngine ActiveSearchEngine
        {
            get
            {
                if (useCustomEngine && customEngine != null)
                {
                    return customEngine;
                }
                return defaultEngine;
            }
        }

        /// <summary>
        /// Sets a custom searchengine
        /// </summary>
        /// <param name="engine">The search engine that should be used</param>
        public void SetCustomEngine(ISearchEngine engine)
        {
            if (engine != null)
            {
                this.customEngine = engine;
                this.useCustomEngine = true;
            }
            else
            {
                this.useCustomEngine = false;
            }
        }
    }
}

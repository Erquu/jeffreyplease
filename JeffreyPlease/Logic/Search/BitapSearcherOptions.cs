﻿
namespace JeffreyPlease.Logic.Search
{
    /// <summary>
    /// Search options to change the behaviour of the BitapSearcher
    /// </summary>
    public class BitapSearcherOptions : SearchOptions
    {
        /// <summary>
        /// The default search options used by jeffrey
        /// </summary>
        public static new BitapSearcherOptions DEFAULT = new BitapSearcherOptions(0, 400, 0.6d, 32, false, false);

        /// <summary>
        /// Gets or sets an estimation for where the patterns is expected to be found
        /// </summary>
        public int Location { get; set; }
        /// <summary>
        /// Gets or sets the maximum distance to the value of <see cref="BitapSearcherOptions.Location"/>
        /// </summary>
        public int Distance { get; set; }
        /// <summary>
        /// <para>Gets or sets the fault tolerance of the serach algorithm</para>
        /// A lower value requires a more accurate match
        /// </summary>
        public double Threshold { get; set; }
        /// <summary>
        /// Gets or sets the maximum pattern length
        /// </summary>
        public int MaxPatternLength { get; set; }

        /// <summary>
        /// Creates a new instance copiing the values from a given instance of BitapSearcherOptions
        /// </summary>
        /// <param name="inheritFrom">The instance to copy the options from</param>
        public BitapSearcherOptions(BitapSearcherOptions inheritFrom) : this(
            inheritFrom.Location,
            inheritFrom.Distance,
            inheritFrom.Threshold,
            inheritFrom.MaxPatternLength,
            inheritFrom.ShouldSort,
            inheritFrom.CaseSensitive) { }
        /// <summary>Creates a new instance of BitapSearcherOptions</summary>
        /// <param name="location">An estimation for where the patterns is expected to be found</param>
        /// <param name="distance">The maximum distance to the value of <see cref="BitapSearcherOptions.Location"/></param>
        /// <param name="threshold">
        /// <para>The fault tolerance of the serach algorithm</para>
        /// A lower value requires a more accurate match
        /// </param>
        /// <param name="maxPatternLength">The maximum pattern length</param>
        /// <param name="sort">Indicates if the search result is sorted for the search score</param>
        /// <param name="caseSensitive">Indicates if the search should be case sensitive</param>
        public BitapSearcherOptions(int location, int distance, double threshold, int maxPatternLength, bool sort, bool caseSensitive) : base (sort, caseSensitive)
        {
            this.Location = location;
            this.Distance = distance;
            this.Threshold = threshold;
            this.MaxPatternLength = maxPatternLength;
        }
    }
}

﻿
namespace JeffreyPlease.Logic.Search
{
    public interface ISearchEngine
    {
        void Initialize(string pattern);

        SearchResult Search(App app);
        SearchResult Search(string text);
    }
}

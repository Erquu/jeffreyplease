﻿using JeffreyPlease.Logic.Apps.SearchContexts;
using JeffreyPlease.Settings;
using System;
using System.Collections.Generic;

namespace JeffreyPlease.Logic.Search
{
    /// <summary>
    /// Searches in a list of apps using the BitapSearcher
    /// </summary>
    public class AppSearch
    {
        /// <summary>
        /// Temporarily stores the item index and it's search score
        /// </summary>
        private struct ResultMapEntry
        {
            /// <summary>Seach score</summary>
            public double score;
            /// <summary>Item index</summary>
            public int item;
        }

        /// <summary>List of apps to search in</summary>
        private List<App> list;
        /// <summary></summary>
        private Dictionary<int, ResultMapEntry> resultMap;
        /// <summary></summary>
        private List<ResultMapEntry> rawResults;

        /// <summary>The search engine manager</summary>
        private SearchEngineManager searchEnginemanager;
        /// <summary>The currently active search engine</summary>
        private ISearchEngine searchEngine;

        /// <summary>
        /// Creates a new searchEngine
        /// </summary>
        /// <param name="list">The list of apps to search in</param>
        public AppSearch(List<App> list)
        {
            this.list = list;

            this.searchEnginemanager = Store.Get<SearchEngineManager>();
        }

        /// <summary>
        /// Searches the list of apps for the given pattern
        /// </summary>
        /// <param name="pattern">The search pattern</param>
        /// <returns>The result set of </returns>
        public List<App> Search(string pattern)
        {
            resultMap = new Dictionary<int, ResultMapEntry>();
            rawResults = new List<ResultMapEntry>();

            searchEngine = this.searchEnginemanager.ActiveSearchEngine;
            searchEngine.Initialize(pattern);

            for (int i = 0, l = list.Count; i < l; i++)
            {
                AnalyzeText(list[i], i);
            }

            if (ISearchContext.SearchOptions.ShouldSort)
            {
                try
                {
                    rawResults.Sort((left, right) => left.score.CompareTo(right.score));
                }
                catch (InvalidOperationException ex) { Store.Get<ErrorLog>().HandleError(ex); }
            }

            List<App> results = new List<App>();

            for (int i = 0; i < rawResults.Count; i++)
            {
                results.Add(list[rawResults[i].item]);
            }

            return results;
        }

        /// <summary>
        /// Processes the given app 
        /// </summary>
        /// <param name="app">The app used for searching</param>
        /// <param name="index">The index of the app (used for later processing)</param>
        private void AnalyzeText(App app, int index)
        {
            if (!String.IsNullOrWhiteSpace(app.Name))
            {
                SearchResult result = searchEngine.Search(app);

                if (result.isMatch)
                {
                    if (resultMap.ContainsKey(index)) {
                        ResultMapEntry existingResult = resultMap[index];
                        existingResult.score = Math.Min(existingResult.score, result.score);
                    } else {
                        ResultMapEntry entry = new ResultMapEntry();
                        entry.item = index;
                        entry.score = result.score;
                        resultMap.Add(index, entry);
                        rawResults.Add(entry);
                    }
                }
            }
        }
    }
}

﻿
namespace JeffreyPlease.Logic.Search
{
    public struct SearchResult
    {
        public bool isMatch;
        public double score;
    }
}

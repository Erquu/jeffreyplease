﻿using System;
using System.Collections.Generic;

namespace JeffreyPlease.Logic.Search
{
    public class BitapSearcher : ISearchEngine
    {
        private string pattern;
        private int patternLength;

        private int matchMask;
        private Dictionary<char, int> patternAlphabet;

        private BitapSearcherOptions options;

        public BitapSearcher() : this(BitapSearcherOptions.DEFAULT) { }
        public BitapSearcher(BitapSearcherOptions options)
        {
            this.options = options;
        }

        public void Initialize(string pattern)
        {
            this.pattern = pattern;

            if (!this.options.CaseSensitive)
            {
                this.pattern = this.pattern.ToLower();
            }
            this.patternLength = this.pattern.Length;

            if (this.patternLength > this.options.MaxPatternLength)
            {
                throw new PatternTooLongException();
            }

            this.matchMask = 1 << (this.patternLength - 1);
            this.patternAlphabet = this.CalculatePatternAlphabet();
        }

        public BitapSearcherOptions GetOptions()
        {
            return this.options;
        }

        public SearchResult Search(App app)
        {
            SearchResult result = new SearchResult();
            SearchResult temp = Search(app.Name);
            result.isMatch = temp.isMatch;
            result.score = temp.score;

            string[] aliases = app.Aliases;
            if (aliases != null)
            {
                foreach (string alias in aliases)
                {
                    temp = Search(alias);
                    if (temp.isMatch)
                    {
                        result.isMatch = true;
                        result.score = Math.Min(result.score, temp.score);
                    }
                }
            }

            return result;
        }
        public SearchResult Search(string text)
        {
            text = this.options.CaseSensitive ? text : text.ToLower();
            SearchResult result = new SearchResult();

            if (this.pattern == text)
            {
                result.isMatch = true;
                result.score = 0;
                return result;
            }

            int i, j;

            int textLength = text.Length;
            int location = this.options.Location;
            double threshold = this.options.Threshold;

            int bestLoc = text.IndexOf(this.pattern, location);
            double binMin, binMid;
            double binMax = (double)(this.patternLength + textLength);
            int start, finish;

            int[] bitArr, lastBitArr;

            int charMatch;

            double score = 1.0d;
            List<int> locations = new List<int>();

            if (bestLoc != -1)
            {
                threshold = Math.Min(this.BitapScore(0, bestLoc), threshold);
                bestLoc = text.LastIndexOf(this.pattern, location + this.patternLength);

                if (bestLoc != -1)
                {
                    threshold = Math.Min(this.BitapScore(0, bestLoc), threshold);
                }
            }

            bestLoc = -1;

            for (i = 0; i < this.patternLength; i++)
            {
                binMin = 0;
                binMid = binMax;

                while (binMin < binMid)
                {
                    if (this.BitapScore(i, (double)location + binMid) <= threshold)
                    {
                        binMin = binMid;
                    }
                    else
                    {
                        binMax = binMid;
                    }
                    binMid = (int)Math.Floor((double)(binMax - binMin) / 2.0d + binMin); // *.5d faster
                }

                binMax = binMid;
                start = (int)Math.Max(1.0d, (double)location - binMid + 1.0d);
                finish = (int)Math.Min(location + binMid, textLength) + this.patternLength;// +Math.Min(textLength, this.patternLength);

                bitArr = new int[finish + 2];
                bitArr[finish + 1] = (1 << i) - 1;

                lastBitArr = new int[finish + 2]; // Filled with 0?

                for (j = finish; j >= start; j--)
                {
                    if (j <= text.Length && this.patternAlphabet.ContainsKey(text[j - 1]))
                    {
                        charMatch = this.patternAlphabet[text[j - 1]];
                    }
                    else
                    {
                        charMatch = 0;
                    }

                    if (i == 0)
                    {
                        bitArr[j] = ((bitArr[j + 1] << 1) | 1) & charMatch;
                    }
                    else
                    {
                        bitArr[j] = ((bitArr[j + 1] << 1) | 1) & charMatch | (((lastBitArr[j + 1] | lastBitArr[j]) << 1) | 1) | lastBitArr[j + 1];
                    }

                    if ((bitArr[j] & this.matchMask) != 0)
                    {
                        score = this.BitapScore(i, j - 1);
                        if (score <= threshold)
                        {
                            threshold = score;
                            bestLoc = j - 1;
                            locations.Add(bestLoc);

                            if (bestLoc > location)
                            {
                                start = Math.Max(1, 2 * location - bestLoc);
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }

                if (this.BitapScore(i + 1, location) > threshold)
                {
                    break;
                }
                lastBitArr = bitArr;
            }

            result.isMatch = (bestLoc >= 0);
            result.score = score;
            return result;
        }

        private Dictionary<char, int> CalculatePatternAlphabet()
        {
            Dictionary<char, int> mask = new Dictionary<char, int>();
            int i = 0;

            for (; i < this.patternLength; i++)
            {
                if (!mask.ContainsKey(this.pattern[i])) {
                    mask.Add(this.pattern[i], 0);
                }
            }

            for (i = 0; i < patternLength; i++)
            {
                mask[this.pattern[i]] |= 1 << (this.pattern.Length - i - 1);
            }

            return mask;
        }

        private double BitapScore(double errors, double location)
        {
            double accuracy = errors / (double)this.patternLength;
            double proximity = Math.Abs((double)this.options.Location - location);

            if (this.options.Distance == 0)
            {
                return proximity != 0 ? 1.0d : accuracy;
            }
            return accuracy + (proximity / (double)this.options.Distance);
        }
    }
}

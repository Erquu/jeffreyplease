﻿
namespace JeffreyPlease.Logic.Search
{
    /// <summary>
    /// Options for the search engine
    /// </summary>
    public class SearchOptions
    {
        /// <summary>
        /// The default search options
        /// </summary>
        public static SearchOptions DEFAULT = new SearchOptions(true, false);

        /// <summary>
        /// Gets or sets a boolean value indicating if the search result should be sorted (by their scores)
        /// </summary>
        public bool ShouldSort { get; set; }
        /// <summary>
        /// Gets or sets an indicator for the search to be case sensitive
        /// </summary>
        public bool CaseSensitive { get; set; }

        /// <param name="shouldSort">Boolean value indicating if the search result should be sorted (by their scores)</param>
        public SearchOptions(bool shouldSort, bool caseSensitive)
        {
            this.ShouldSort = shouldSort;
            this.CaseSensitive = caseSensitive;
        }
    }
}

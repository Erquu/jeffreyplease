﻿using JeffreyPlease.Settings;
using JSONdotNET;
using System;
using System.IO;

namespace JeffreyPlease.Logic.Repository
{
    public class LocalRepository : IRepository
    {
        public LocalRepository(Uri repositoryUri) : base(repositoryUri) { }

        protected override void StartDownload(string uri, string name)
        {
            if (File.Exists(uri))
            {
                string target = Path.Combine(Path.GetTempPath(), String.Format("{0}.jeffreyplugin", name.ToLower()));
                File.Copy(uri, target, true);

                this.FireDownloadComplete(target);
            }
        }

        protected override void FetchMeta()
        {
            JSONObject repo = Store.Get<SettingsManager>().GetFile(this.reposositoryUri.LocalPath);
            this.repositoryObject = repo;

            this.FireMetaFetchComplete(repo);
        }
    }
}

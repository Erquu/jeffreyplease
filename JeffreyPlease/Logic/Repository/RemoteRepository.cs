﻿using JeffreyPlease.Logic.Apps.SearchContexts;
using JeffreyPlease.Settings;
using JSONdotNET;
using System;
using System.IO;
using System.Net;

namespace JeffreyPlease.Logic.Repository
{
    public class RemoteRepository : IRepository
    {
        /// <summary>The proxy used for loading plugins</summary>
        private WebProxy proxy = null;

        public RemoteRepository(Uri repositoryUri) : base(repositoryUri)
        {
            SettingsManager settingsManager = Store.Get<SettingsManager>();
            JSONObject settingsObject = settingsManager.SettingsObject;
            if (settingsObject.ContainsKey("proxy"))
            {
                JSONObject proxy = settingsObject["proxy"];
                if (proxy.ContainsKey("host") && proxy.ContainsKey("port"))
                {
                    bool active = true;

                    try
                    {
                        if (proxy.ContainsKey("active"))
                        {
                            active = proxy.GetBool("active");
                        }
                    }
                    catch (Exception e) { Store.Get<ErrorLog>().HandleError(e); }

                    try
                    {
                        if (active)
                        {
                            string host = proxy.GetString("host");
                            int port = proxy.GetInt("port");
                            this.proxy = new WebProxy(host, port);
                        }
                    }
                    catch (Exception e) { Store.Get<ErrorLog>().HandleError(e); }
                }
            }
        }

        protected override void StartDownload(string uri, string name)
        {
            SettingsManager settingsManager = Store.Get<SettingsManager>();

            if (Uri.IsWellFormedUriString(uri, UriKind.Absolute))
            {
                WebClient c = new WebClient();
                c.Proxy = this.proxy;
                string target = Path.Combine(settingsManager.PluginDir.FullName, String.Format("{0}.jeffreyplugin", name.ToLower()));
                try
                {
                    c.DownloadFile(uri, target);
                    this.FireDownloadComplete(target);
                }
                catch (WebException e)
                {
                    Store.Get<ErrorLog>().HandleError(e);
                    Store.Get<ContextManager>().SetActiveContext(new MessageContext("Could not install plugin", e.Message));
                }
            }
        }

        protected override void FetchMeta()
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(this.reposositoryUri);
            request.Proxy = this.proxy;

            try
            {
                WebResponse response = request.GetResponse();
                Stream responseStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream);

                this.repositoryObject = JSON.Parse(reader.ReadToEnd());

                reader.Close();
                responseStream.Close();
                response.Close();

                this.FireMetaFetchComplete(this.repositoryObject);
            }
            catch (Exception e)
            {
                Store.Get<ErrorLog>().HandleError(e);
                Store.Get<ContextManager>().SetActiveContext(new MessageContext("Could not fetch plugin repository", e.Message));
            }
        }
    }
}

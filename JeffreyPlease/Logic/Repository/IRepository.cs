﻿using JeffreyPlease.Utils;
using JSONdotNET;
using System;
using System.Threading;

namespace JeffreyPlease.Logic.Repository
{
    public delegate void MetaFetchCompleteEventHandler(JSONObject repo);
    public delegate void DownloadCompleteEventHandler(string pluginTempPath);

    public abstract class IRepository
    {
        protected JSONObject repositoryObject = null;
        protected Uri reposositoryUri = null;

        public JSONObject RepositoryObject { get { return repositoryObject; } }

        private Thread metaDownloadThread;

        public event MetaFetchCompleteEventHandler MetaFetchComplete;
        public event DownloadCompleteEventHandler DownloadComplete;

        public IRepository(Uri reposositoryUri)
        {
            this.reposositoryUri = reposositoryUri;
        }

        public void Fetch()
        {
            this.Fetch(true);
        }
        public void Fetch(bool async)
        {
            this.PrepareRepo(async);
        }

        public void DownloadPlugin(string name)
        {
            (new Thread(new ParameterizedThreadStart(SearchPlugin))).Start(name);
        }

        private void SearchPlugin(object nameObject)
        {
            if (nameObject != null)
            {
                this.PrepareRepo(false);
                string name = nameObject.ToString();

                if (this.repositoryObject.ContainsKey("packages"))
                {
                    JSONObject packagesObject = this.repositoryObject["packages"];
                    if (packagesObject is JSONArray)
                    {
                        JSONArray packages = packagesObject as JSONArray;
                        for (int i = 0; i < packages.Length; i++)
                        {
                            JSONObject package = packages[i];
                            if (package.ContainsKey("name"))
                            {
                                string packageName = package.GetString("name");
                                if (name.Equals(packageName))
                                {
                                    this.StartDownload(package, packageName);
                                }
                            }
                        }
                    }
                }
            }
        }

        private void StartDownload(JSONObject package, string name)
        {
            if (package.ContainsKey("releases") && package["releases"] is JSONArray)
            {
                JSONArray releases = package["releases"] as JSONArray;
                for (int i = 0; i < releases.Length; i++)
                {
                    JSONObject release = releases[i];
                    if (release.ContainsKey("version"))
                    {
                        if (Tools.CheckVersion(release.GetString("version")))
                        {
                            if (release.ContainsKey("url"))
                            {
                                this.StartDownload(release.GetString("url"), name);
                            }
                        }
                    }
                }
            }
        }

        private void PrepareRepo(bool async)
        {
            if (this.repositoryObject == null)
            {
                if (this.metaDownloadThread == null || this.metaDownloadThread.ThreadState != ThreadState.Running)
                {
                    if (this.metaDownloadThread != null)
                        this.metaDownloadThread.Abort();

                    if (async)
                    {
                        this.metaDownloadThread = new Thread(new ThreadStart(this.FetchMeta));
                        this.metaDownloadThread.Start();
                    }
                    else
                    {
                        this.FetchMeta();
                    }
                }
                else
                {
                    if (async)
                    {
                        // Do nothing since download is already in progress
                    }
                    else
                    {
                        this.metaDownloadThread.Join();
                    }
                }
            }
            else
            {
                if (this.MetaFetchComplete != null)
                    this.MetaFetchComplete.Invoke(this.repositoryObject);
            }
        }

        protected void FireDownloadComplete(string target)
        {
            if (this.DownloadComplete != null)
                this.DownloadComplete.Invoke(target);
        }

        protected void FireMetaFetchComplete(JSONObject repo)
        {
            if (this.MetaFetchComplete != null)
                this.MetaFetchComplete.Invoke(repo);
        }

        protected abstract void StartDownload(string uri, string name);
        protected abstract void FetchMeta();
    }
}

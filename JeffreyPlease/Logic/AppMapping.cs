﻿using JeffreyPlease.Logic.Mappings;
using System;
using System.Collections.Generic;
using System.IO;

namespace JeffreyPlease.Logic
{
    /// <summary>
    /// <para>Maps a file extension to an App</para>
    /// When apps are loaded, a check is made if any mapping for the extension exists
    /// </summary>
    public abstract class AppMapping
    {
        /// <summary>
        /// extension - mapping map :)
        /// </summary>
        private static Dictionary<string, AppMapping> mappings = new Dictionary<string, AppMapping>();

        /// <summary>
        /// Adds an AppMapping
        /// </summary>
        /// <param name="mapping">The mapping to be added</param>
        public static void AddMapping(AppMapping mapping)
        {
            string extension = mapping.Extension.ToLower();
            if (!mappings.ContainsKey(extension))
            {
                mappings.Add(extension, mapping);
            }
        }

        static AppMapping()
        {
            AddMapping(new exe());
            AddMapping(new bat());
            AddMapping(new cmd());
            AddMapping(new lnk());
        }

        /// <summary>
        /// Checks if a mapping for the given file exists
        /// </summary>
        /// <param name="file">The file to be checked</param>
        /// <returns>True if file is valid and has a mapping</returns>
        public static bool IsValid(FileInfo file)
        {
            string extension;
            return file.Exists && IsValid(file, out extension);
        }
        /// <summary>
        /// Checks if a mapping for the given file exists
        /// </summary>
        /// <param name="file">The file to be checked</param>
        /// <param name="extension">The extension of the file</param>
        /// <returns>True if file is valid and has a mapping</returns>
        private static bool IsValid(FileInfo file, out string extension)
        {
            extension = file.Extension.ToLower();
            if (!String.IsNullOrWhiteSpace(extension))
            {
                if (extension[0] == '.')
                    extension = extension.Substring(1);

                return mappings.ContainsKey(extension);
            }
            return false;
        }

        /// <summary>
        /// Creates an app from a file if the file extension is mapped
        /// </summary>
        /// <param name="file">The file to be converted</param>
        /// <returns>The app for the file or null if file is not valid</returns>
        public static App Get(FileInfo file)
        {
            string extension;
            if (IsValid(file, out extension))
                return mappings[extension].GetApp(file);
            return null;
        }

        /// <summary>
        /// The using extension of the mapping
        /// </summary>
        public abstract string Extension { get; }
        /// <summary>
        /// Converts the given file to it's representative app
        /// </summary>
        /// <param name="file">The file to be converted</param>
        /// <returns>The App for the file</returns>
        public abstract App GetApp(FileInfo file);
    }
}

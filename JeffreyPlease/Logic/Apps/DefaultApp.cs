﻿using JeffreyPlease.Logic.Apps.SearchContexts;

namespace JeffreyPlease.Logic.Apps
{
    public abstract class DefaultApp : App
    {
        public virtual string Name { get; protected set; }
        public virtual string[] Aliases { get; set; }
        public virtual string Description { get; protected set; }
        public virtual System.Drawing.Icon Icon { get; protected set; }
        public virtual bool SaveStatistics { get { return false; } }

        public DefaultApp()
        {
            this.Icon = null;
        }

        public abstract void Run();

        public virtual ISearchContext GetContext() { return null; }

        public virtual void Hover() { }
        public virtual void Unhover() { }
    }
}

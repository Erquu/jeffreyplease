﻿using JeffreyPlease.UI;
using System;

namespace JeffreyPlease.Logic.Apps.Static
{
    public class LiveStyleApp : DefaultApp
    {
        public override string Name { get { return "LiveStyle"; } }
        public override string Description { get {
            bool active = Store.Get<ThemeManager>().IsLiveStyling;
            return String.Format("Activate to {0} LiveStyle", (active ? "stop" : "start"));
        } }

        public override void Run()
        {
            ThemeManager tm = Store.Get<ThemeManager>();
            if (!tm.IsLiveStyling)
                tm.EnableLiveStyle();
            else
                tm.DisableLiveStyle();

            System.Windows.Forms.MessageBox.Show(String.Format("LiveStyle is now {0}", tm.IsLiveStyling ? "enabled" : "disabled"));
        }
    }
}

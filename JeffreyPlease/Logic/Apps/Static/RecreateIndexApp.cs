﻿using JeffreyPlease.Logic.Apps.SearchContexts;
using JeffreyPlease.Logic.Loaders;
using JeffreyPlease.Properties;
using System.Drawing;
using System.Threading;

namespace JeffreyPlease.Logic.Apps.Static
{
    class RecreateIndexApp : DefaultApp
    {
        public override string Name { get { return "Recreate Index"; } }
        public override string Description { get { return "Search for new Programs"; } }
        public override Icon Icon { get { return new Icon(Resources.Icon_Recycle, new Size(72, 72)); } }

        private AppLoader loaderInstance;

        public RecreateIndexApp(AppLoader loader)
        {
            this.loaderInstance = loader;
        }

        public override void Run()
        {
            loaderInstance.ReIndex();

            Notification.Show(this.Icon, "Index creation complete");
        }

        public override ISearchContext GetContext()
        {
            Thread reindexThread = new Thread(new ThreadStart(Run));
            reindexThread.Name = "Reindexing thread";
            reindexThread.Start();

            return new MessageContext("Indexing is running", "The indexing is running in the background");
        }
    }
}

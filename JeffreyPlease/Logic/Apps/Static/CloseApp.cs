﻿using JeffreyPlease.Properties;
using System.Drawing;
using System.Windows.Forms;

namespace JeffreyPlease.Logic.Apps.Static
{
    public class CloseApp : DefaultApp
    {
        public override string Name { get { return "Close"; } }
        public override string Description { get { return "Say goodbye to Jeffrey"; } }
        public override Icon Icon { get { return new Icon(Resources.Icon_Close, new Size(72, 72)); } }

        public override void Run()
        {
            Application.Exit();
        }
    }
}

﻿using JeffreyPlease.Properties;
using System.Drawing;

namespace JeffreyPlease.Logic.Apps.Static
{
    public class OpenSettingsApp : DefaultApp
    {
        public override string Name { get { return "Open Settings"; } }
        public override string Description { get { return "Open Settings"; } }
        public override Icon Icon { get { return new Icon(Resources.Icon_Gear, new Size(72, 72)); } }

        public OpenSettingsApp()
        {
            Aliases = new string[] { "Settings", "Einstellungen" };
        }

        public override void Run()
        {
            SettingsView sv = new SettingsView();
            System.Windows.Forms.Application.Run(sv);
        }
    }
}

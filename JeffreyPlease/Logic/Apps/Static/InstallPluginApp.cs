﻿using JeffreyPlease.Logic.Apps.SearchContexts;
using JeffreyPlease.Properties;
using System.Drawing;
using System.Threading;

namespace JeffreyPlease.Logic.Apps.Static
{
    public class InstallPluginApp : DefaultApp
    {
        public override string Name { get { return "Install Plugins"; } }
        public override string Description { get { return "Install some Plugins"; } }
        public override Icon Icon { get { return new Icon(Resources.Icon_Download, new Size(72, 72)); } }

        private PluginRepositoryContext context = new PluginRepositoryContext();

        private Thread loadThread = null;

        public override void Run() { }

        public override ISearchContext GetContext()
        {
            if (loadThread != null)
            {
                loadThread.Abort();
                loadThread = null;
            }

            loadThread = new Thread(new ThreadStart(LoadThemeContext));
            loadThread.Start();

            MessageContext c = new MessageContext("Loading Plugins...", "This could take a while, please wait");
            c.Deactivated = Deactivate;
            return c;
        }

        private void LoadThemeContext()
        {
            PluginRepositoryContext context = new PluginRepositoryContext();
            context.Load();
            Store.Get<ContextManager>().SetActiveContext(context);
        }

        private void Deactivate()
        {
            if (loadThread != null)
            {
                loadThread.Abort();
                loadThread = null;
            }
        }
    }
}

﻿using JeffreyPlease.Properties;
using JeffreyPlease.Settings;
using JeffreyPlease.UI;
using System.Drawing;

namespace JeffreyPlease.Logic.Apps.Static
{
    public class ThemePreviewApp : DefaultApp
    {
        public ThemePreviewApp(MetaInformation meta)
        {
            this.Name = meta.Name;
            this.Icon = new Icon(Resources.Icon_Rainbow, new Size(72, 72));

            if (meta.Description != null)
            {
                this.Description += meta.Description;
                if (meta.Author != null)
                    this.Description += " - ";
            }
            if (meta.Author != null)
                this.Description += meta.Author;
        }

        public override void Run()
        {
            ThemeManager tm = Store.Get<ThemeManager>();
            tm.SetActive(Name);
            Store.Get<SettingsManager>().ChangeSetting("theme", Name);
        }

        public override void Hover()
        {
            ThemeManager tm = Store.Get<ThemeManager>();
            tm.SetActive(Name);
        }
    }
}

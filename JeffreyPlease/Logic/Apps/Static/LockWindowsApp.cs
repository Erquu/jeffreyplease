﻿using JeffreyPlease.Properties;
using System.Drawing;
using System.Runtime.InteropServices;

namespace JeffreyPlease.Logic.Apps.Static
{
    public class LockWindowsApp : DefaultApp
    {
        [DllImport("user32.dll")]
        public static extern bool LockWorkStation();

        public override string Name { get { return "Lock"; } }
        public override string Description { get { return "Lock Windows"; } }
        public override Icon Icon { get { return new Icon(Resources.Icon_Locked, new Size(72, 72)); } }

        public override void Run()
        {
            LockWorkStation();
        }
    }
}

﻿using JeffreyPlease.Logic.Apps.SearchContexts;
using System.Drawing;

namespace JeffreyPlease.Logic.Apps.Static
{
    public class MessageApp : DefaultApp
    {
        protected ISearchContext context = null;

        public MessageApp(string name, string description) : this(name, description, null) { }
        public MessageApp(string name, string description, Icon icon) : this(name, description, icon, null) { }
        public MessageApp(string name, string description, Icon icon, ISearchContext context)
        {
            this.Name = name;
            this.Description = description;
            this.Icon = icon;

            this.context = context;
        }

        public override void Run() { }
        public override ISearchContext GetContext() { return context; }
    }
}

﻿using JeffreyPlease.Logic.Apps.SearchContexts;
using JeffreyPlease.Logic.Repository;
using JeffreyPlease.Plugin;
using JeffreyPlease.Properties;
using JeffreyPlease.Settings;
using JeffreyPlease.UI;
using System.Drawing;
using System.IO;
using System.Threading;

namespace JeffreyPlease.Logic.Apps.Static
{
    public class RepositoryApp : DefaultApp
    {
        private MetaInformation meta;
        private IRepository repo;

        public RepositoryApp(MetaInformation meta, IRepository repo)
        {
            this.meta = meta;
            this.repo = repo;

            this.Name = this.meta.Name;
            this.Description = this.meta.Description;

            this.Aliases = this.meta.Labels;

            if (this.meta.Type == PluginType.PLUGIN)
            {
                this.Icon = new Icon(Resources.Icon_Plugin, new Size(72, 72));
            }
            else if (this.meta.Type == PluginType.THEME)
            {
                this.Icon = new Icon(Resources.Icon_Theme, new Size(72, 72));
            }
        }

        public override void Run()
        {
            this.repo.DownloadComplete += new DownloadCompleteEventHandler(repo_DownloadComplete);
            this.repo.DownloadPlugin(this.meta.Name);
        }

        public override ISearchContext GetContext() {
            new Thread(new ThreadStart(Run)).Start();
            return new MessageContext("Installing...", "Please wait a moment");
        }

        private void repo_DownloadComplete(string pluginTempPath)
        {
            string extension = Path.GetExtension(pluginTempPath);

            if (".jeffreyplugin".Equals(extension, System.StringComparison.InvariantCultureIgnoreCase))
            {
                SettingsManager settings = Store.Get<SettingsManager>();

                // TODO: Loading is async, make the plugin installed message show only after reloading is done
                Store.Get<PluginLoader>().Load(false);

                TemporaryContext c = new MessageContext("Plugin installed", "Feel free to try it right now");
                ThemeBrowserApp[] themeBrowserApps = Store.Get<AppStore>().GetByType<ThemeBrowserApp>();
                if (themeBrowserApps.Length > 0)
                {
                    c.Add(themeBrowserApps[0]);
                }

                Store.Get<ContextManager>().SetActiveContext(c);
            }
        }
    }
}

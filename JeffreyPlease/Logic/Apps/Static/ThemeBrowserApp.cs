﻿using JeffreyPlease.Logic.Apps.SearchContexts;
using JeffreyPlease.Properties;
using System.Drawing;

namespace JeffreyPlease.Logic.Apps.Static
{
    public class ThemeBrowserApp : DefaultApp
    {
        private ThemeBrowserContext context = new ThemeBrowserContext();

        public override string Name { get { return "Theme Browser"; } }
        public override string Description { get { return "Make Jeffrey change his appearance"; } }
        public override Icon Icon { get { return new Icon(Resources.Icon_Swatches, new Size(72, 72)); } }

        public override void Run() { }

        public override ISearchContext GetContext()
        {
            return context;
        }
    }
}

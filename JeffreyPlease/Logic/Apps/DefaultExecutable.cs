﻿using JeffreyPlease.Settings;
using JeffreyPlease.Utils;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;

namespace JeffreyPlease.Logic.Apps
{
    public class DefaultExecutable : DefaultApp
    {
        public FileInfo File { get; private set; }
        public override bool SaveStatistics { get { return true; } }

        public DefaultExecutable(FileInfo file)
        {
            this.Name = file.Name;
            this.File = file;

            this.Description = this.File.FullName;
            this.Icon = Tools.ExtractIcon(this.File.FullName);
        }

        public override void Run()
        {
            Run(File);
        }
        protected virtual void Run(FileInfo file)
        {
            bool processFound = false;

            if (file != null && file.Exists)
            {
                Process[] processes = Process.GetProcesses();
                foreach (Process process in processes)
                {
                    try
                    {
                        if (process.MainModule.FileName.Equals(file.FullName))
                        {
                            Tools.SwitchToThisWindow(process.MainWindowHandle, false);
                            Tools.ShowWindow(process.MainWindowHandle, Tools.SW_SHOW);
                            processFound = true;
                            break;
                        }
                    }
                    catch (Win32Exception ex) { Store.Get<ErrorLog>().HandleError(ex); }
                }
            }

            if (!processFound)
            {
                try
                {
                    Process proc = new Process();
                    proc.StartInfo.FileName = File.FullName;
                    proc.StartInfo.LoadUserProfile = true;
                    proc.Start();
                }
                catch (Win32Exception ex)
                {
                    Store.Get<ErrorLog>().HandleError(ex);
                    // TODO: Show message box to user
                }
            }
        }

        public virtual JSONdotNET.JSONObject ToJSONObject()
        {
            string filePath = File.FullName.Replace('\\', '/');
            return new JSONdotNET.JSONObject().Add("name", Name).Add("path", filePath);
        }
    }
}

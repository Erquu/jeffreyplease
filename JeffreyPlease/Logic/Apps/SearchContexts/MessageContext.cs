﻿using JeffreyPlease.Logic.Apps.Static;

namespace JeffreyPlease.Logic.Apps.SearchContexts
{
    public class MessageContext : TemporaryContext
    {
        public MessageContext(string message, string description) : this(message, description, null) { }
        public MessageContext(string message, string description, ISearchContext subContext)
        {
            this.Add(new MessageApp(message, description, null, subContext));
        }
    }
}

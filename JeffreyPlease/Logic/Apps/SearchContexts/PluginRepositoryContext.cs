﻿using JeffreyPlease.Logic.Apps.Static;
using JeffreyPlease.Logic.Repository;
using JeffreyPlease.Plugin;
using JeffreyPlease.Properties;
using JeffreyPlease.Settings;
using JeffreyPlease.UI;
using JeffreyPlease.Utils;
using JSONdotNET;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;

namespace JeffreyPlease.Logic.Apps.SearchContexts
{
    public class PluginRepositoryContext : ISearchContext
    {
        private List<App> plugins;

        public void Load()
        {
            plugins = new List<App>();

            JSONArray repos = new JSONArray();

            try
            {
                JSONObject settingsObject = Store.Get<SettingsManager>().SettingsObject;
                repos = (JSONArray)settingsObject.Get("repositories");
            }
            catch (Exception ex) { Store.Get<ErrorLog>().HandleError(ex); }

            for (int i = 0; i < repos.Length; i++)
            {
                if (repos[i].ContainsKey("path"))
                {
                    FileInfo repoLocation = new FileInfo(repos[i].GetString("path"));
                    LoadRepo(repoLocation);
                }
                else if (repos[i].ContainsKey("url"))
                {
                    try
                    {
                        Uri uri = new Uri(repos[i].GetString("url"));
                        LoadRepo(uri);
                    }
                    catch (UriFormatException ex)
                    {
                        string message = String.Format("Error with repository \"{0}\"", repos[i].GetString("name"));
                        plugins.Add(new MessageApp(message, ex.Message, new Icon(Resources.Icon_Caution, new Size(72, 72))));
                    }
                }
            }
        }

        public override List<App> ProvideSearchObjects()
        {
            if (plugins.Count == 0)
            {
                plugins.Clear();
                plugins.Add(new MessageApp("No plugins found!", "Try adding other repositories or try again later."));

                InstallPluginApp[] result = Store.Get<AppStore>().GetByType<InstallPluginApp>();
                if (result.Length > 0) {
                    InstallPluginApp installApp = result[0];
                    HijackerApp app = new HijackerApp();
                    app.SetHijackedApp(installApp);
                    app.Name = "Retry";
                    app.Description = "Search again for new plugins";
                    plugins.Add(app);
                }
            }

            return plugins;
        }

        private void LoadRepo(FileInfo repoFile)
        {
            if (repoFile.Exists)
            {
                LocalRepository repo = new LocalRepository(new Uri(repoFile.FullName));
                LoadRepo(repo);
            }
        }

        private void LoadRepo(Uri uri)
        {
            RemoteRepository repo = new RemoteRepository(uri);
            LoadRepo(repo);
        }

        private void LoadRepo(Repository.IRepository repo)
        {
            repo.Fetch(false);

            JSONObject repoObj = repo.RepositoryObject;
            PluginLoader loader = Store.Get<PluginLoader>();
            
            if (repoObj != null && repoObj.ContainsKey("packages") && repoObj["packages"] is JSONArray)
            {
                JSONArray arr = repoObj["packages"] as JSONArray;
                for (int j = 0; j < arr.Length; j++)
                {
                    JSONObject plugin = arr[j];

                    string name = "";
                    string author = "";
                    string description = "";
                    string[] labels = new string[0];
                    PluginType type = PluginType.UNKNOWN;

                    if (plugin.ContainsKey("name"))
                        name = plugin.GetString("name");
                    if (plugin.ContainsKey("description"))
                        description = plugin.GetString("description");
                    if (plugin.ContainsKey("author"))
                        author = plugin.GetString("author");

                    if (plugin.ContainsKey("type"))
                    {
                        string typeString = plugin.GetString("type");
                        type = (PluginType)Enum.Parse(typeof(PluginType), typeString, true);
                    }

                    if (plugin.ContainsKey("labels") && plugin["labels"] is JSONArray)
                    {
                        JSONArray labelsArray = plugin["labels"] as JSONArray;
                        labels = new string[labelsArray.Length];
                        for (int i = 0; i < labelsArray.Length; i++)
                        {
                            labels[i] = labelsArray[i].ToString();
                        }
                    }

                    if (plugin.ContainsKey("releases") && plugin["releases"] is JSONArray)
                    {
                        JSONArray releases = plugin["releases"] as JSONArray;
                        for (int i = 0; i < releases.Length; i++)
                        {
                            JSONObject release = releases[i];
                            bool validVersion = false;

                            if (release.ContainsKey("version"))
                            {
                                string version = release.GetString("version");
                                validVersion = Tools.CheckVersion(version);
                            }

                            if (validVersion)
                            {
                                MetaInformation meta = new MetaInformation(name, description, author, type, labels);

                                if (!InList(name) && !loader.IsLoaded(name))
                                    this.plugins.Add(new RepositoryApp(meta, repo));

                                break;
                            }
                        }
                    }
                }
            }
        }

        private bool InList(string name)
        {
            if (!String.IsNullOrWhiteSpace(name))
            {
                foreach (App plugin in this.plugins)
                {
                    if (name.Equals(plugin.Name, StringComparison.InvariantCultureIgnoreCase))
                        return true;
                }
            }
            return false;
        }
    }
}

﻿using JeffreyPlease.Logic.Apps.Static;
using JeffreyPlease.UI;
using System.Collections.Generic;

namespace JeffreyPlease.Logic.Apps.SearchContexts
{
    public class ThemeBrowserContext : ISearchContext
    {
        public override List<App> ProvideSearchObjects()
        {
            List<App> apps = new List<App>();

            ThemeManager tm = Store.Get<ThemeManager>();
            foreach (MetaInformation theme in tm.GetThemeInformations())
            {
                if (theme.Name.Equals(tm.ActiveThemeName, System.StringComparison.InvariantCultureIgnoreCase))
                    apps.Insert(0, new ThemePreviewApp(theme));
                else
                    apps.Add(new ThemePreviewApp(theme));
            }

            return apps;
        }
    }
}

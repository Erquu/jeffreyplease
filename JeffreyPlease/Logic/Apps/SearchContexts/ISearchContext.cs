﻿using JeffreyPlease.Logic.Search;
using JeffreyPlease.Settings;
using System;
using System.Collections.Generic;
using System.Threading;

namespace JeffreyPlease.Logic.Apps.SearchContexts
{
    public class SearchHookEventArgs
    {
        internal bool propagationStopped = false;

        public string Pattern { get; set; }
        public List<App> Result { get; private set; }
        public List<App> Apps { get; set; }
        public bool Handled { get; set; }

        public SearchHookEventArgs(string pattern, List<App> foundApps, List<App> apps)
        {
            this.Result = foundApps;
            this.Pattern = pattern;
            this.Apps = apps;
            this.Handled = false;
        }

        public void StopPropagation()
        {
            propagationStopped = false;
        }
    }
    public delegate void SearchHookEventHandler(SearchHookEventArgs ev);

    public delegate void ContextEventCallback();

    public class SearchResultEventArgs
    {
        public App[] Result { get; private set; }

        public SearchResultEventArgs(App[] result)
        {
            this.Result = result;
        }
    }
    public delegate void SearchCompleteEventHandler(SearchResultEventArgs ev);

    /// <summary>
    /// <para>The SearchContext is used to provide a custom set of Apps (Search-objects)</para>
    /// <para>to the search engine</para>
    /// <para>It can hold custom search options</para>
    /// It is used for deep menues (like the plugin-browser)
    /// </summary>
    public abstract class ISearchContext
    {
        /// <summary>
        /// <para>Gets or sets custom search options</para>
        /// By default, the SearchOptions.DEFAULT will be used
        /// </summary>
        public static SearchOptions SearchOptions { get; set; }

        /// <summary>
        /// <para>Fires when a search in this context is complete</para>
        /// This is a thread unsafe event
        /// </summary>
        public event SearchCompleteEventHandler SearchComplete;
        /// <summary>
        /// <para>Fires before the search starts</para>
        /// This is a thread unsafe event
        /// </summary>
        public event SearchHookEventHandler SearchHook_Before;
        /// <summary>
        /// <para>Fires after the search is completed</para>
        /// <para>but before the SearchComplete event is fired</para>
        /// This is a thread unsafe event
        /// </summary>
        public event SearchHookEventHandler SearchHook_After;

        /// <summary>Fires when the context is activated</summary>
        public ContextEventCallback Activated;
        /// <summary>Fires when the context is deactivated</summary>
        public ContextEventCallback Deactivated;

        /// <summary>The search thread</summary>
        private Thread searchThread = null;

        /// <summary>
        /// Fires the SearchComplete event
        /// </summary>
        /// <param name="apps">The search result</param>
        protected void FireSearchComplete(App[] apps)
        {
            FireSearchComplete(new SearchResultEventArgs(apps));
        }
        /// <summary>
        /// Fires the SearchComplete event
        /// </summary>
        /// <param name="args">The search result</param>
        protected void FireSearchComplete(SearchResultEventArgs args)
        {
            if (SearchComplete != null)
                SearchComplete.Invoke(args);
        }

        protected virtual void OnSearchHook_Before(SearchHookEventArgs hookArgs) { }
        protected virtual void OnSearchHook_After(SearchHookEventArgs hookArgs) { }

        /// <summary>
        /// Cleans up all resources used by the context
        /// </summary>
        public void QuitClean()
        {
            if (searchThread != null)
                searchThread.Abort();
        }

        /// <summary>
        /// Returns all objects to search in
        /// </summary>
        /// <returns>All objects to search in</returns>
        //public abstract Dictionary<string, App> ProvideSearchObjects();
        public abstract List<App> ProvideSearchObjects();

        /// <summary>
        /// Searches the context for a given pattern
        /// </summary>
        /// <param name="pattern">The search pattern</param>
        public virtual void Search(string pattern)
        {
            if (searchThread != null)
               searchThread.Abort();
            searchThread = null;

            searchThread = new Thread(new ParameterizedThreadStart(StartSearch));
            searchThread.Start(pattern);
        }

        /// <summary>
        /// <para>Starts the search</para>
        /// This is the target for the search thread
        /// </summary>
        /// <param name="wordObject">The search pattern</param>
        protected virtual void StartSearch(object wordObject)
        {
            string word = wordObject.ToString();
            List<App> foundApps = new List<App>();
            List<App> apps = ProvideSearchObjects();

            if (String.IsNullOrWhiteSpace(word))
            {
                foreach (App app in apps)
                {
                    foundApps.Add(app);
                }
            }
            else
            {
                SearchHookEventArgs hookArgs = new SearchHookEventArgs(word, foundApps, apps);
                OnSearchHook_Before(hookArgs);
                FireDelegate(SearchHook_Before, hookArgs);

                if (!hookArgs.Handled)
                {
                    apps = hookArgs.Apps;

                    try
                    {
                        //FuzzySearch f = new FuzzySearch(apps.Keys.ToList(), options);
                        AppSearch f = new AppSearch(apps);
                        foundApps.AddRange(f.Search(hookArgs.Pattern));
                    }
                    catch (PatternTooLongException) { /* Unhandled */ }
                    catch (KeyNotFoundException ex) { Store.Get<ErrorLog>().HandleError(ex); }
                }

                hookArgs = new SearchHookEventArgs(word, foundApps, apps);
                OnSearchHook_After(hookArgs);
                FireDelegate(SearchHook_After, hookArgs);
            }
            FireSearchComplete(foundApps.ToArray());
        }

        /// <summary>
        /// Invokes a given SearchHookEventHandler
        /// </summary>
        /// <param name="hooks">The SearchHookEventHandler to be invoked</param>
        /// <param name="hookArgs">The SearchHoolEventArgs to be used as parameter for the invokation</param>
        protected void FireDelegate(SearchHookEventHandler hooks, SearchHookEventArgs hookArgs)
        {
            if (hooks != null)
            {
                foreach (Delegate hook in hooks.GetInvocationList())
                {
                    try
                    {
                        if (!hookArgs.propagationStopped)
                            hook.DynamicInvoke(hookArgs);
                    }
                    catch (Exception ex) { Store.Get<ErrorLog>().HandleError(ex); }
                }

            }
        }
    }
}

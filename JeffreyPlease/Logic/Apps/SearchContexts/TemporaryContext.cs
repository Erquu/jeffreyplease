﻿using System.Collections.Generic;

namespace JeffreyPlease.Logic.Apps.SearchContexts
{
    public class TemporaryContext : ISearchContext
    {
        private List<App> context;

        public TemporaryContext()
        {
            context = new List<App>();
        }

        public void Add(App app)
        {
            context.Add(app);
        }

        public override List<App> ProvideSearchObjects()
        {
            return context;
        }
    }
}

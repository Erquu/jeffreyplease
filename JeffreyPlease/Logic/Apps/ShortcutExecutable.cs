﻿using JeffreyPlease.Utils;
using System;
using System.IO;

namespace JeffreyPlease.Logic.Apps
{
    public class ShortcutExecutable : DefaultExecutable
    {
        public FileInfo ResolvedFile { get; private set; }

        public ShortcutExecutable(FileInfo file)
            : base(file)
        {
            string resolvedPath = Tools.ResolveShortcut(file.FullName);

            if (!String.IsNullOrWhiteSpace(resolvedPath) && System.IO.File.Exists(resolvedPath))
            {
                this.ResolvedFile = new FileInfo(resolvedPath);
                this.Description = this.ResolvedFile.FullName;
            }
        }

        public override void Run()
        {
            base.Run();
        }
    }
}

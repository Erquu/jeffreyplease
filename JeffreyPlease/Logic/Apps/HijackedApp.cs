﻿using JeffreyPlease.Logic.Apps.SearchContexts;
using System;

namespace JeffreyPlease.Logic.Apps
{
    public class HijackerApp : App
    {
        protected internal App HijackedApp { get; private set; }

        internal void SetHijackedApp(App hijackedApp)
        {
            if (hijackedApp == null)
                throw new ArgumentNullException("Hijacked App must not be null!");
            this.HijackedApp = hijackedApp;

            this.Name = this.HijackedApp.Name;
            this.Description = this.HijackedApp.Description;
            this.SaveStatistics = this.HijackedApp.SaveStatistics;
            this.Icon = this.HijackedApp.Icon;
            this.Aliases = this.HijackedApp.Aliases;
        }

        public virtual string Name { get; set; }
        public virtual string Description { get; set; }
        public virtual bool SaveStatistics { get; set; }
        public virtual System.Drawing.Icon Icon { get; set; }
        public virtual string[] Aliases
        {
            get { return this.HijackedApp.Aliases; }
            set { this.HijackedApp.Aliases = value; }
        }

        public virtual void Hover()
        {
            this.HijackedApp.Hover();
        }

        public virtual void Unhover()
        {
            this.HijackedApp.Unhover();
        }

        public virtual void Run()
        {
            this.HijackedApp.Run();
        }

        public virtual ISearchContext GetContext()
        {
            return this.HijackedApp.GetContext();
        }
    }
}

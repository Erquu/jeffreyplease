﻿using JeffreyPlease.Logic.Apps;
using JeffreyPlease.Logic.Apps.SearchContexts;
using JeffreyPlease.Settings;
using JSONdotNET;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace JeffreyPlease.Logic
{
    /// <summary>
    /// Callback delegate for the AppStore.StoreCleared event
    /// </summary>
    public delegate void StoreClearedCallback();

    /// <summary>
    /// The AppStore is the (by default) default SearchContext
    /// nothing special so far...
    /// </summary>
    public class AppStore : ISearchContext
    {
        /// <summary>Fires when the store is cleared</summary>
        public event StoreClearedCallback StoreCleared;

        /// <summary>All the searchable apps</summary>
        private Dictionary<string, App> apps = new Dictionary<string, App>();

        /// <summary>
        /// Adds an app to the SearchContext using it's name as the search-key
        /// </summary>
        /// <param name="app">The app to be added</param>
        public void Add(App app)
        {
            Add(app.Name, app);
        }
        /// <summary>
        /// Adds an app to the SeachContext using a given search-key
        /// </summary>
        /// <remarks>Notification: appadded</remarks>
        /// <param name="name">That thing that is used for searching</param>
        /// <param name="app">The app to be added</param>
        public void Add(string name, App app)
        {
            if (!apps.ContainsKey(name))
            {
                apps.Add(name, app);
            }
        }

        /// <summary>
        /// <para>Hijacks an existing app</para>
        /// <para>The default action of the hijacked app is prevented,</para>
        /// instead the new app (HijackerApp) will be called
        /// </summary>
        /// <param name="appToHijack">The app to hijack</param>
        /// <param name="newApp">The app that's taking the jijacked apps place</param>
        /// <returns>true if the hijack was successful</returns>
        /// <exception cref="ArgumentNullException">hijacked app is null</exception>
        public bool Hijack(App appToHijack, HijackerApp newApp)
        {
            KeyValuePair<string, App> pair = apps.FirstOrDefault(v => v.Value == appToHijack);
            if (pair.Key != null)
            {
                newApp.SetHijackedApp(appToHijack);
                apps[pair.Key] = newApp;

                return true;
            }
            return false;
        }

        /// <summary>Returns an app by it's key</summary>
        /// <param name="key">The key of the app</param>
        /// <returns>The app if it exists or null</returns>
        public App Get(string key)
        {
            if (apps.ContainsKey(key))
                return apps[key];
            return null;
        }

        /// <summary>Finds an app by its name</summary>
        /// <param name="name">The name of the app to find</param>
        /// <returns>The found app or null if it does not exist</returns>
        public App Find(string name)
        {
            if (!String.IsNullOrWhiteSpace(name))
            {
                KeyValuePair<string, App> foundApp = apps.FirstOrDefault(v => name.Equals(v.Key, StringComparison.InvariantCultureIgnoreCase) && this.CheckApp(v.Value, name));
                if (foundApp.Value != null)
                {
                    return foundApp.Value;
                }
            }
            return null;
        }

        /// <summary>
        /// <para>Checks if a given app fits to a name,</para>
        /// <para>this comes in handy if an app is hijacked</para>
        /// and you check for the original name
        /// </summary>
        /// <param name="app">The app to check</param>
        /// <param name="name">The name to check against</param>
        /// <returns>True if one part of the app fits to the name</returns>
        private bool CheckApp(App app, string name)
        {
            string appName = app.Name.ToLower();
            if (name != null && name.Equals(appName, StringComparison.InvariantCultureIgnoreCase))
            {
                return true;
            }
            else
            {
                if (app is HijackerApp)
                {
                    // If the app is hijacked, get the original app and check against it
                    return this.CheckApp((app as HijackerApp).HijackedApp, name);
                }
                else
                {
                    // If the app is an executable, the file name will be considered for searching too
                    if (app is ShortcutExecutable)
                    {
                        ShortcutExecutable se = app as ShortcutExecutable;
                        if (se.ResolvedFile != null && name.Equals(se.ResolvedFile.Name, StringComparison.InvariantCultureIgnoreCase))
                        {
                            return true;
                        }
                    }
                    if (app is DefaultExecutable)
                    {
                        DefaultExecutable de = app as DefaultExecutable;
                        if (name.Equals(de.File.Name, StringComparison.InvariantCultureIgnoreCase))
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        /// <summary>Returns all apps by a given type</summary>
        /// <typeparam name="T">The type of the app(s)</typeparam>
        /// <returns>All found apps for the given type</returns>
        public T[] GetByType<T>() where T : App
        {
            return apps.Where(v => (v.Value is T)).Select(v => (T)v.Value).ToArray();
        }

        /// <summary>Clears the app-store</summary>
        public void Clear()
        {
            apps.Clear();
            if (StoreCleared != null)
                StoreCleared.Invoke();
        }

        /// <summary>
        /// <para>Creates a cache file for all current apps</para>
        /// It is used to reduce the initial loading time
        /// </summary>
        public void CacheCurrentState()
        {
            SettingsManager settings = Store.Get<SettingsManager>();
            string cacheFilePath = Path.Combine(settings.SettingsDir.FullName, "appcache.json");
            JSONArray cache = new JSONArray();

            foreach (App app in apps.Values)
            {
                if (app is DefaultExecutable)
                {
                    cache.Add(((DefaultExecutable)app).ToJSONObject());
                }
            }

            settings.Save(cacheFilePath, cache);
        }

        /// <summary>Returns all objects to search in</summary>
        /// <returns>All objects to search in</returns>
        public override List<App> ProvideSearchObjects()
        {
            return new List<App>(apps.Values);
        }
    }
}

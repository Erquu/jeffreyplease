﻿using JeffreyPlease.Logic.Apps.SearchContexts;
using System.Drawing;

namespace JeffreyPlease.Logic
{   
    // TODO: Write summary
    /// <summary></summary>
    public interface App
    {
        /// <summary>Gets the name of the app</summary>
        string Name { get; }
        /// <summary>Gets the description of the app</summary>
        string Description { get; }
        /// <summary>Gets or sets the aliases of the app (altenative names)</summary>
        string[] Aliases { get; set; }
        /// <summary>Gets a value indicating if the input statistics should be saved for this app</summary>
        bool SaveStatistics { get; }
        /// <summary>Gets the icon of the app</summary>
        Icon Icon { get; }

        /// <summary>Runs the main logic for the app</summary>
        void Run();
        /// <summary>Returns a custom search context if provided</summary>
        /// <returns>A new search context or null</returns>
        ISearchContext GetContext();

        /// <summary>Will be called when the app is set active (selected)</summary>
        void Hover();
        /// <summary>Will be called when the app is set inactive (not selected)</summary>
        void Unhover();
    }
}

﻿using JeffreyPlease.UI.Themes;
using JSONdotNET;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace JeffreyPlease.UI
{
    /// <summary>
    /// Callback delegate for ThemeManager's ThemeChanged Event
    /// </summary>
    /// <param name="oldTheme">The old plugin</param>
    /// <param name="newTheme">The new plugin</param>
    public delegate void ThemeChangedCallback(Theme oldTheme, Theme newTheme);

    /// <summary>
    /// Manages all Themes
    /// </summary>
    public class ThemeManager
    {
        /// <summary>Fires if the active plugin changes</summary>
        public event ThemeChangedCallback ThemeChanged;

        /// <summary>
        /// Internal container holding all nescessary information of a plugin
        /// </summary>
        private class ThemeContainer
        {
            /// <summary>The meta information of the plugin</summary>
            public MetaInformation meta;
            /// <summary>The plugin itself</summary>
            public Theme theme;
            /// <summary>Indicator if plugin was loaded</summary>
            public bool loaded;
        }

        /// <summary>Store of all themes identified by their names</summary>
        private Dictionary<string, ThemeContainer> themes = new Dictionary<string, ThemeContainer>();
        /// <summary>The name of the currently active plugin</summary>
        private string _activeThemeName = "Simple & Light";
        /// <summary>The active plugin</summary>
        private Theme _activeTheme;
        /// <summary>Link to the internal themes directory</summary>
        private DirectoryInfo themesDir;

        /// <summary>Indicator if livestyle is enabled</summary>
        private bool liveStyle = false;
        /// <summary>FileSytemWatcher required if livestyle is enabled (to watch for plugin changes)</summary>
        private FileSystemWatcher liveStyleWatcher;

        /// <summary>
        /// Returns the currently active plugin
        /// </summary>
        public Theme ActiveTheme
        {
            get
            {
                return _activeTheme;
            }
            private set
            {
                Theme oldTheme = _activeTheme;
                _activeTheme = value;
                if (ThemeChanged != null)
                    ThemeChanged.Invoke(oldTheme, _activeTheme);
            }
        }

        /// <summary>
        /// Gets the name of the currently active theme
        /// </summary>
        public string ActiveThemeName
        {
            get
            {
                return _activeThemeName;
            }
        }

        /// <summary>
        /// Returns true if livestyle is enabled
        /// </summary>
        public bool IsLiveStyling { get { return liveStyle; } }

        /// <summary>
        /// Creates a new instance of the ThemeManager loading themes from the given dir
        /// </summary>
        /// <param name="themesDir">The directory from where the themes are loaded</param>
        public ThemeManager(DirectoryInfo themesDir)
        {
            this.themesDir = themesDir;

            this._activeTheme = new SimpleLightTheme();
            this.themes.Add("Simple & Light", new ThemeContainer()
            {
                meta = new MetaInformation("Simple & Light", "A minimal white theme", "Der Entwickler", Plugin.PluginType.THEME),
                theme = this._activeTheme
            });
            this.themes.Add("Simple & Dark", new ThemeContainer()
            {
                meta = new MetaInformation("Simple & Dark", "A minimal gray theme", "Der Entwickler", Plugin.PluginType.THEME),
                theme = new SimpleDarkTheme()
            });
        }

        /// <summary>
        /// Returns a list of all loaded themes
        /// </summary>
        /// <returns></returns>
        public List<MetaInformation> GetThemeInformations()
        {
            return this.themes.Select(v => v.Value.meta).ToList();
        }

        /// <summary>
        /// Checks if a plugin with a given name exists
        /// </summary>
        /// <param name="name">The name of the plugin</param>
        /// <returns>True if the plugin exists</returns>
        public bool Exists(string name)
        {
            return themes.ContainsKey(name);
        }

        /// <summary>Enables livestyle</summary>
        public void EnableLiveStyle()
        {
            this.liveStyle = true;
            this.liveStyleWatcher = new FileSystemWatcher(this.themesDir.FullName);
            this.liveStyleWatcher.Changed += new FileSystemEventHandler(liveStyleWatcher_Changed);
            this.liveStyleWatcher.Renamed += new RenamedEventHandler(liveStyleWatcher_Renamed);
            this.liveStyleWatcher.EnableRaisingEvents = true;
        }

        /// <summary>Disables livestyle</summary>
        public void DisableLiveStyle()
        {
            if (liveStyle)
            {
                liveStyleWatcher.EnableRaisingEvents = false;
                liveStyleWatcher = null;
                liveStyle = false;
            }
        }

        /// <summary>
        /// Sets a plugin with a given name active
        /// </summary>
        /// <param name="name">The name of the plugin to be activated</param>
        public void SetActive(string name)
        {
            if (name != _activeThemeName && Exists(name))
            {
                ThemeContainer container = themes[name];
                if (!container.loaded)
                {
                    container.theme.Load();
                    container.loaded = true;
                }

                _activeThemeName = name;
                ActiveTheme = container.theme;
            }
        }
        public void SetActive(Theme theme)
        {
            if (theme != null)
            {
                ActiveTheme = theme;
            }
        }

        public void AddTheme(string name, string description, string author, Theme theme)
        {
            if (!themes.ContainsKey(name))
            {
                ThemeContainer tc = new ThemeContainer();
                tc.meta = new MetaInformation(name, description, author, Plugin.PluginType.THEME);
                tc.theme = theme;

                themes.Add(name, tc);
            }
        }
        public void AddTheme(JSONObject theme)
        {
            LoadFromJSON(theme);
        }

        private ThemeContainer LoadFromJSON(JSONObject theme)
        {
            string name = "Unnamed Theme";
            string description = null;
            string author = null;

            if (theme.ContainsKey("name"))
            {
                name = theme.GetString("name");
            }

            if (theme.ContainsKey("description"))
                description = theme.GetString("description");
            if (theme.ContainsKey("author"))
                author = theme.GetString("author");

            Debug.WriteLine("Loading Theme " + name);

            ThemeContainer tc = new ThemeContainer();
            tc.meta = new MetaInformation(name, description, author, Plugin.PluginType.THEME);
            tc.theme = new JSONTheme(theme);

            if (!themes.ContainsKey(name))
                themes.Add(name, tc);
            else
                themes[name] = tc;

            return tc;
        }

        /// <summary>
        /// Loads a plugin file
        /// </summary>
        /// <param name="themeFile">The FileInfo for the themefile to be loaded</param>
        /// <returns>The ThemeContainer for the loaded plugin or null in case of errors</returns>
        private ThemeContainer LoadFromFile(FileInfo themeFile)
        {
            if (themeFile.Exists && ".jeffreytheme".Equals(themeFile.Extension.ToLower()))
            {
                Stream fileInputStream = new FileStream(themeFile.FullName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                JSONObject theme = JSON.Parse(fileInputStream);
                fileInputStream.Close();

                return LoadFromJSON(theme);
            }
            return null;
        }

        private void liveStyleWatcher_Renamed(object sender, RenamedEventArgs e)
        {
            Debug.WriteLine(e.Name + " renamed");
            ThemeContainer container = LoadFromFile(new FileInfo(e.FullPath));
            if (container != null)
            {
                container.theme.Load();
                ActiveTheme = container.theme;
            }
        }

        private void liveStyleWatcher_Changed(object sender, FileSystemEventArgs e)
        {
            Debug.WriteLine(e.Name + " changed");
            ThemeContainer container = LoadFromFile(new FileInfo(e.FullPath));
            if (container != null)
            {
                container.theme.Load();
                ActiveTheme = container.theme;
            }
        }
    }
}

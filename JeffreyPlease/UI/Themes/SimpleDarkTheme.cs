﻿using System.Drawing;

namespace JeffreyPlease.UI.Themes
{
    public class SimpleDarkTheme :  Theme
    {
        public override void Load()
        {
            Width = 600;

            Padding = 1;
            Opacity = 0.98d;

            SearchTextColor = Color.FromArgb(34, 34, 34);
            PaddingColor = SearchTextColor;

            Background = new SolidBrush(SearchTextColor);
            SearchBackground = SearchTextColor;
            ResultBackground = Background;
            SelectedResultBackground = Background;

            SearchTextColor = Color.White;
            SelectedResultTextColor = Brushes.White;

            ResultTextColor = new SolidBrush(Color.FromArgb(136, 136, 136));
            ResultSubtextColor = ResultTextColor;

            BorderColorTop = Background;
            BorderColorBottom = Background;

            ShortcutColor = Brushes.White;
            SelectedShortcutColor = Brushes.White;

            CornerRoundness = 2;
            InnerCornerRoundness = 2;
        }
    }
}

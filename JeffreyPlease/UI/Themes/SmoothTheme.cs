﻿using System.Drawing;

namespace JeffreyPlease.UI.Themes
{
    public class SmoothTheme : Theme
    {
        public SmoothTheme() { }

        public override void Load()
        {
            CornerRoundness = 0;
            InnerCornerRoundness = 0;
            Background = new SolidBrush(Color.FromArgb(255, 203, 144));
            Padding = 10;
            InnerPadding = 10;
            SearchPadding = 10;
            Width = 500;

            ResultTopMargin = 10;

            PaddingColor = Color.FromArgb(94, 51, 0);

            SearchBackground = Color.FromArgb(110, 173, 151);
            SearchFont = new Font("Arial", 30.0f, FontStyle.Bold);
            SearchTextColor = Color.White;

            ResultHeight = 50;
            ResultBackground = new SolidBrush(PaddingColor);
            SelectedResultBackground = new SolidBrush(Color.FromArgb(234, 95, 105));

            ResultFont = new Font("Arial", 13.0f, FontStyle.Bold);
            ResultTextColor = Background;

            SelectedResultFont = ResultFont;
            SelectedResultTextColor = new SolidBrush(Color.FromArgb(240, 240, 240));

            ResultSubFont = new Font("Arial", 11.0f, FontStyle.Regular);
            ResultSubtextColor = new SolidBrush(Color.FromArgb(193, 120, 35));

            SelectedResultSubFont = ResultSubFont;
            SelectedResultSubtextColor = Brushes.White;

            ShortcutFont = ResultFont;
            ShortcutColor = SelectedResultTextColor;

            SelectedShortcutFont = new Font("Arial", 20.0f, FontStyle.Bold);
            SelectedShortcutColor = ShortcutColor;

            BorderColorTop = new SolidBrush(Color.FromArgb(200, 67, 105));
            BorderColorBottom = ResultBackground;
        }
    }
}

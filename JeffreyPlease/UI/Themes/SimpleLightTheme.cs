﻿using System.Drawing;

namespace JeffreyPlease.UI.Themes
{
    public class SimpleLightTheme :  Theme
    {
        public override void Load()
        {
            Width = 600;

            Background = new SolidBrush(Color.FromArgb(153, 153, 153));
            PaddingColor = Color.FromArgb(250, 250, 250);
            Padding = 1;

            ResultBackground = new SolidBrush(PaddingColor);
            SearchBackground = PaddingColor;
            BorderColorTop = ResultBackground;
            BorderColorBottom = ResultBackground;

            SelectedResultTextColor = ResultBackground;
            ResultSubtextColor = ResultBackground;

            SelectedResultBackground = new SolidBrush(Color.FromArgb(150, 217, 242));
            SelectedResultSubtextColor = SelectedResultBackground;

            CornerRoundness = 2;
            InnerCornerRoundness = 2;
        }
    }
}

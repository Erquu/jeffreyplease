﻿using JeffreyPlease.Logic;
using JeffreyPlease.Settings;
using System;
using System.Drawing;
using System.Linq;

namespace JeffreyPlease.UI
{
    /// <summary>
    /// Class for rendering default apps
    /// </summary>
    public static class AppRenderer
    {
        /// <summary>
        /// The string format used for cutting too long text
        /// </summary>
        private static StringFormat format = new StringFormat()
        {
            Trimming = StringTrimming.EllipsisCharacter
        };

        /// <summary>
        /// Creates the shortcut string for a given index
        /// </summary>
        /// <param name="index">The zero based index of the app</param>
        /// <param name="active">True if the app is selected</param>
        /// <returns>The shortcur string</returns>
        private static string CreateSmallString(int index, bool active)
        {
            return active ? "Enter" : ("STRG + " + (index + 1));
        }

        /// <summary>
        /// Renders an default app
        /// </summary>
        /// <param name="g">The graphics instance to render to</param>
        /// <param name="x">The x origin</param>
        /// <param name="y">The y origin</param>
        /// <param name="width">The width of the app</param>
        /// <param name="index">The zero based index of the app</param>
        /// <param name="app">The app itself</param>
        /// <param name="plugin">The plugin, the app should be rendered with</param>
        /// <param name="active">True if the app should be rendered as active</param>
        public static void Render(Graphics g, int x, int y, int width, int index, App app, Theme theme, bool active)
        {
            int height = theme.ResultHeight;
            int bottom = y + height;

            Brush bgBrush = active ? theme.SelectedResultBackground : theme.ResultBackground;

            Font resultFont = active ? theme.SelectedResultFont : theme.ResultFont;
            Brush resultTextColor = active ? theme.SelectedResultTextColor : theme.ResultTextColor;

            Font resultSubFont = active ? theme.SelectedResultSubFont : theme.ResultSubFont;
            Brush resultSubtextColor = active ? theme.SelectedResultSubtextColor : theme.ResultSubtextColor;

            Font shortcutFont = active ? theme.SelectedShortcutFont : theme.ShortcutFont;
            Brush shortcutTextColor = active ? theme.SelectedShortcutColor : theme.ShortcutColor;

            string smallString = AppRenderer.CreateSmallString(index, active);

            int offsetX = 5;

            if (bgBrush != Brushes.Transparent)
                g.FillRectangle(bgBrush, 0, y, width, height);

            if (theme.ShowIcon)
            {
                Icon icon = app.Icon;
                if (icon != null)
                {
                    try
                    {
                        // TODO: Find out how a dividebyzeroexception is thrown here sometimes
                        g.DrawIcon(icon, new Rectangle(offsetX, y + 10, height - 15, height - 15));
                    }
                    catch (DivideByZeroException e) { Store.Get<ErrorLog>().HandleError(e); }
                }
                offsetX += (height - 15);
            }

            offsetX += theme.ResultIconMargin;

            SizeF size = g.MeasureString(app.Name, resultFont);
            SizeF descriptionSize = g.MeasureString(app.Description, resultSubFont);
            SizeF smallSize = g.MeasureString(smallString, shortcutFont);

            Point resultPosition = new Point(offsetX, y + 5);
            Point descriptionPosition = new Point(offsetX, y + 5 + (int)size.Height);
            Point shortcutPosition = new Point((width - (int)smallSize.Width - 10), y + 5);

            string appName = app.Name;
            if (appName.Contains('.'))
                appName = appName.Substring(0, appName.LastIndexOf('.'));

            g.DrawString(
                appName,
                resultFont,
                resultTextColor,
                new Rectangle(resultPosition.X, resultPosition.Y, (width - (int)smallSize.Width - resultPosition.X - 10), (int)size.Height),
                format);
            g.DrawString(
                app.Description,
                resultSubFont,
                resultSubtextColor,
                new Rectangle(descriptionPosition.X, descriptionPosition.Y, (width - (int)smallSize.Width - descriptionPosition.X - 10), (int)descriptionSize.Height),
                format);

            g.DrawString(smallString, shortcutFont, shortcutTextColor, shortcutPosition);

            g.DrawLine(new Pen(theme.BorderColorTop), 0, y, width, y);
            g.DrawLine(new Pen(theme.BorderColorBottom), 0, y + height - 1, width, y + height - 1);
        }
    }
}

﻿using System.Drawing;

namespace JeffreyPlease.UI
{
    /// <summary>A new suit for Jeffrey</summary>
    public class Theme
    {
        /// <summary>The general opacity of the input form</summary>
        private double _opacity = 1.0d;
        /// <summary>
        /// Gets or sets the opacity of the input form
        /// </summary>
        public virtual double Opacity { get { return _opacity; } set { _opacity = value; } }

        /// <summary>Gets or sets an indicator if the icon should be drawn</summary>
        public virtual bool ShowIcon { get; set; }
        /// <summary>Gets or sets the maximum visible number of search results</summary>
        public virtual int ResultCount { get; set; }

        /// <summary>Gets or sets the corner roundness of the input form</summary>
        public virtual int CornerRoundness { get; set; }
        /// <summary>Gets or sets the corner roundness of the inner box</summary>
        public virtual int InnerCornerRoundness { get; set; }
        /// <summary>Gets or sets the form background</summary>
        public virtual Brush Background { get; set; }
        /// <summary>Gets or sets the forms outer padding</summary>
        public virtual int Padding { get; set; }
        /// <summary>Gets or sets the forms inner padding</summary>
        public virtual int InnerPadding { get; set; }
        /// <summary>Gets or sets the searchfields padding</summary>
        public virtual int SearchPadding { get; set; }
        /// <summary>Gets or sets the size of the gap between the searchbar and the first search result</summary>
        public virtual int ResultTopMargin { get; set; }
        /// <summary>Gets or sets the size of the gap between the icon and the text in the search results</summary>
        public virtual int ResultIconMargin { get; set; }
        /// <summary>Gets or sets the width of the frame</summary>
        public virtual int Width { get; set; }

        /// <summary>Gets or sets color of the inner padding</summary>
        public virtual Color PaddingColor { get; set; }

        /// <summary>Gets or sets the color of the Searchbar</summary>
        public virtual Color SearchBackground { get; set; }
        /// <summary>Gets or sets the font of the searchbar-text</summary>
        public virtual Font SearchFont { get; set; }
        /// <summary>Gets or sets the color of the searchbar-text</summary>
        public virtual Color SearchTextColor { get; set; }

        /// <summary>Gets or sets the height of a search result</summary>
        public virtual int ResultHeight { get; set; }
        /// <summary>Gets or sets the background of a search result</summary>
        public virtual Brush ResultBackground { get; set; }
        /// <summary>Gets or sets the background of an active search result</summary>
        public virtual Brush SelectedResultBackground { get; set; }

        /// <summary>Gets or sets an indicator if the scrollbar should be rendered</summary>
        public virtual bool ScrollbarVisible { get; set; }
        /// <summary>Gets or sets the left padding of the scrollbar</summary>
        public virtual int ScrollbarPaddingLeft { get; set; }
        /// <summary>Gets or sets the right padding of the scrollbar</summary>
        public virtual int ScrollbarPaddingRight { get; set; }
        /// <summary>Gets or sets the width of the scrollbar</summary>
        public virtual int ScrollbarWidth { get; set; }
        /// <summary>Gets or sets the color of the scrollbar</summary>
        public virtual Brush ScrollbarColor { get; set; }

        /// <summary>Gets or sets the font of the search result main text</summary>
        public virtual Font ResultFont { get; set; }
        /// <summary>Gets or sets the color of the search result main text</summary>
        public virtual Brush ResultTextColor { get; set; }

        /// <summary>Gets or sets the font of the active search results main text</summary>
        public virtual Font SelectedResultFont { get; set; }
        /// <summary>Gets or sets the color of the active search results main text</summary>
        public virtual Brush SelectedResultTextColor { get; set; }

        /// <summary>Gets or sets the font of the search result description</summary>
        public virtual Font ResultSubFont { get; set; }
        /// <summary>Gets or sets the color of the search result description</summary>
        public virtual Brush ResultSubtextColor { get; set; }

        /// <summary>Gets or sets the font of an active search results description</summary>
        public virtual Font SelectedResultSubFont { get; set; }
        /// <summary>Gets or sets the text color of an active search results description</summary>
        public virtual Brush SelectedResultSubtextColor { get; set; }

        /// <summary>Gets or sets the font of the shortcut text</summary>
        public virtual Font ShortcutFont { get; set; }
        /// <summary>Gets or sets the color of the shortcut text</summary>
        public virtual Brush ShortcutColor { get; set; }

        /// <summary>Gets or sets the font of an active shortcut text</summary>
        public virtual Font SelectedShortcutFont { get; set; }
        /// <summary>Gets or sets the color of an active shortcut text</summary>
        public virtual Brush SelectedShortcutColor { get; set; }

        /// <summary>Gets or sets the border color of the search results upper border</summary>
        public virtual Brush BorderColorTop { get; set; }
        /// <summary>Gets or sets the vorder color of the search results lower border</summary>
        public virtual Brush BorderColorBottom { get; set; }

        /// <summary>Loads the plugin</summary>
        public virtual void Load() { }

        /// <summary>Creates a new instance of a default with the default values</summary>
        public Theme()
        {
            CornerRoundness = 2;
            InnerCornerRoundness = CornerRoundness;

            PaddingColor = Color.FromArgb(250, 250, 250);
            Background = new SolidBrush(Color.FromArgb(153, 153, 153));
            Padding = 1;
            InnerPadding = 10;
            SearchPadding = 5;
            Width = 600;
            Opacity = 1;

            ShowIcon = true;

            ResultCount = 7;
            ResultTopMargin = 0;
            ResultIconMargin = 0;

            SearchBackground = PaddingColor;
            SearchFont = new System.Drawing.Font("Arial", 22F);
            SearchTextColor = Color.FromArgb(60, 60, 60);

            ResultHeight = 50;
            ResultBackground = new SolidBrush(PaddingColor);
            SelectedResultBackground = new SolidBrush(Color.FromArgb(150, 217, 242));

            ScrollbarVisible = true;
            ScrollbarPaddingLeft = 2;
            ScrollbarPaddingRight = 2;
            ScrollbarWidth = 2;
            ScrollbarColor = Brushes.DarkGray;

            ResultFont = new Font("Arial", 13.0f, FontStyle.Bold);
            ResultTextColor = new SolidBrush(SearchTextColor);

            SelectedResultFont = ResultFont;
            SelectedResultTextColor = ResultBackground;

            ResultSubFont = new Font("Arial", 11.0f, FontStyle.Regular);
            ResultSubtextColor = ResultBackground;

            SelectedResultSubFont = ResultSubFont;
            SelectedResultSubtextColor = SelectedResultBackground;

            ShortcutFont = ResultFont;
            ShortcutColor = ResultTextColor;

            SelectedShortcutFont = ShortcutFont;
            SelectedShortcutColor = ShortcutColor;

            BorderColorTop = ResultBackground;
            BorderColorBottom = ResultBackground;
        }
    }
}

﻿using JeffreyPlease.Logic;
using JeffreyPlease.Utils;
using System;
using System.Windows.Forms;

namespace JeffreyPlease.UI
{
    /// <summary>
    /// Panel to enable a scrollable search result
    /// </summary>
    public class ScrollableAppPanel : Panel
    {
        /// <summary>
        /// Gets the currently selected app
        /// </summary>
        public App SelectedApp
        {
            get
            {
                if (currentApps != null && currentApps.Length > 0)
                {
                    int index = scrollIndex + selectionIndex;
                    if (MathHelper.Between(0, index, currentApps.Length))
                    {
                        return currentApps[index];
                    }
                }
                return null;
            }
        }

        /// <summary>The app which was selected the last time</summary>
        private App lastSelectedApp = null;
        /// <summary>The apps which are in the current list</summary>
        private App[] currentApps;
        /// <summary>The number of apps (same as currentApps.Length)</summary>
        private int appCount;

        /// <summary>The plugin, the apps should be rendered with</summary>
        private Theme theme;
        /// <summary>The index of the selected app</summary>
        private int selectionIndex = 0;
        /// <summary>The current scroll position</summary>
        private int scrollIndex = 0;

        /// <summary>Creates a new Panel, renders using a given plugin</summary>
        /// <param name="plugin">The plugin, the apps should be rendered with</param>
        public ScrollableAppPanel(Theme theme)
        {
            //SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer | ControlStyles.UserPaint, true);
            this.SetStyle((ControlStyles)204802, true);

            this.UpdateTheme(theme);
        }

        /// <summary>
        /// Returns the app with the given, zero based index
        /// </summary>
        /// <param name="index">The zero based index of the app</param>
        /// <returns>The app or null if the index is invalid</returns>
        public App GetAppByVisibleIndex(int index)
        {
            if (MathHelper.Between(0, index, this.theme.ResultCount) && MathHelper.Between(0, this.scrollIndex + index, this.appCount))
                return this.currentApps[(scrollIndex + index)];
            return null;
        }

        /// <summary>Removes all apps from the list</summary>
        public void Clear()
        {
            this.selectionIndex = 0;
            this.scrollIndex = 0;

            this.currentApps = new App[0];
            this.appCount = 0;

            this.Invalidate();
        }

        /// <summary>Updates the plugin</summary>
        /// <param name="plugin">The new plugin</param>
        public void UpdateTheme(Theme theme)
        {
            this.theme = theme;
        }

        /// <summary>
        /// Updates the list of apps
        /// </summary>
        /// <param name="apps">The new list of apps</param>
        public void UpdateApps(App[] apps)
        {
            this.currentApps = apps;
            this.appCount = this.currentApps.Length;

            this.selectionIndex = 0;
            this.scrollIndex = 0;

            this.Invalidate();
        }

        /// <summary>
        /// Scrolls using the given range
        /// </summary>
        /// <param name="count">The number of scroll steps (e.g. 1 to scroll down, -1 to scroll up)</param>
        public new void Scroll(int count)
        {
            int newIndex = (this.scrollIndex + this.selectionIndex + count);

            if (newIndex < 0)
                newIndex = this.appCount - 1;
            else if (newIndex >= this.appCount)
                newIndex = 0;

            this.Select(newIndex);
        }

        /// <summary>
        /// Scrolls to and selects the app with the given index
        /// </summary>
        /// <param name="index">The index of the selected index</param>
        public void Select(int index)
        {
            if (this.appCount > 0)
            {
                index = MathHelper.Clamp(0, index, this.appCount);

                if (lastSelectedApp != null)
                    lastSelectedApp.Unhover();
                lastSelectedApp = currentApps[index];
                lastSelectedApp.Hover();

                int resultCount = this.theme.ResultCount;

                if (index >= this.scrollIndex + resultCount)
                {
                    this.selectionIndex = resultCount - 1;
                    this.scrollIndex = index - resultCount + 1;
                }
                else if (index < scrollIndex)
                {
                    this.selectionIndex = 0;
                    this.scrollIndex = index;
                }
                else
                {
                    this.selectionIndex = index - this.scrollIndex;
                }

                this.Invalidate();
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            //e.Graphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            e.Graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBilinear;
            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            if (currentApps != null)
            {
                bool showScrollbar = this.theme.ScrollbarVisible && (this.appCount > this.theme.ResultCount);

                int topMargin = this.theme.ResultTopMargin;
                int scrollbarWidth = this.theme.ScrollbarWidth;
                int scrollbarPaddingRight = this.theme.ScrollbarPaddingRight;
                int scrollbarTotalSize = this.theme.ScrollbarPaddingLeft + scrollbarWidth + scrollbarPaddingRight;

                int offsetY = topMargin;
                int height = this.theme.ResultHeight;
                int width = this.Width;
                int buttonWidth = showScrollbar ? width - scrollbarTotalSize - 1 : width;

                int visibleApps = Math.Min(this.appCount, this.theme.ResultCount);

                for (int i = 0; i < visibleApps; i++)
                {
                    AppRenderer.Render(
                        e.Graphics,
                        0,
                        offsetY,
                        buttonWidth,
                        i,
                        currentApps[this.scrollIndex + i],
                        this.theme,
                        (i == this.selectionIndex));
                    offsetY += height;
                }

                if (showScrollbar)
                {
                    int buttonHeight = this.theme.ResultHeight;
                    int totalHeight = this.appCount * buttonHeight;

                    double relativeTotalHeight = (double)(visibleApps * buttonHeight) / (double)totalHeight;
                    double test = (double)relativeTotalHeight * buttonHeight;

                    e.Graphics.FillRectangle(
                        this.theme.ScrollbarColor,
                        width - scrollbarWidth - scrollbarPaddingRight - 1,
                        topMargin + Convert.ToInt32(test * this.scrollIndex),
                        scrollbarWidth,
                        Convert.ToInt32(test * visibleApps));
                }
            }
            else
            {
                base.OnPaint(e);
            }
        }
    }
}

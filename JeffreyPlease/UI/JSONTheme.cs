﻿using JSONdotNET;
using System;
using System.Drawing;
using System.Text.RegularExpressions;

namespace JeffreyPlease.UI
{
    /// <summary>
    /// Class for loading a plugin from a file
    /// </summary>
    public class JSONTheme : Theme
    {
        /// <summary>The plugin file information</summary>
        protected JSONObject themeObject;

        protected JSONTheme()
        {

        }
        /// <summary>
        /// Creates a new instance of FileTheme
        /// </summary>
        /// <param name="themeFile">The plugin file information</param>
        public JSONTheme(JSONObject themeObject)
        {
            this.themeObject = themeObject;
        }

        /// <summary>
        /// Loads the plugin from the file
        /// </summary>
        public override void Load()
        {
            if (this.themeObject != null)
            {
                Color color;
                Brush brush;

                // COLORS
                if (GetBrush("background", themeObject, out brush, this.Background))
                    this.Background = brush;
                if (GetColor("paddingColor", themeObject, out color, this.PaddingColor))
                    this.PaddingColor = color;
                if (GetColor("searchBackground", themeObject, out color, this.SearchBackground))
                    this.SearchBackground = color;
                if (GetBrush("resultBackground", themeObject, out brush, this.ResultBackground))
                    this.ResultBackground = brush;
                if (GetBrush("selectedResultBackground", themeObject, out brush, this.SelectedResultBackground))
                    this.SelectedResultBackground = brush;
                if (GetColor("searchTextColor", themeObject, out color, this.SearchTextColor))
                    this.SearchTextColor = color;
                if (GetBrush("resultTextColor", themeObject, out brush, this.ResultTextColor))
                    this.ResultTextColor = brush;
                if (GetBrush("selectedResultTextColor", themeObject, out brush, this.SelectedResultTextColor))
                    this.SelectedResultTextColor = brush;
                if (GetBrush("resultSubtextColor", themeObject, out brush, this.ResultSubtextColor))
                    this.ResultSubtextColor = brush;
                if (GetBrush("selectedResultSubtextColor", themeObject, out brush, this.SelectedResultSubtextColor))
                    this.SelectedResultSubtextColor = brush;
                if (GetBrush("shortcutColor", themeObject, out brush, this.ShortcutColor))
                    this.ShortcutColor = brush;
                if (GetBrush("selectedShortcutColor", themeObject, out brush, this.SelectedShortcutColor))
                    this.SelectedShortcutColor = brush;
                if (GetBrush("borderColorTop", themeObject, out brush, this.BorderColorTop))
                    this.BorderColorTop = brush;
                if (GetBrush("borderColorBottom", themeObject, out brush, this.BorderColorBottom))
                    this.BorderColorBottom = brush;
                if (GetBrush("scrollbarColor", themeObject, out brush, this.ScrollbarColor))
                    this.ScrollbarColor = brush;

                // FONTS
                this.SearchFont = GetFont("search", themeObject, this.SearchFont);
                this.ResultFont = GetFont("result", themeObject, this.ResultFont);
                this.ResultSubFont = GetFont("resultSub", themeObject, this.ResultSubFont);
                this.ShortcutFont = GetFont("shortcut", themeObject, this.ResultSubFont);
                this.SelectedResultFont = GetFont("selectedResult", themeObject, this.SelectedResultFont);
                this.SelectedResultSubFont = GetFont("selectedResultSub", themeObject, this.SelectedResultSubFont);
                this.SelectedShortcutFont = GetFont("selectedShortcut", themeObject, this.ResultSubFont);

                // SIZING AND OTHER STUFF
                if (themeObject.ContainsKey("showIcon"))
                    this.ShowIcon = themeObject.GetBool("showIcon");
                if (themeObject.ContainsKey("opacity"))
                    this.Opacity = themeObject.GetDouble("opacity");
                if (themeObject.ContainsKey("cornerRoundness"))
                    this.CornerRoundness = themeObject.GetInt("cornerRoundness");
                if (themeObject.ContainsKey("innerCornerRoundness"))
                    this.InnerCornerRoundness = themeObject.GetInt("innerCornerRoundness");
                if (themeObject.ContainsKey("padding"))
                    this.Padding = themeObject.GetInt("padding");
                if (themeObject.ContainsKey("innerPadding"))
                    this.InnerPadding = themeObject.GetInt("innerPadding");
                if (themeObject.ContainsKey("searchPadding"))
                    this.SearchPadding = themeObject.GetInt("searchPadding");
                if (themeObject.ContainsKey("width"))
                    this.Width = themeObject.GetInt("width");
                if (themeObject.ContainsKey("resultHeight"))
                    this.ResultHeight = themeObject.GetInt("resultHeight");
                if (themeObject.ContainsKey("resultTopMargin"))
                    this.ResultTopMargin = themeObject.GetInt("resultTopMargin");
                if (themeObject.ContainsKey("resultIconMargin"))
                    this.ResultIconMargin = themeObject.GetInt("resultIconMargin");
                if (themeObject.ContainsKey("scrollbarVisible"))
                    this.ScrollbarVisible = themeObject.GetBool("scrollbarVisible");
                if (themeObject.ContainsKey("scrollbarPaddingLeft"))
                    this.ScrollbarPaddingLeft = themeObject.GetInt("scrollbarPaddingLeft");
                if (themeObject.ContainsKey("scrollbarPaddingRight"))
                    this.ScrollbarPaddingRight = themeObject.GetInt("scrollbarPaddingRight");
                if (themeObject.ContainsKey("scrollbarWidth"))
                    this.ScrollbarWidth = themeObject.GetInt("scrollbarWidth");

                this.themeObject = null;
            }
        }

        /// <summary>
        /// Parses a font from a JSONObject
        /// </summary>
        /// <param name="name">The key prefix for the font settings</param>
        /// <param name="obj">The JSONObject to read from</param>
        /// <param name="def">The default font which will be used if errors occur</param>
        /// <returns>A Font...</returns>
        protected Font GetFont(string name, JSONObject obj, Font def)
        {
            string family = def.FontFamily.Name;
            float size = def.Size;
            FontStyle style = def.Style;

            if (obj.ContainsKey(name + "FontFamily"))
                family = obj.GetString(name + "FontFamily");
            if (obj.ContainsKey(name + "FontSize"))
                size = obj.GetFloat(name + "FontSize");
            if (obj.ContainsKey(name + "FontStyle"))
            {
                FontStyle newStyle = FontStyle.Regular;
                JSONObject styleObj = obj.Get(name + "FontStyle");
                if (styleObj is JSONArray)
                {
                    JSONArray styleArr = (JSONArray)styleObj;
                    for (int i = 0; i < styleArr.Length; i++)
                    {
                        AddFontStyle(styleArr[i].ToString(), ref newStyle);
                    }
                }
                else if (styleObj is JSONTextNode)
                {
                    AddFontStyle(styleObj.ToString(), ref newStyle);
                }
                style = newStyle;
            }

            return new Font(family, size, style);
        }

        /// <summary>
        /// <para>Adds a font style identified by the name</para>
        /// <para>Possible values are:</para>
        /// <para>bold</para>
        /// <para>italic</para>
        /// <para>strikeout</para>
        /// underline
        /// </summary>
        /// <param name="style">The style to be added</param>
        /// <param name="fontStyle">The FontStyle to which the new style should be added to</param>
        protected void AddFontStyle(string style, ref FontStyle fontStyle)
        {
            switch (style.ToLower())
            {
                case "bold":
                    fontStyle |= FontStyle.Bold;
                    break;
                case "italic":
                    fontStyle |= FontStyle.Italic;
                    break;
                case "strikeout":
                    fontStyle |= FontStyle.Strikeout;
                    break;
                case "underline":
                    fontStyle |= FontStyle.Underline;
                    break;
            }
        }


        /// <summary>
        /// Parses a SolidBrush from a JSONObject
        /// </summary>
        /// <param name="name">The key of the json color field</param>
        /// <param name="obj">The JSONObject to read from</param>
        /// <param name="color">The result brush</param>
        /// <param name="def">The default brush which will be used if an error occurs</param>
        /// <returns>True on success</returns>
        protected bool GetBrush(string name, JSONObject obj, out Brush brush, Brush def)
        {
            Color color = Color.Pink;
            if (GetColor(name, obj, out color, color))
            {
                brush = new SolidBrush(color);
                return true;
            }
            brush = def;
            return false;
        }

        /// <summary>
        /// Parses a color from a JSONObject
        /// </summary>
        /// <param name="name">The key of the json color field</param>
        /// <param name="obj">The JSONObject to read from</param>
        /// <param name="color">The resulting color</param>
        /// <param name="def">The default color which will be used if an error occurs</param>
        /// <returns>True on success</returns>
        protected bool GetColor(string name, JSONObject obj, out Color color, Color def)
        {
            Color outColor = def;
            bool success = false;

            if ("transparent".Equals(name.ToLower()))
            {
                color = Color.Transparent;
                return true;
            }


            if (obj.ContainsKey(name))
            {
                JSONObject c = obj.Get(name);
                if (c is JSONArray)
                {
                    JSONArray colorArray = (JSONArray)c;
                    if (colorArray.Length == 1)
                    {
                        outColor = Color.FromArgb(
                            Convert.ToInt32(colorArray[0].ToString()),
                            Convert.ToInt32(colorArray[0].ToString()),
                            Convert.ToInt32(colorArray[0].ToString()));
                        success = true;
                    }
                    else if (colorArray.Length == 3)
                    {
                        outColor = Color.FromArgb(
                            Convert.ToInt32(colorArray[0].ToString()),
                            Convert.ToInt32(colorArray[1].ToString()),
                            Convert.ToInt32(colorArray[2].ToString()));
                        success = true;
                    }
                }
                else if (c is JSONTextNode)
                {
                    string colorString = c.ToString();
                    if (Regex.IsMatch(colorString, @"^#[\dA-F]+$", RegexOptions.IgnoreCase))
                        outColor = ColorTranslator.FromHtml(c.ToString());
                    else
                        outColor = Color.FromName(c.ToString());
                    success = true;
                }
            }

            color = outColor;
            return success;
        }
    }
}

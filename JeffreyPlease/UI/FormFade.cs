﻿using JeffreyPlease.Utils;
using System.Threading;
using System.Windows.Forms;

namespace JeffreyPlease.UI
{
    /// <summary>
    /// Class for managing opacity fade ins/outs of forms
    /// </summary>
    public class FormFade
    {
        private delegate void SetOpacityCallback(double opacity);

        /// <summary>Gets or sets the speed of the fade</summary>
        public double Speed { get; set; }
        /// <summary>Gets or sets the initial opacity as a double from 0 to 1 where 1 is fully visible</summary>
        public double StartOpacity { get; set; }
        /// <summary>Gets or sets the end opacity as a double from 0 to 1 where 1 is fully visible</summary>
        public double EndOpacity { get; set; }
        /// <summary>Gets the state of the fade</summary>
        public bool Running { get; private set; }

        /// <summary>The form to be faded</summary>
        private Form form = null;
        /// <summary>The thread calculating and timing the opacity updates</summary>
        private Thread fadeThread = null;

        /// <summary>
        /// Creates a new Instance using the default values (instantly sets the opacity to 1)
        /// </summary>
        public FormFade()
        {
            this.Speed = this.StartOpacity = this.EndOpacity = 1.0D;
        }
        /// <summary>
        /// Creates a new Instance fading from 0 to 1 using the given speed
        /// </summary>
        /// <param name="speed">The speed of the fade</param>
        public FormFade(double speed)
        {
            this.Speed = speed;
            this.StartOpacity = 0;
            this.EndOpacity = 1.0d;
        }
        /// <summary>
        /// Creates a new Instance where the fade starts at 0 to the given end-opacity
        /// </summary>
        /// <param name="speed">The speed of the fade</param>
        /// <param name="endOpacity">The end opacity as a double from 0 to 1 where 1 is fully visible</param>
        public FormFade(double speed, double endOpacity)
        {
            this.Speed = speed;
            this.StartOpacity = 0;
            this.EndOpacity = endOpacity;
        }
        /// <summary>
        /// Creates a new fully custom Instance
        /// </summary>
        /// <param name="speed">The speed of the fade</param>
        /// <param name="startOpacity">The end opacity as a double from 0 to 1 where 1 is fully visible</param>
        /// <param name="endOpacity">The end opacity as a double from 0 to 1 where 1 is fully visible</param>
        public FormFade(double speed, double startOpacity, double endOpacity)
        {
            this.Speed = speed;
            this.StartOpacity = startOpacity;
            this.EndOpacity = endOpacity;
        }

        /// <summary>
        /// <para>Prepare the given form for the fade</para>
        /// Call this before showing the form
        /// </summary>
        /// <param name="form">The form to be faded</param>
        public void Prepare(Form form)
        {
            if (this.form != null)
                this.form.Opacity = this.EndOpacity;

            if (form != null)
            {
                this.form = form;
                this.form.Opacity = this.StartOpacity;
            }
        }

        /// <summary>
        /// Starts the fade for the prepared form
        /// </summary>
        public void Start()
        {
            if (this.form != null)
            {
                this.form.Opacity = this.StartOpacity;

                if (this.StartOpacity != this.EndOpacity)
                {
                    StartFade();
                }
            }
        }

        /// <summary>
        /// Stops the fade and set the prepared form instantly to the given end opacity
        /// </summary>
        public void Stop()
        {
            if (this.form != null)
            {
                if (this.fadeThread != null)
                    this.fadeThread.Abort();

                SetOpacity(this.EndOpacity);
            }
        }

        /// <summary>
        /// Starts the fade thread and cleans up from old fading if needed
        /// </summary>
        private void StartFade()
        {
            if (this.fadeThread != null)
                this.fadeThread.Abort();

            this.fadeThread = new Thread(new ThreadStart(FadeThreadHandler));
            this.fadeThread.Start();

            this.Running = true;
        }

        /// <summary>
        /// Sets the opacity of the prepared form thread safe
        /// </summary>
        /// <param name="opacity">The opacity to be set</param>
        private void SetOpacity(double opacity)
        {
            if (this.form.InvokeRequired)
            {
                this.form.Invoke(new SetOpacityCallback(SetOpacity), opacity);
            }
            else
            {
                this.form.Opacity = opacity;
            }
        }

        /// <summary>Thread Handler</summary>
        private void FadeThreadHandler()
        {
            double startOpacity = this.StartOpacity;
            double currentOpacity = startOpacity;
            double speed = this.Speed;
            double endOpacity = this.EndOpacity;
            bool visibleDir = endOpacity > this.StartOpacity; // true = up (++)

            while ((visibleDir && currentOpacity < endOpacity) || (!visibleDir && currentOpacity > endOpacity))
            {
                if (visibleDir)
                    currentOpacity += speed;
                else
                    currentOpacity -= speed;

                SetOpacity(MathHelper.InclusiveClamp(startOpacity, currentOpacity, endOpacity));

                Thread.Sleep(50);
            }
            currentOpacity = MathHelper.InclusiveClamp(startOpacity, currentOpacity, endOpacity);
            SetOpacity(currentOpacity);

            this.Running = false;
        }
    }
}

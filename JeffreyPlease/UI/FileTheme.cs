﻿using JeffreyPlease.Settings;
using System.IO;

namespace JeffreyPlease.UI
{
    /// <summary>
    /// Class for loading a plugin from a file
    /// </summary>
    public class FileTheme : JSONTheme
    {
        /// <summary>The plugin file information</summary>
        private FileInfo themeFile;

        /// <summary>
        /// Creates a new instance of FileTheme
        /// </summary>
        /// <param name="themeFile">The plugin file information</param>
        public FileTheme(FileInfo themeFile)
        {
            this.themeFile = themeFile;
            this.LoadFromFile(this.themeFile);
        }

        /// <summary>
        /// Loads the plugin from the file
        /// </summary>
        public override void Load()
        {
            LoadFromFile(this.themeFile);
        }

        /// <summary>
        /// Loads the plugin from a given file
        /// </summary>
        /// <param name="themeFile">The file to load the plugin from</param>
        private void LoadFromFile(FileInfo themeFile)
        {
            if (themeFile.Exists && ".jeffreytheme".Equals(themeFile.Extension.ToLower()))
            {
                this.themeObject = Store.Get<SettingsManager>().GetFile(themeFile.FullName);
            }
        }
    }
}

﻿using JeffreyPlease.Settings;
using JeffreyPlease.Utils;
using JSONdotNET;
using System;
using System.IO;
using System.Windows.Forms;

namespace JeffreyPlease.UI.Controls
{
    public partial class SearchPathControl : UserControl
    {
        private struct ListControlItem
        {
            public string name;
            public bool recursive;
            public string path;

            public override string ToString()
            {
                return name;
            }
        }

        private bool changed = false;

        public SearchPathControl()
        {
            InitializeComponent();

            btnDelete.Visible = false;
        }

        public bool Add(string path, bool recursive, bool showMessageBox)
        {
            if (!String.IsNullOrWhiteSpace(path))
            {
                if (!(File.Exists(path) || Directory.Exists(path))) {
                    if (showMessageBox) {
                        MessageBox.Show("The file or folder does not exist.");
                    }
                    return false;
                }
                foreach (object itemObj in listBox1.Items)
                {
                    if (itemObj is ListControlItem && ((ListControlItem)itemObj).path == path)
                    {
                        if (showMessageBox)
                        {
                            txtPath.Select();
                            MessageBox.Show("An item with that path already exists, please choose another path.");
                        }
                        return false;
                    }
                }

                ListControlItem item = new ListControlItem();
                if (Tools.IsDirectory(path))
                {
                    DirectoryInfo file = new DirectoryInfo(path);
                    item.name = file.Name;
                    item.path = file.FullName;
                }
                else
                {
                    FileInfo file = new FileInfo(path);
                    item.name = file.Name;
                    item.path = file.FullName;
                }
                item.recursive = recursive;
                listBox1.Items.Add(item);

                ResetInput();

                return true;
            }
            else
            {
                if (showMessageBox)
                    MessageBox.Show("Please enter a valid path!");
            }
            return false;
        }

        public void Save(SettingsManager settingsManager)
        {
            if (changed)
            {
                JSONArray paths = new JSONArray();
                foreach (object itemObj in listBox1.Items)
                {
                    if (itemObj is ListControlItem)
                    {
                        ListControlItem item = (ListControlItem)itemObj;
                        paths.Add((new JSONObject()).Add("path", item.path).Add("recursive", item.recursive));
                    }
                }
                settingsManager.AddSetting("searchPaths", paths);
            }
        }

        private void txtPath_TextChanged(object sender, EventArgs e)
        {
            CheckPath();
            //chkRecursive.Enabled = Tools.IsDirectory(txtPath.Text);
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            object item = listBox1.SelectedItem;
            if (item == null)
            {
                btnAdd.Text = "Add";
            }
            else if (item is ListControlItem)
            {
                ListControlItem c = (ListControlItem)item;
                txtPath.Text = c.path;
                chkRecursive.Checked = c.recursive;

                if (Tools.IsDirectory(c.path))
                {
                    chkRecursive.Enabled = true;
                }
                else
                {
                    chkRecursive.Enabled = false;
                }

                btnDelete.Visible = true;
            }

            if (txtPath.Focused)
                txtPath.Select(txtPath.Text.Length, 0);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            int index = listBox1.SelectedIndex;
            if (index >= 0)
            {
                listBox1.Items.RemoveAt(index);
                ResetInput();

                changed = true;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (Add(txtPath.Text, chkRecursive.Checked, true))
                changed = true;
        }

        private void CheckPath()
        {
            string path = txtPath.Text;
            if (!String.IsNullOrWhiteSpace(path))
            {
                object objectToSelect = null;
                foreach (object itemObj in listBox1.Items)
                {
                    if (itemObj is ListControlItem)
                    {
                        ListControlItem item = (ListControlItem)itemObj;
                        if (item.path.ToLower() == path.Replace('/', '\\').ToLower())
                        {
                            objectToSelect = itemObj;
                            btnAdd.Text = "Update";
                            break;
                        }
                        else
                        {
                            btnAdd.Text = "Add";
                        }
                    }
                }
                if (objectToSelect != null)
                    listBox1.SetSelected(listBox1.Items.IndexOf(objectToSelect), true);
            }
            else
            {
                btnAdd.Text = "Add";
            }
        }

        private void ResetInput()
        {
            txtPath.Text = String.Empty;
        }
    }
}

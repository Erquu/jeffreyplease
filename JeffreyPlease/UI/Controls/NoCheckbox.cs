﻿using System.Drawing;
using System.Windows.Forms;

namespace JeffreyPlease.UI.Controls
{
    public class NoCheckbox : CheckBox
    {
        protected override void OnPaint(PaintEventArgs pevent)
        {
            //base.OnPaint(pevent);
            pevent.Graphics.Clear(BackColor);

            if (this.Checked)
                pevent.Graphics.DrawString(Text, Font, Brushes.Black, PointF.Empty);
            else
                pevent.Graphics.DrawString(Text, Font, Brushes.Gray, PointF.Empty);
        }
    }
}

﻿using JeffreyPlease.Plugin;
using JeffreyPlease.Utils;
using System;
using System.Windows.Forms;

namespace JeffreyPlease.UI.Controls
{
    public partial class PluginViewControl : UserControl
    {
        private PluginLoader pluginLoader;
        private UserControl activeControl = null;

        public PluginViewControl()
        {
            InitializeComponent();

            this.pluginLoader = Store.Get<PluginLoader>();
            if (pluginLoader != null)
            {
                foreach (JeffreyExtension ext in pluginLoader.LoadedPlugins)
                {
                    lstPlugins.Items.Add(ext.ToString());
                }
            }

            if (lstPlugins.Items.Count == 0)
            {
                txtPluginName.Text = "No plugins installed";
                chkActive.Visible = false;
            }
            else
            {
                lstPlugins.SelectedIndex = 0;
            }
        }

        private void lstPlugins_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (MathHelper.Between(0, lstPlugins.SelectedIndex, this.pluginLoader.LoadedPlugins.Count))
            {
                JeffreyExtension ext = this.pluginLoader.LoadedPlugins[lstPlugins.SelectedIndex];
                txtPluginName.Text = ext.Name;
                chkActive.Checked = true;

                try
                {
                    
                    UserControl settingsPanel = ext.GetSettingsPanel();
                    if (settingsPanel != null)
                    {
                        tableLayoutPanel1.Controls.Add(settingsPanel, 1, 1);
                        settingsPanel.Dock = DockStyle.Fill;
                    }
                    else
                    {
                        if (activeControl != null)
                        {
                            tableLayoutPanel1.Controls.Remove(activeControl);
                        }
                    }
                    activeControl = settingsPanel;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("There is an error in this plugin, could not load view!");
                    Store.Get<JeffreyPlease.Settings.ErrorLog>().HandleError(ex);
                }
            }
        }
    }
}

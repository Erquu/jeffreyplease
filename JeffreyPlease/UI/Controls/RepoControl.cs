﻿using JeffreyPlease.Settings;
using JSONdotNET;
using System;
using System.Windows.Forms;

namespace JeffreyPlease.UI.Controls
{
    public partial class RepoControl : UserControl
    {
        private struct ListControlItem
        {
            public string name;
            public string url;

            public override string ToString()
            {
                return name;
            }
        }

        private bool changed = false;

        public RepoControl()
        {
            InitializeComponent();

            btnDelete.Visible = false;
        }

        public bool Add(string name, string url, bool showMessageBox)
        {
            if (!String.IsNullOrWhiteSpace(name) && !String.IsNullOrWhiteSpace(url))
            {
                ListControlItem item;

                foreach (object itemObj in listBox1.Items)
                {
                    if (itemObj is ListControlItem && ((ListControlItem)itemObj).name == name)
                    {
                        listBox1.Items.Remove(itemObj);
                        break;
                    }
                }

                item = new ListControlItem();
                item.name = name;
                item.url = url;
                listBox1.Items.Add(item);

                ResetInput();
            }
            else
            {
                if (showMessageBox)
                    MessageBox.Show("Please enter a valid name and/or url!");
                return false;
            }
            return true;
        }

        public void Save(SettingsManager settingsManager)
        {
            if (changed)
            {
                JSONArray repos = new JSONArray();
                foreach (object itemObj in listBox1.Items)
                {
                    if (itemObj is ListControlItem)
                    {
                        ListControlItem item = (ListControlItem)itemObj;
                        repos.Add((new JSONObject()).Add("name", item.name).Add("url", item.url));
                    }
                }
                settingsManager.AddSetting("repositories", repos);
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            object item = listBox1.SelectedItem;
            if (item is ListControlItem)
            {
                ListControlItem c = (ListControlItem)item;
                txtName.Text = c.name;
                txtUrl.Text = c.url;

                btnDelete.Visible = true;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            int index = listBox1.SelectedIndex;
            if (index >= 0)
            {
                listBox1.Items.RemoveAt(index);
                ResetInput();

                changed = true;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (Add(txtName.Text, txtUrl.Text, true))
                changed = true;
        }

        private void ResetInput()
        {
            txtName.Text = String.Empty;
            txtUrl.Text = String.Empty;
        }
    }
}

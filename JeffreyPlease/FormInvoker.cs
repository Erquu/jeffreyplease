﻿using System;
using System.Windows.Forms;

namespace JeffreyPlease
{
    public class FormInvoker
    {
        internal static InputForm inputForm;

        public static bool InvokeRequired { get { return FormInvoker.inputForm.InvokeRequired; } }

        public static void Invoke(Form form, Delegate method)
        {
            if (InvokeRequired)
            {
                inputForm.Invoke(method);
            }
            else
            {
                method.DynamicInvoke();
            }
        }
    }
}

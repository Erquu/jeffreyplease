﻿using JeffreyPlease.Plugin.Container;
using JeffreyPlease.Settings;
using JSONdotNET;
using System;
using System.IO;
using System.Windows.Forms;

namespace JeffreyPlease.Plugin
{
    /// <summary>
    /// <para>Plugin-Class to define a new JeffreyPlease Plugin</para>
    /// Classes inheriting from this class will be instanciated by the PluginLoader
    /// </summary>
    public abstract class JeffreyExtension
    {
        /// <summary>Name of the plugin</summary>
        public abstract string Name { get; }
        /// <summary>Description of the plugin</summary>
        public abstract string Description { get; }

        private ResourceProvider activeProvider = null;

        public virtual UserControl GetSettingsPanel() { return null; }

        private bool settingsInitialized = false;

        /// <summary>
        /// <para>Provides a settings object for this JeffreyExtension only</para>
        /// the saving and loading will be done automatically
        /// </summary>
        /// <returns>The loaded settings object (empty if nothing is saved yet)</returns>
        protected JSONObject ProvideSettings()
        {
            SettingsManager settings = Store.Get<SettingsManager>();
            if (!this.settingsInitialized)
            {
                this.settingsInitialized = true;
                settings.SaveRequest += new SaveRequestEventHandler(settings_SaveRequest);
            }
            return settings.GetSetting(Name);
        }

        /// <summary>
        /// <para>Provides a settings folder to save and load resources from</para>
        /// Jeffrey makes sure that it exists
        /// </summary>
        /// <returns>The settings folder for this JeffreyExtension</returns>
        protected DirectoryInfo ProvideSettingsFolder()
        {
            DirectoryInfo dir = GetPluginDir();

            if (!dir.Exists)
                dir.Create();

            return dir;
        }

        private void settings_SaveRequest(SettingsManager instance)
        {
            SettingsManager settings = Store.Get<SettingsManager>();
            JSONObject pluginSettings = settings.GetSetting(Name);

            if (pluginSettings != null && !pluginSettings.Empty)
                instance.AddSetting(Name, pluginSettings);
        }

        private DirectoryInfo GetPluginDir()
        {
            SettingsManager settings = Store.Get<SettingsManager>();
            char[] invalidChars = Path.GetInvalidPathChars();
            string name = this.Name;

            if (String.IsNullOrWhiteSpace(name))
                name = "PluginFiles";

            foreach (char c in invalidChars)
                name = name.Replace(char.ToString(c), "");

            return new DirectoryInfo(Path.Combine(settings.PluginDir.FullName, name));
        }

        public override string ToString()
        {
            return this.Name;
        }
    }
}

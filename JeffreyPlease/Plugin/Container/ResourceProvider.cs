﻿using System.IO;
using System.IO.Compression;

namespace JeffreyPlease.Plugin.Container
{
    public class ResourceProvider
    {
        private FileStream fs;
        private ZipArchive archive;
        internal int activeCount = 0;

        internal ResourceProvider(FileStream fs, ZipArchive archive)
        {
            this.fs = fs;
            this.archive = archive;
        }

        ~ResourceProvider()
        {
            this.fs.Close();
            this.archive.Dispose();
        }
    }
}

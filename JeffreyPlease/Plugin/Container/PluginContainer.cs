﻿using System;
using System.IO;
using System.IO.Compression;

namespace JeffreyPlease.Plugin.Container
{
    public class PluginContainer
    {
        private FileInfo archive;
        private ZipArchive zip;
        private Stream fs;

        public PluginContainer(FileInfo file)
        {
            this.archive = file;
        }
        ~PluginContainer()
        {
            this.Close();
        }

        public void Open()
        {
            this.fs = new FileStream(this.archive.FullName, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            this.zip = new ZipArchive(this.fs, ZipArchiveMode.Update);
        }

        public void Close()
        {
            if (this.zip != null)
                this.zip.Dispose();
            if (this.fs != null)
                this.fs.Close();

            this.zip = null;
            this.fs = null;
        }

        public void DeleteResource(string name)
        {
            GetEntry(name).Delete();
        }

        public void AddFile(string name, Stream file)
        {
            ZipArchiveEntry entry = this.zip.CreateEntry(name);
            Stream entryStream = entry.Open();

            file.CopyTo(entryStream);

            file.Close();
            entryStream.Close();
        }
        public void AddFile(string name, FileInfo file)
        {
            Stream inputStream = file.OpenRead();

            AddFile(name, inputStream);

            inputStream.Close();
        }

        public Stream GetResource(string name)
        {
            Stream stream = GetEntry(name).Open();
            return stream;
        }

        private ZipArchiveEntry GetEntry(string name)
        {
            ZipArchiveEntry entry = this.zip.GetEntry(name);
            if (entry != null)
            {
                return entry;
            }
            else
            {
                throw new NullReferenceException("Requested resource does not exist");
            }
        }
    }
}

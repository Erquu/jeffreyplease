﻿using JeffreyPlease.Plugin.Container;
using JeffreyPlease.Settings;
using JeffreyPlease.UI;
using JSONdotNET;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Linq;

namespace JeffreyPlease.Plugin
{
    /// <summary>
    /// Class for loading Plugins
    /// </summary>
    public class PluginLoader
    {
        /// <summary>The plugin type to be loaded</summary>
        private static Type EXTENSION_TYPE = typeof(JeffreyExtension);

        /// <summary>Instances of all loaded plugins</summary>
        public List<JeffreyExtension> LoadedPlugins { get; private set; }

        /// <summary>List of already loaded plugins (file-names)</summary>
        private List<string> filesLoaded = new List<string>();

        public PluginLoader()
        {
            this.LoadedPlugins = new List<JeffreyExtension>();
        }

        /// <summary>
        /// Searches and loads all plugins from the plugin folder
        /// </summary>
        private void LoadPlugins()
        {
            SettingsManager settings = Store.Get<SettingsManager>();
            DirectoryInfo pluginDir = settings.PluginDir;
            pluginDir.Refresh();

            if (pluginDir.Exists)
            {
                foreach (FileInfo plugin in pluginDir.GetFiles("*.jeffreyplugin"))
                {
                    Load(plugin);
                }

                try
                {
                    JSONObject developerMode = settings.GetSetting("developer");
                    if (developerMode.Type == JSONObjectType.LITERAL && ((JSONLiteralNode)developerMode).ToBoolean())
                    {
                        foreach (FileInfo plugin in pluginDir.GetFiles("*.dll"))
                        {
                            LoadDll(plugin);
                        }
                    }
                }
                catch (FormatException ex) { Store.Get<ErrorLog>().HandleError(ex); }
            }
        }

        /// <summary>
        /// Loads all plugins
        /// </summary>
        /// <param name="async">Loading should be in a new thread</param>
        public void Load(bool async)
        {
            if (async)
            {
                Thread loadThread = new Thread(new ThreadStart(LoadPlugins));
                loadThread.Name = "PluginLoader Thread";
                loadThread.Start();
            }
            else
            {
                LoadPlugins();
            }
        }
        public JeffreyExtension Load(FileInfo plugin)
        {
            JeffreyExtension ext = null;

            if (plugin != null && plugin.Exists)
            {
                if (filesLoaded.BinarySearch(plugin.FullName) < 0)
                {
                    filesLoaded.Add(plugin.FullName);

                    PluginContainer container = new PluginContainer(plugin);
                    container.Open();
                    JSONObject mf = JSON.Parse(container.GetResource("mf"));

                    Debug.WriteLine(mf.ToString(false));

                    if (mf.ContainsKey("config"))
                    {
                        SettingsManager settingsManager = Store.Get<SettingsManager>();

                        JSONArray configs = mf.GetArray("config");
                        foreach (JSONObject config in configs)
                        {
                            settingsManager.MigrateJSON(config);
                        }
                    }

                    JSONArray autoLoad = mf.GetArray("autoload");
                    for (int i = 0; i < autoLoad.Length; i++)
                    {
                        string resource = autoLoad[i].ToString();
                        if (resource.EndsWith(".jeffreytheme", StringComparison.InvariantCultureIgnoreCase))
                        {
                            Debug.WriteLine("Autoload {0}", resource);

                            Stream themeStream = container.GetResource(resource);
                            JSONObject theme = JSON.Parse(themeStream);
                            themeStream.Close();
                            Store.Get<ThemeManager>().AddTheme(theme);
                        }
                        else if (resource.EndsWith(".dll", StringComparison.InvariantCultureIgnoreCase))
                        {
                            Stream pluginStream = container.GetResource(resource);
                            ext = LoadDll(pluginStream);
                            pluginStream.Close();
                        }
                    }

                    container.Close();
                }
            }

            return ext;
        }

        public bool IsLoaded(string pluginName)
        {
            if (!String.IsNullOrWhiteSpace(pluginName))
            {
                foreach (string ext in this.filesLoaded)
                {
                    if (Path.GetFileNameWithoutExtension(ext).Equals(pluginName, StringComparison.InvariantCultureIgnoreCase))
                    {
                        return true;
                    }
                }
                foreach (JeffreyExtension ext in this.LoadedPlugins)
                {
                    if (pluginName.Equals(ext.Name, StringComparison.InvariantCultureIgnoreCase))
                        return true;
                }
            }
            return false;
        }

        public JeffreyExtension LoadDll(FileInfo pluginFile)
        {
            JeffreyExtension ext = null;

            if (pluginFile.Exists)
            {
                Stream pluginStream = null;
                try
                {
                    pluginStream = Store.Get<SettingsManager>().OpenRead(pluginFile.FullName);
                    ext = LoadDll(pluginStream);
                }
                catch (Exception ex)
                {
                    Store.Get<ErrorLog>().HandleError(ex);
                }
                finally
                {
                    if (pluginStream != null)
                        pluginStream.Close();
                }
            }

            return ext;
        }
        public JeffreyExtension LoadDll(Stream dllStream)
        {
            JeffreyExtension ext = null;

            try
            {
                Assembly assembly = Assembly.Load(ReadFully(dllStream));
                foreach (Type type in assembly.GetTypes())
                {
                    if (type.IsPublic && !type.IsAbstract && !type.IsInterface)
                    {
                        Type typeInterface = type.BaseType;

                        if (typeInterface != null && PluginLoader.EXTENSION_TYPE.FullName.Equals(typeInterface.FullName))
                        {
                            try
                            {
                                ext = (JeffreyExtension)Activator.CreateInstance(type);

                                LoadedPlugins.Add(ext);

                                Debug.WriteLine("Plugin Loader: " + ext);
                            }
                            catch (Exception ex) { Store.Get<ErrorLog>().HandleError(ex); }
                        }
                    }
                }
            }
            catch (ReflectionTypeLoadException ex) { Store.Get<ErrorLog>().HandleError(ex); }
            catch (Exception ex) { Store.Get<ErrorLog>().HandleError(ex); }

            return ext;
        }

        private byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
    }
}

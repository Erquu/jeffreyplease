﻿using JeffreyPlease.Plugin;

namespace JeffreyPlease.UI
{
    /// <summary>
    /// Class for holding general information about a plugin or plugin
    /// </summary>
    public class MetaInformation
    {
        /// <summary>Gets the name</summary>
        public string Name { get; private set; }
        /// <summary>Gets the description</summary>
        public string Description { get; private set; }
        /// <summary>Gets the labels if set</summary>
        public string[] Labels { get; private set; }
        /// <summary>Gets the autor</summary>
        public string Author { get; private set; }
        /// <summary>Gets the type of the Plugin</summary>
        public PluginType Type { get; private set; }

        /// <summary>
        /// Creates a new instance of MetaInformation
        /// </summary>
        /// <param name="name">The name</param>
        /// <param name="description">The description</param>
        /// <param name="author">The author</param>
        /// <param name="type">The type</param>
        public MetaInformation(string name, string description, string author, PluginType type) : this(name, description, author, type, null) { }
        /// <summary>
        /// Creates a new instance of MetaInformation
        /// </summary>
        /// <param name="name">The name</param>
        /// <param name="description">The description</param>
        /// <param name="author">The author</param>
        /// <param name="type">The type</param>
        /// <param name="labels">The labels</param>
        public MetaInformation(string name, string description, string author, PluginType type, string[] labels)
        {
            this.Name = name;
            this.Description = description;
            this.Author = author;
            this.Type = type;
            this.Labels = labels;
        }
    }
}

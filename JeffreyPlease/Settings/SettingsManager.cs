﻿using JeffreyPlease.UI;
using JSONdotNET;
using System;
using System.IO;

namespace JeffreyPlease.Settings
{
    /// <summary>
    /// Callback delegate for Save requests by the <see cref="JeffreyPlease.Settings.SettingsManager"/>
    /// </summary>
    /// <param name="instance">The used instance of the SettingsManager</param>
    public delegate void SaveRequestEventHandler(SettingsManager instance);
    /// <summary>
    /// Event delegate for when the save file is initially created
    /// </summary>
    /// <param name="instance">The used instance of the SettingsManager</param>
    /// <param name="initialSettings">The initial settings object</param>
    public delegate void InitializationEventHandler(SettingsManager instance, JSONObject initialSettings);

    /// <summary>
    /// The SettingsManager loads and saves the settings and contains all relevant information about directories used by jeffrey
    /// </summary>
    public class SettingsManager
    {
        /// <summary>
        /// Fires when the SettingsManager starts saving the current settings
        /// </summary>
        public event SaveRequestEventHandler SaveRequest;
        /// <summary>
        /// Fires when the SettingsManager creates the save file
        /// </summary>
        public event InitializationEventHandler Initialize;

        /// <summary>Directory used  for storing all relevant Data</summary>
        public DirectoryInfo SettingsDir { get; private set; }
        /// <summary>Directory where themes are saved in</summary>
        public DirectoryInfo ThemesDir { get; private set; }
        /// <summary>Directory where plugins are saved in</summary>
        public DirectoryInfo PluginDir { get; private set; }
        /// <summary>The global settings object</summary>
        public JSONObject SettingsObject { get { return settingsObject; } }
        /// <summary>Indicator for changes</summary>
        private bool changed = false;
        /// <summary>The settings object</summary>
        private JSONObject settingsObject = null;

        /// <summary>Creates a new instance of the SettingsManger</summary>
        public SettingsManager()
        {
            string roaming = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            if (Directory.Exists(roaming)) {
                SettingsDir = new DirectoryInfo(Path.Combine(roaming, "JeffreyPlease"));
                if (!SettingsDir.Exists)
                    SettingsDir.Create();
                ThemesDir = new DirectoryInfo(Path.Combine(SettingsDir.FullName, "themes"));
                if (!ThemesDir.Exists)
                    ThemesDir.Create();
                PluginDir = new DirectoryInfo(Path.Combine(SettingsDir.FullName, "plugins"));
                if (!PluginDir.Exists)
                    PluginDir.Create();
            }
        }

        /// <summary>Gets the settings object identified by the given key</summary>
        /// <param name="key">The key of the settingsobject</param>
        /// <returns>The settings object or an empty JSONObject in case nothing is found</returns>
        public JSONObject GetSetting(string key)
        {
            if (settingsObject != null && settingsObject.ContainsKey(key))
                return settingsObject.Get(key);
            return new JSONObject();
        }

        /// <summary>
        /// Adds a settings object to the settings file
        /// </summary>
        /// <param name="key">The key for the setting</param>
        /// <param name="obj">The settings object</param>
        public void AddSetting(string key, JSONObject obj)
        {
            if (!obj.Empty)
            {
                settingsObject.Add(key, obj);
                changed = true;
            }
        }

        /// <summary>
        /// Loads the general Jeffrey settings
        /// </summary>
        public void Load()
        {
            FileInfo settingsFile = GetSettingsFileInfo("settings.json");

            if (settingsFile.Exists)
            {
                Stream fileInputStream = OpenRead(settingsFile.FullName);
                this.settingsObject = JSON.Parse(fileInputStream);
                fileInputStream.Close();
            }
            else
            {
                this.settingsObject = new JSONObject();
                if (Initialize != null)
                    Initialize.Invoke(this, this.settingsObject);
            }
        }

        /// <summary>
        /// Loads stuff that needs to wait until all other parts of JeffreyPlease are loaded
        /// </summary>
        public void Post_Load()
        {
            if (settingsObject.ContainsKey("theme"))
            {
                ThemeManager themeManager = Store.Get<ThemeManager>();
                themeManager.SetActive(settingsObject.GetString("theme"));
            }
        }

        /// <summary>
        /// Opens a file in read only mode (clean) and returns the FileStream
        /// </summary>
        /// <param name="filePath">The file to be opened</param>
        /// <returns>The input stream of the file (readonly) or null if the file does not exist</returns>
        public FileStream OpenRead(string filePath)
        {
            if (File.Exists(filePath))
                return new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            return null;
        }

        /// <summary>
        /// Parses a given json file and returns it as a JSONObject
        /// </summary>
        /// <param name="fileName">The file to be parsed</param>
        /// <returns>The JSONObject for the file, if the file does not exist, an empty JSONObject will be returned</returns>
        public JSONObject GetFile(string fileName)
        {
            JSONObject resultFile;
            FileInfo settingsFile = GetSettingsFileInfo(fileName);

            if (settingsFile.Exists)
            {
                Stream fileInputStream = OpenRead(settingsFile.FullName);
                resultFile = JSON.Parse(fileInputStream);
                fileInputStream.Close();
            }
            else
            {
                resultFile = new JSONObject();
            }
            return resultFile;
        }

        /// <summary>
        /// Saves the settings to the default settings file
        /// </summary>
        public void Save()
        {
            if (changed)
            {
                Save("settings.json", settingsObject);
            }
            if (SaveRequest != null)
                SaveRequest.Invoke(this);
        }
        /// <summary>
        /// <para>Saves a JSONObject into a given file.</para>
        /// <para>This method makes sure the file is saved, if the given file is invalid in any way</para>
        /// A temporary file will be created in the same folder
        /// </summary>
        /// <param name="fileName">The name of the file to be saved to</param>
        /// <param name="data">The data to be saved</param>
        public void Save(string fileName, JSONObject data)
        {
            FileInfo saveFile = new FileInfo(Path.Combine(SettingsDir.FullName, fileName));
            FileInfo saveTempFile;
            int num = 0;
            do
            {
                saveTempFile = new FileInfo(Path.Combine(SettingsDir.FullName, fileName + ".TMP~" + (++num)));
            } while (saveTempFile.Exists);

            FileStream outputStream = (!saveTempFile.Exists) ? saveTempFile.Create() : saveTempFile.OpenWrite();
            StreamWriter writer = new StreamWriter(outputStream);
            writer.Write(data.ToString());
            writer.Close();
            outputStream.Close();

            try
            {
                saveFile.Delete();
                saveTempFile.MoveTo(saveFile.FullName);
            }
            catch (Exception ex) { Store.Get<ErrorLog>().HandleError(ex); }
        }

        public void MigrateJSON(JSONObject from)
        {
            this.MigrateJSON(from, settingsObject);
        }
        public void MigrateJSON(JSONObject from, JSONObject to)
        {
            if (from is JSONObject)
            {
                foreach (string key in from.Keys)
                {
                    if (to.ContainsKey(key))
                    {
                        Type t = from[key].GetType();
                        if (t == to[key].GetType() && t == typeof(JSONObject))
                        {
                            MigrateJSON(from[key], to[key]);
                        }
                        else
                        {
                            to.Add(key, from[key]);
                        }
                    }
                    else
                    {
                        to.Add(key, from[key]);
                    }
                }
            }
        }

        /// <summary>
        /// Adds or modifies an entry in the default settings file
        /// </summary>
        /// <param name="name">The key of the entry</param>
        /// <param name="newValue">The new value for the entry</param>
        public void ChangeSetting(string name, string newValue) { changed = true; settingsObject.Add(name, new JSONTextNode(newValue)); }
        /// <summary>
        /// Adds or modifies an entry in the default settings file
        /// </summary>
        /// <param name="name">The key of the entry</param>
        /// <param name="newValue">The new value for the entry</param>
        public void ChangeSetting(string name, double newValue) { changed = true; settingsObject.Add(name, new JSONLiteralNode(newValue.ToString())); }
        /// <summary>
        /// Adds or modifies an entry in the default settings file
        /// </summary>
        /// <param name="name">The key of the entry</param>
        /// <param name="newValue">The new value for the entry</param>
        public void ChangeSetting(string name, float newValue) { changed = true; settingsObject.Add(name, new JSONLiteralNode(newValue.ToString())); }
        /// <summary>
        /// Adds or modifies an entry in the default settings file
        /// </summary>
        /// <param name="name">The key of the entry</param>
        /// <param name="newValue">The new value for the entry</param>
        public void ChangeSetting(string name, int newValue) { changed = true; settingsObject.Add(name, new JSONLiteralNode(newValue.ToString())); }

        private FileInfo GetSettingsFileInfo(string fileName)
        {
            if (!Path.IsPathRooted(fileName))
            {
                fileName = Path.Combine(SettingsDir.FullName, fileName);
            }

            return new FileInfo(fileName);
        }
    }
}

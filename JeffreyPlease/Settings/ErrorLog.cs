﻿using System;
using System.Diagnostics;
using System.IO;

namespace JeffreyPlease.Settings
{
    /// <summary>
    /// Logs exceptions into a file
    /// </summary>
    public class ErrorLog
    {
        /// <summary>Indicates if the messages should be logged</summary>
        private bool logToFile = false;
        /// <summary>Indicator if anything was logged (to prevent the creation of empty files)</summary>
        private bool empty = true;
        /// <summary>Path to the log file</summary>
        private string debugFilePath;
        /// <summary>Directory for the log files</summary>
        private DirectoryInfo debugDir;
        /// <summary>The File-Stream-Writer</summary>
        private StreamWriter writer = null;

        /// <summary>Creates a new instance of the logger</summary>
        public ErrorLog()
        {
            SettingsManager settings = Store.Get<SettingsManager>();

            if (settings.SettingsObject.ContainsKey("debug"))
            {
                try
                {
                    logToFile = settings.SettingsObject.GetBool("debug");
                }
                catch (Exception) { /* UNHANDLED */ }
            }

            if (logToFile)
            {
                try
                {
                    debugDir = new DirectoryInfo(Path.Combine(settings.SettingsDir.FullName, "debug"));
                    if (!debugDir.Exists)
                        debugDir.Create();

                    debugFilePath = Path.Combine(debugDir.FullName, String.Format("jeffrey_{0:dd_MM_yy_H_MM_ss}.log", DateTime.Now));
                    Stream logStream = File.Create(debugFilePath);
                    writer = new StreamWriter(logStream);

                    settings.SaveRequest += new SaveRequestEventHandler(settings_SaveRequest);
                }
                catch (Exception) { logToFile = false; }
            }
        }

        /// <summary>Writes the exception details into the log</summary>
        /// <param name="ex">The exception to be handled</param>
        public void HandleError(Exception ex)
        {
            Debug.WriteLine(ex.StackTrace);
            if (logToFile && writer != null)
            {
                empty = false;

                writer.WriteLine("------------------");
                writer.WriteLine(ex.Message);
                if (ex.InnerException != null)
                    writer.WriteLine(ex.InnerException.Message);
                writer.WriteLine(ex.StackTrace);
                writer.Flush();
            }
        }

        private void settings_SaveRequest(SettingsManager instance)
        {
            try
            {
                if (writer != null)
                {
                    writer.Close();
                    writer = null;
                }

                if (empty)
                {
                    try
                    {
                        File.Delete(debugFilePath);
                    }
                    catch (Exception) { /* UNHANDLED */ }
                }
            }
            catch (Exception) { /* UNHANDLED */ }
        }
    }
}

﻿using JeffreyPlease.Logic;
using JSONdotNET;
using System;
using System.Collections.Generic;

namespace JeffreyPlease.Settings
{
    /// <summary>
    /// <para>The StatisticsManager manages all the jeffrey statistics</para>
    /// Like usage data and input information
    /// </summary>
    public class StatisticsManager
    {
        /// <summary>The JSONObject to save the statistics in</summary>
        private JSONObject statisticsObject;
        /// <summary>Dictionary holding the input history for programs</summary>
        private Dictionary<string, List<string>> inputMap = new Dictionary<string,List<string>>();
        /// <summary>Indicator for changes</summary>
        private bool changed = false;

        /// <summary>Creates a new instance of the StatisticsManager</summary>
        /// <param name="settingsManager">The settingsmanager to get the settings stuff from</param>
        public StatisticsManager(SettingsManager settingsManager)
        {
            statisticsObject = settingsManager.GetFile("statistics.json");
            LoadMap();

            settingsManager.SaveRequest += new SaveRequestEventHandler(settingsManager_SaveRequest);
        }

        /// <summary>Increases the amount of jeffrey openings</summary>
        public void AddOpen()
        {
            Add("opened");
        }
        /// <summary>Saves an input (shortcut) for the given app</summary>
        /// <param name="input">The entered input (shortcut)</param>
        /// <param name="appname">The name of the app</param>
        public void AddOpen(string input, string appname)
        {
            AddOpen();

            List<string> map;

            if (!inputMap.ContainsKey(appname))
                inputMap.Add(appname, (map = new List<string>()));
            else
                map = inputMap[appname];

            if (!map.Contains(input))
                map.Add(input);
        }

        /// <summary>Increases the amount of program runs</summary>
        public void AddRun()
        {
            Add("runned");
        }

        /// <summary>Increases the amount of keystrokes</summary>
        public void AddKeyDown()
        {
            Add("keystrokes");
        }

        /// <summary>Increases the amount of the given key</summary>
        /// <param name="key">The key of the value to increase</param>
        private void Add(string key)
        {
            changed = true;

            long current = 0;
            try
            {
                if (statisticsObject.ContainsKey(key))
                    current = statisticsObject.GetLong(key);
            }
            catch (FormatException ex) { Store.Get<ErrorLog>().HandleError(ex); }
            catch (KeyNotFoundException ex) { Store.Get<ErrorLog>().HandleError(ex); }
            statisticsObject.Add(key, (++current));
        }

        /// <summary>
        /// <para>Loads the input map from the the statistics object</para>
        /// <para>The input map is used to get user specific behaviour</para>
        /// when searching for apps to give the user a better experience
        /// </summary>
        private void LoadMap()
        {
            if (statisticsObject.ContainsKey("inputmap"))
            {
                AppStore appstore = Store.Get<AppStore>();

                JSONObject mapObj = statisticsObject["inputmap"];
                foreach (string key in mapObj.Keys)
                {
                    App app = appstore.Get(key);
                    if (app != null)
                    {
                        JSONObject mapArrObj = mapObj.Get(key);
                        if (mapArrObj is JSONArray)
                        {
                            JSONArray mapArr = (JSONArray)mapArrObj;
                            List<string> map = new List<string>();

                            for (int i = 0; i < mapArr.Length; i++)
                            {
                                string shortName = mapArr[i].ToString();
                                map.Add(shortName);
                            }

                            inputMap.Add(key, map);
                            app.Aliases = map.ToArray();
                        }
                    }
                }
            }
        }

        private void settingsManager_SaveRequest(SettingsManager instance)
        {
            if (changed)
            {
                JSONObject map = new JSONObject();
                foreach (string key in inputMap.Keys)
                {
                    JSONArray arr = new JSONArray();
                    foreach (string inp in inputMap[key])
                    {
                        arr.Add(inp);
                    }
                    map.Add(key, arr);
                }
                statisticsObject.Add("inputmap", map);
                instance.Save("statistics.json", statisticsObject);
            }
        }
    }
}

﻿//#define COLOR_HIGHLIGHT
//#define FORM_SHADOW_ACTIVE

using JeffreyPlease.Logic;
using JeffreyPlease.Logic.Apps.SearchContexts;
using JeffreyPlease.Settings;
using JeffreyPlease.UI;
using JeffreyPlease.Utils;
using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace JeffreyPlease
{
    /// <summary>
    /// <para>The mighty Jeffrey in all his pride</para>
    /// The main input/search form
    /// </summary>
    public class InputForm : Form
    {
        /// <summary>
        /// This textbox makes sure the text is rendered using AntiAlias smoothing mode
        /// </summary>
        private class TestBox : TextBox
        {
            public delegate void SpecialKeyPressedEventHandler(Keys keyData);
            public event SpecialKeyPressedEventHandler SpecialKeyPressed;

            protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
            {
                if (keyData == Keys.Escape || keyData == Keys.Enter)
                {
                    if (this.SpecialKeyPressed != null)
                        this.SpecialKeyPressed.Invoke(keyData);
                    return true;
                }
                return base.ProcessCmdKey(ref msg, keyData);
            }

            protected override void OnPaint(PaintEventArgs e)
            {
                e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                base.OnPaint(e);
            }
        }

        /// <summary>Callback used for invoking</summary>
        /// <param name="ev">Event Args</param>
        private delegate void SearchCompleteCallback(SearchResultEventArgs ev);

        /// <summary>Textbox for the search input</summary>
        private TestBox txtInput;
        /// <summary>Panel used to create the most outer padding</summary>
        private Panel panelInnerPadding;
        /// <summary>Panel used for creating a Textbox-Padding</summary>
        private Panel panelSearchPadding;
        /// <summary>Panel Rendering the search result</summary>
        private ScrollableAppPanel resultPanel;

        /// <summary>StatisticsManager</summary>
        private StatisticsManager statisticsManager;
        /// <summary>ContextManager</summary>
        private ContextManager contextManager;
        /// <summary>The currently active plugin</summary>
        private Theme activeTheme;
        /// <summary>Class for support fading on startup</summary>
        private FormFade fade;

        /* The following two fields are used to limit the number of size-recomputations */
        /// <summary>Current count of visible apps</summary>
        private int currentAppCount = 0;
        /// <summary>Old count of visible apps</summary>
        private int previousAppCount = 0;

        /// <summary>
        /// <para>Indicates if the context should reset on the next press of the backspace key</para>
        /// <para>In a custom search-context, it makes sure that the reset is only after the second keypress</para>
        /// of the backspace key when the <code>txtInput.Text</code> is empty
        /// </summary>
        private bool resetContextOnNextBackspace = false;

        /// <summary>Jeffreys constructor :)</summary>
        public InputForm()
        {
            ThemeManager tm = Store.Get<ThemeManager>();

            this.activeTheme = tm.ActiveTheme;
            this.fade = new FormFade(0.2d, 0, this.activeTheme.Opacity);
            
            InitializeComponent();

            CreateBorder();

            this.statisticsManager = Store.Get<StatisticsManager>();

            this.contextManager = Store.Get<ContextManager>();
            this.contextManager.SearchComplete += new SearchCompleteEventHandler(store_SearchComplete);

            this.txtInput.TextChanged += new EventHandler(txtInput_TextChanged);
            this.txtInput.KeyDown += new KeyEventHandler(txtInput_KeyDown);

            tm.ThemeChanged += new ThemeChangedCallback(InputForm_ThemeChanged);

            //this.contextManager.Search("");
        }

        /// <summary>
        /// Initializes all components
        /// </summary>
        private void InitializeComponent()
        {
            this.txtInput = new TestBox();
            this.txtInput.SpecialKeyPressed += new TestBox.SpecialKeyPressedEventHandler(txtInput_SpecialKeyPressed);

            this.panelInnerPadding = new Panel();
            this.panelSearchPadding = new Panel();
            this.resultPanel = new ScrollableAppPanel(this.activeTheme);

            this.panelSearchPadding.Controls.Add(this.txtInput);
            this.panelInnerPadding.Controls.Add(this.panelSearchPadding);
            this.panelInnerPadding.Controls.Add(this.resultPanel);
            this.Controls.Add(this.panelInnerPadding);

            this.txtInput.TabIndex = 0;

            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;

            this.Name = "JeffreyPlease";
            this.ShowInTaskbar = false;
            this.StartPosition = FormStartPosition.Manual;

            this.UpdateStyle(this.activeTheme);

#if COLOR_HIGHLIGHT
            this.panelInnerPadding.BackColor = Color.Yellow;
            this.panelSearchPadding.BackColor = Color.Red;
            this.resultPanel.BackColor = Color.Pink;
#endif
        }

#if FORM_SHADOW_ACTIVE
        protected override System.Windows.Forms.CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ClassStyle |= Tools.CS_DROPSHADOW;
                return cp;
            }
        }
#endif

        /// <summary>
        /// Shows and focuses this form
        /// </summary>
        public void CallJeffrey()
        {
            this.fade.Prepare(this);

            this.Show();
            this.BringToFront();
            this.TopMost = true;

            Clear();
            RecomputeSize(0, true);

            Tools.Focus(this.Handle);

            this.fade.Start();

            Activate();

            this.txtInput.Select();
            this.txtInput.Focus();
        }

        /// <summary>
        /// Closes the input form (this form...) and resets the search result and the text input
        /// </summary>
        public void HideJeffrey()
        {
            if (!Store.Get<ThemeManager>().IsLiveStyling)
            {
                this.Clear();

                this.txtInput.Text = "";
                this.Hide();
                this.contextManager.ResetContext();

                GC.Collect();
            }
        }

        /// <summary>
        /// Removes all search results
        /// </summary>
        private void Clear()
        {
            this.resultPanel.Clear();
        }

        /// <summary>Changes and applies a new plugin</summary>
        /// <param name="style">The style to be used</param>
        public void UpdateStyle(Theme style)
        {
            System.Diagnostics.Debug.WriteLine("UPDATING THEME");

            this.SuspendLayout();

            this.Opacity = style.Opacity;

            if (this.fade != null)
                this.fade.EndOpacity = this.Opacity;

            this.Width = style.Width;

            //this.BackColor = activeTheme.Background;

            this.txtInput.Font = style.SearchFont;
            this.txtInput.BackColor = style.SearchBackground;
            this.txtInput.ForeColor = style.SearchTextColor;
            this.txtInput.BorderStyle = BorderStyle.None;
            this.txtInput.Location = new Point(style.SearchPadding, style.SearchPadding);

            // Panel inside InnerPanel
            this.panelSearchPadding.Location = new Point(style.InnerPadding, style.InnerPadding);
            this.panelSearchPadding.BackColor = this.txtInput.BackColor;

            // Panel inside form
            this.panelInnerPadding.Location = new Point(style.Padding, style.Padding);
            this.panelInnerPadding.BackColor = style.PaddingColor;

            // Result Panel
            this.resultPanel.UpdateTheme(style);
            
            this.ResumeLayout(false);
            this.PerformLayout();

            int searchFontHeight = TextRenderer.MeasureText("M", style.SearchFont).Height;

            this.panelInnerPadding.Size = new Size(
                style.Width - (style.Padding * 2),
                this.panelSearchPadding.Height + (style.InnerPadding * 2) + style.ResultTopMargin);

            this.panelSearchPadding.Size = new Size(
                this.panelInnerPadding.Width - (style.InnerPadding * 2),
                searchFontHeight + (style.SearchPadding * 2));

            this.txtInput.Size = new Size(
                this.panelSearchPadding.Width - (style.SearchPadding * 2),
                searchFontHeight);

            this.resultPanel.Location = new Point(
                style.InnerPadding, 
                this.panelSearchPadding.Bottom);

            this.resultPanel.Size = new Size(
                this.panelInnerPadding.Width - (style.InnerPadding * 2),
                this.panelInnerPadding.Height - this.panelSearchPadding.Bottom - style.ResultTopMargin);

            this.ClientSize = new Size(
                style.Width,
                this.panelInnerPadding.Height + (style.Padding * 2));

            if (!Store.Get<ThemeManager>().IsLiveStyling)
            {
                Screen workingScreen = Screen.FromPoint(Tools.GetMousePosition());
                this.Location = new Point(
                    workingScreen.Bounds.X + (int)(((double)workingScreen.Bounds.Width * .5d) - ((double)this.ClientSize.Width * .5d)),
                    200
                );
            }

            this.CreateBorder();

            this.Refresh();
        }

        /// <summary>
        /// <para>Runs an app with a given index (if available)</para>
        /// The index usually is between 0 and the visible app count (see <see cref="Theme"/>)
        /// </summary>
        /// <param name="index">The zero based index of the app</param>
        public void RunApp(int index)
        {
            App app = this.resultPanel.GetAppByVisibleIndex(index);
            RunApp(app);
        }
        /// <summary>Runs a given app</summary>
        /// <param name="app">
        /// <para>The app to be run</para>
        /// Can be null
        /// </param>
        public void RunApp(App app)
        {
            if (app != null)
            {
                ISearchContext context = null;
                try
                {
                    context = app.GetContext();
                }
                catch (Exception ex)
                {
                    HandleAppException(ex);
                    return;
                }
                if (context == null)
                {
                    try
                    {
                        new Thread(new ThreadStart(app.Run)).Start();
                    }
                    catch (Exception ex)
                    {
                        HandleAppException(ex);
                        return;
                    }

                    this.HideJeffrey();

                    if (app.SaveStatistics)
                        this.statisticsManager.AddOpen(this.txtInput.Text, app.Name);
                }
                else
                {
                    this.Clear();
                    this.txtInput.Text = "";

                    this.contextManager.SetActiveContext(context);
                }
            }
        }

        private void HandleAppException(Exception ex)
        {
            Store.Get<ErrorLog>().HandleError(ex);
            this.contextManager.SetActiveContext(new MessageContext("Oops, something did go wrong with this one", "Please try again."));
        }

        /// <summary>
        /// Creates the borders (corner-roundness) for the form and the inner-padding-panel
        /// </summary>
        private void CreateBorder()
        {
            if (this.activeTheme.CornerRoundness != 0)
            {
                this.Region = Region.FromHrgn(Tools.CreateRoundRectRgn(0, 0, this.Width, this.Height, this.activeTheme.CornerRoundness, this.activeTheme.CornerRoundness));
            }
            else
            {
                this.Region = new Region(new Rectangle(0, 0, this.Width, this.Height));
            }

            if (this.activeTheme.InnerCornerRoundness != 0)
            {
                this.panelInnerPadding.Region = Region.FromHrgn(Tools.CreateRoundRectRgn(0, 0, this.panelInnerPadding.Width, this.panelInnerPadding.Height, this.activeTheme.InnerCornerRoundness, this.activeTheme.InnerCornerRoundness));
            }
            else
            {
                this.panelInnerPadding.Region = new Region(new Rectangle(0, 0, this.panelInnerPadding.Width, this.panelInnerPadding.Height));
            }
        }

        /// <summary>
        /// <para>Converts the Keys D1-D9 to their zero based related integers (0-9)</para>
        /// Example: Keys.D1 will return 0
        /// </summary>
        /// <param name="key">The key to be converted</param>
        /// <returns>The related, zero-based integer for the key or -1 if invalid key</returns>
        private int KeyCodeToNumber(Keys key)
        {
            switch (key)
            {
                case Keys.D1: return 0;
                case Keys.D2: return 1;
                case Keys.D3: return 2;
                case Keys.D4: return 3;
                case Keys.D5: return 4;
                case Keys.D6: return 5;
                case Keys.D7: return 6;
                case Keys.D8: return 7;
                case Keys.D9: return 8;
            }
            return -1;
        }

        /// <summary>
        /// Recomputes the heights of all panels (and this frame) related to the number of given/visible apps
        /// </summary>
        /// <param name="appCount">The number of visible apps</param>
        /// <returns>true if a recomputation was required</returns>
        private bool RecomputeSize(int appCount)
        {
            return RecomputeSize(appCount, false);
        }
        /// <summary>
        /// Recomputes the heights of all panels (and this frame) related to the number of given/visible apps
        /// </summary>
        /// <param name="appCount">The number of visible apps</param>
        /// <param name="force">Indicator if a recoputation should be enforced</param>
        /// <returns>true if a recomputation was required</returns>
        private bool RecomputeSize(int appCount, bool force)
        {
            bool changed = force || (this.previousAppCount != appCount); 
            if (changed) {
                int resultTopMargin = (appCount == 0) ? 0 : this.activeTheme.ResultTopMargin;

                this.panelInnerPadding.Height = (appCount * this.activeTheme.ResultHeight) + (this.activeTheme.InnerPadding * 2) + (this.panelSearchPadding.Height) + resultTopMargin;
                this.resultPanel.Height = (this.panelInnerPadding.Height - this.panelSearchPadding.Bottom - this.activeTheme.InnerPadding);
                this.Size = new System.Drawing.Size(this.Width, this.panelInnerPadding.Height + (this.activeTheme.Padding * 2));

                this.CreateBorder();
                this.Update();
            }
            this.previousAppCount = appCount;

            return changed;
        }

        #region EventHandler

        private void InputForm_ThemeChanged(Theme oldTheme, Theme newTheme)
        {
            if (this.InvokeRequired)
                this.Invoke(new ThemeChangedCallback(this.InputForm_ThemeChanged), oldTheme, newTheme);
            else
            {
                this.activeTheme = newTheme;
                this.UpdateStyle(this.activeTheme);
                this.RecomputeSize(this.currentAppCount, true);
                this.resultPanel.Invalidate();
            }
        }

        void txtInput_SpecialKeyPressed(Keys keyData)
        {
            switch (keyData)
            {
                case Keys.Escape:
                    this.HideJeffrey();
                    break;
                case Keys.Enter:
                    this.RunApp(this.resultPanel.SelectedApp);
                    break;
            }
        }
        
        private void txtInput_KeyDown(object sender, KeyEventArgs e)
        {
            this.statisticsManager.AddKeyDown();
            switch (e.KeyCode)
            {
                case Keys.Up:
                    this.resultPanel.Scroll(-1);
                    e.Handled = true;
                    break;
                case Keys.Down:
                    this.resultPanel.Scroll(1);
                    e.Handled = true;
                    break;
                case Keys.PageUp:
                    this.resultPanel.Scroll(-this.activeTheme.ResultCount);
                    e.Handled = true;
                    break;
                case Keys.PageDown:
                    this.resultPanel.Scroll(this.activeTheme.ResultCount);
                    e.Handled = true;
                    break;
                case Keys.Back:
                    if (this.txtInput.Text.Length == 0 && resetContextOnNextBackspace)
                    {
                        this.contextManager.ResetContext();
                    }
                    else
                    {
                        this.resetContextOnNextBackspace = true;
                    }
                    break;
                case Keys.D1:
                case Keys.D2:
                case Keys.D3:
                case Keys.D4:
                case Keys.D5:
                case Keys.D6:
                case Keys.D7:
                case Keys.D8:
                case Keys.D9:
                    int index = KeyCodeToNumber(e.KeyCode);
                    if ((e.Modifiers & Keys.Control) == Keys.Control && index >= 0 && index < this.currentAppCount)
                    {
                        this.RunApp(index);
                        e.Handled = true;
                    }
                    break;
            }
            if (e.KeyCode != Keys.Back)
                this.resetContextOnNextBackspace = false;
        }

        private void txtInput_TextChanged(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(this.txtInput.Text) && this.contextManager.ActiveContext == this.contextManager.DefaultContext)
            {
                this.Clear();
                this.RecomputeSize(0, true);
            }
            else
            {
                this.contextManager.Search(this.txtInput.Text);

                if (this.fade.Running)
                    this.fade.Stop();
            }
        }

        private void store_SearchComplete(SearchResultEventArgs ev)
        {
            if (this.InvokeRequired)
            {
                try
                {
                    this.Invoke(new SearchCompleteCallback(this.store_SearchComplete), ev);
                }
                catch (Exception ex) { Store.Get<ErrorLog>().HandleError(ex); }
            }
            else
            {
                this.SuspendLayout();

                App[] result = ev.Result;
#if !COLOR_HIGHLIGHT
                this.resultPanel.UpdateApps(result);
#endif

                this.currentAppCount = Math.Min(result.Length, this.activeTheme.ResultCount);
                this.RecomputeSize(this.currentAppCount);

                this.ResumeLayout(false);
                this.PerformLayout();
            }
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            //base.OnPaintBackground(e);
            e.Graphics.FillRectangle(this.activeTheme.Background, this.ClientRectangle);
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;

namespace JeffreyPlease
{
    public delegate void ObjectAddedEventHandler(object objectAdded);

    /// <summary>
    /// <para>The Store is a static key-value instance manager</para>
    /// which uses the type of the stored instance as the identifier (key)
    /// </summary>
    public class Store
    {
        /// <summary>The store itself</summary>
        private static Dictionary<Type, object> store = new Dictionary<Type, object>();

        public static event ObjectAddedEventHandler ObjectAdded;

        /// <summary>
        /// <para>Adds an instance of Type T to the store</para>
        /// <para>If an element of that Type is already stored, the function</para>
        /// will return the existing instance
        /// </summary>
        /// <typeparam name="T">The type of the object</typeparam>
        /// <param name="instance">The instance (of Type T)</param>
        /// <returns>The stored instance</returns>
        public static T Put<T>(T instance)
        {
            Type type = typeof(T);
            if (!store.ContainsKey(type))
                store.Add(type, instance);
            else
                instance = (T)store[type];

            if (ObjectAdded != null)
                ObjectAdded.Invoke(instance);

            return instance;
        }

        /// <summary>
        /// <para>Returns the instance for the given type T</para>
        /// <para>If an instance of the Type T does not exist in the store</para>
        /// The default value for that Type will be returned (usually null)
        /// </summary>
        /// <typeparam name="T">The type of the object to be returned</typeparam>
        /// <returns>The instance of the stored object with the Type T</returns>
        public static T Get<T>()
        {
            Type type = typeof(T);

            if (store.ContainsKey(type))
            {
                return (T)store[type];
            }
            return default(T);
        }
    }
}

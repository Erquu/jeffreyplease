﻿using JeffreyPlease.UI;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JeffreyPlease
{
    public class Notification
    {
        public static readonly int DEFAULT_SHOW_TIME = 5000;

        private NotificationForm form;

        public Icon Icon { get{ return form.Icon; } set{ form.Icon = value; } }
        public string BigText { get{ return form.BigText; } set{ form.BigText = value; } }
        public string SmallText{ get{ return form.SmallText; } set{ form.SmallText = value; } }
        public int ShowTime { get{ return form.ShowTime; } set{ form.ShowTime = value; } }

        private Notification()
        {
            this.form = new NotificationForm();
        }

        public static Notification Show(Icon icon)
        {
            return Show(icon, DEFAULT_SHOW_TIME);
        }
        public static Notification Show(Icon icon, int showTime)
        {
            return Show(icon, "", showTime);
        }
        public static Notification Show(Icon icon, string bigText)
        {
            return Show(icon, bigText, DEFAULT_SHOW_TIME);
        }
        public static Notification Show(Icon icon, string bigText, int showTime)
        {
            return Show(icon, bigText, "", showTime);
        }
        public static Notification Show(Icon icon, string bigText, string smallText)
        {
            return Show(icon, bigText, smallText, DEFAULT_SHOW_TIME);
        }
        public static Notification Show(Icon icon, string bigText, string smallText, int showTime)
        {
            Notification n = new Notification();
            n.form.Icon = icon;
            n.form.BigText = bigText;
            n.form.SmallText = smallText;
            n.form.ShowTime = showTime;

            FormInvoker.Invoke(n.form, new MethodInvoker(delegate()
            {
                n.form.TopMost = true;
                n.form.Show();
                n.form.BringToFront();
            }));

            return n;
        }
    }

    internal class NotificationForm : Form
    {
        private static readonly uint WS_EX_NOACTIVATE = 0x08000000;
        private static readonly uint WS_EX_TOOLWINDOW = 0x00000080;

        private static readonly int STARTING_HEIGHT = 2;
        private static readonly float CLICKTOCLOSE_PADDING = 3;

        private float currentWidth, currentHeight;
        private float baseWidth, baseHeight;

        private float baseSpeedX = .1f;
        private float baseSpeedY = .08f;

        private string bigText = "";
        private string smallText = "";

        private bool isClosing = false;
        private bool recompute = false;
        private bool autoClose = true;

        private bool showAddition = false;

        private int showTime = Notification.DEFAULT_SHOW_TIME;

        private Icon icon = null;

        private Thread showThread = null;

        public string BigText
        {
            get { return this.bigText; }
            set
            {
                this.bigText = value;
                this.Reset();
                this.recompute = true;
                this.Invalidate();
            }
        }
        public string SmallText
        {
            get { return this.smallText; }
            set
            {
                this.smallText = value;
                this.Reset();
                this.recompute = true;
                this.Invalidate();
            }
        }
        public new Icon Icon
        {
            get { return this.icon; }
            set
            {
                this.icon = value;
                this.Reset();
                this.recompute = true;
                this.Invalidate();
            }
        }
        public int ShowTime
        {
            get
            {
                return this.showTime;
            }
            set
            {
                if (value < 0)
                {
                    this.autoClose = false;
                }
                else
                {
                    this.autoClose = true;
                    this.showTime = value;
                }
                this.Reset();
            }
        }

        internal NotificationForm()
        {
            SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer | ControlStyles.UserPaint, true);

            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MinimizeBox = false;
            this.MaximizeBox = false;

            this.Height = 60;
            this.Width = 300;

            this.StartPosition = FormStartPosition.CenterScreen;

            this.BackColor = Color.FromArgb(0, 59, 106);
        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams baseParams = base.CreateParams;

                // No click capture, no focus
                baseParams.ExStyle |= (int)(WS_EX_NOACTIVATE | WS_EX_TOOLWINDOW);

                // no focus
                //baseParams.ExStyle |= (int)(WS_EX_NOACTIVATE);

                return baseParams;
            }
        }

        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);

            this.Reset();
            this.CalculateSize();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            this.ShowInTaskbar = false;

            this.MinimumSize = new Size(1, 1);

            this.baseWidth = (float)this.Width;
            this.baseHeight = (float)this.Height;

            this.currentWidth = (float)(this.Width = 1);
            this.currentHeight = (float)(this.Height = STARTING_HEIGHT);

            Screen s = Screen.FromControl(this);
            this.Location = new Point(this.Location.X, s.Bounds.Height - 150);
        }

        private void CalculateSize()
        {
            ThemeManager tm = Store.Get<ThemeManager>();
            Theme theme = tm.ActiveTheme;

            Graphics g = Graphics.FromImage(new Bitmap(100, 100));

            float width = (theme.Padding + theme.InnerPadding) * 2;
            float height = width;

            float fontHeight = 0;
            float fontWidth = 0;

            SizeF fontSize = g.MeasureString(this.bigText, theme.ResultFont);
            fontWidth = Math.Max(fontWidth, fontSize.Width);
            fontHeight += fontSize.Height;

            fontSize = g.MeasureString(this.smallText, theme.ResultSubFont);
            fontWidth = Math.Max(fontWidth, fontSize.Width);
            fontHeight += fontSize.Height;

            fontHeight += CLICKTOCLOSE_PADDING;

            if (this.icon != null)
            {
                width += theme.ResultIconMargin;
                width += fontHeight;
            }

            if (showAddition)
            {
                fontSize = g.MeasureString("Click to close", theme.SelectedResultSubFont);
                fontWidth = Math.Max(fontWidth, fontSize.Width);
                fontHeight += fontSize.Height + (CLICKTOCLOSE_PADDING * 2);
            }

            width += fontWidth;
            height += fontHeight;

            this.baseWidth = width;
            this.baseHeight = height;
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            ThemeManager tm = Store.Get<ThemeManager>();
            Theme activeTheme = tm.ActiveTheme;

            bool invalidate = false;

            if (this.recompute)
            {
                this.CalculateSize();
                this.recompute = false;
            }

            if (!isClosing)
            {
                if (this.currentWidth < this.baseWidth)
                {
                    this.Width = (int)(this.currentWidth += this.baseSpeedX);
                    if (this.Width > this.baseWidth)
                        this.currentWidth = this.baseWidth;
                    invalidate = true;
                }
                else if (this.currentHeight < this.baseHeight)
                {
                    this.Height = (int)(this.currentHeight += this.baseSpeedY);
                    if (this.Height > this.baseHeight)
                        this.currentHeight = this.baseHeight;
                    invalidate = true;
                }
                if (this.currentWidth > this.baseWidth)
                {
                    this.Width = (int)(this.currentWidth -= this.baseSpeedX);
                    if (this.Width < this.baseWidth)
                        this.currentWidth = this.baseWidth;
                    invalidate = true;
                }
                else if (this.currentHeight > this.baseHeight)
                {
                    this.Height = (int)(this.currentHeight -= this.baseSpeedY);
                    if (this.Height < this.baseHeight)
                        this.currentHeight = this.baseHeight;
                    invalidate = true;
                }
            }
            else
            {
                if (this.currentHeight > STARTING_HEIGHT)
                {
                    int height = (int)(this.currentHeight -= this.baseSpeedY);
                    if (height < STARTING_HEIGHT)
                        height = STARTING_HEIGHT;
                    this.Height = height;
                    invalidate = true;
                }
                else if (this.currentWidth > 0)
                {
                    int width = (int)(this.currentWidth -= this.baseSpeedX);
                    if (width <= 1)
                    {
                        this.Close();
                        invalidate = false;
                    }
                    else
                    {
                        this.Width = width;
                        invalidate = true;
                    }
                }
            }

            g.FillRectangle(activeTheme.Background, this.ClientRectangle);

            float h = this.baseHeight;

            if (showAddition)
            {
                SizeF fontSize = g.MeasureString("Click to close", activeTheme.SelectedResultSubFont);
                h -= fontSize.Height + (CLICKTOCLOSE_PADDING * 2);

                g.FillRectangle(activeTheme.SelectedResultBackground, 0, h, this.Width, fontSize.Height + (CLICKTOCLOSE_PADDING * 2f));
            }

            float allPadding = activeTheme.Padding;
            g.FillRectangle(
                new SolidBrush(activeTheme.PaddingColor),
                allPadding,
                allPadding,
                this.ClientRectangle.Width - (allPadding * 2.0f),
                h - (allPadding * 2.0f)
            );

            allPadding += activeTheme.InnerPadding;
            g.FillRectangle(
                activeTheme.ResultBackground,
                allPadding,
                allPadding,
                this.ClientRectangle.Width - (allPadding * 2.0f),
                h - (allPadding * 2.0f)
            );

            if (invalidate)
            {
                this.Invalidate();
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            ThemeManager tm = Store.Get<ThemeManager>();
            Theme theme = tm.ActiveTheme;

            float offsetX = theme.Padding + theme.InnerPadding;
            float offsetY = offsetX;

            SizeF fontSizeBig = g.MeasureString(this.bigText, theme.ResultFont);
            SizeF fontSizeSmall = g.MeasureString(this.smallText, theme.ResultSubFont);

            if (this.icon != null)
            {
                float padding = theme.Padding + theme.InnerPadding;
                float height = fontSizeBig.Height + fontSizeSmall.Height;
                g.DrawIcon(this.icon, new Rectangle(
                    (int)offsetX,
                    (int)offsetY,
                    (int)height,
                    (int)height
                ));
                offsetX += height + theme.ResultIconMargin;
            }
            g.DrawString(this.bigText, theme.ResultFont, theme.ResultTextColor, offsetX, offsetY);

            offsetY += fontSizeSmall.Height + CLICKTOCLOSE_PADDING;

            g.DrawString(this.smallText, theme.ResultSubFont, theme.ResultSubtextColor, offsetX, offsetY);

            if (this.showAddition)
            {
                SizeF fontSize = g.MeasureString("Click to close", theme.SelectedResultSubFont);
                offsetX = (float)this.Width * .5f - fontSize.Width * .5f;
                offsetY = this.baseHeight - fontSize.Height - (CLICKTOCLOSE_PADDING);
                g.DrawString("Click to close", theme.SelectedResultSubFont, theme.SelectedResultTextColor, offsetX, offsetY);
            }
        }

        protected override void OnClick(EventArgs e)
        {
            if (!this.autoClose)
            {
                this.isClosing = true;
                this.Invalidate();
            }
        }

        private float DrawCentered(string s, Font f, Graphics g, float offsetY)
        {
            SizeF size = g.MeasureString(s, f);

            float w = (float)this.Width;
            float hw = w * .5f;

            float sw = size.Width * .5f;

            g.DrawString(s, f, Brushes.White, new PointF(hw - sw, offsetY));

            return size.Height;
        }

        private void Reset()
        {
            this.isClosing = false;

            if (autoClose)
            {
                if (this.showThread != null)
                {
                    this.showThread.Abort();
                    this.showThread = null;
                }

                this.showThread = new Thread(ShowThreadCallback);
                this.showThread.Start();
            }
        }

        private void ShowThreadCallback()
        {
            Thread.Sleep(this.ShowTime);

            if (this.autoClose)
            {
                this.isClosing = true;
            }
            else
            {
                this.showAddition = true;
                this.CalculateSize();
            }

            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(this.Invalidate));
            }
            else
            {
                this.Invalidate();
            }
        }
    }
}

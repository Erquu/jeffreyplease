﻿using JeffreyPlease.Settings;
using JeffreyPlease.Utils;
using JSONdotNET;
using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace JeffreyPlease
{
    public partial class SettingsView : Form
    {
        private SettingsManager settingsManager;

        public SettingsView()
        {
            InitializeComponent();

            settingsManager = Store.Get<SettingsManager>();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            LoadSettings();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            Save();

            base.OnClosing(e);
        }

        private void LoadSettings()
        {
            JSONObject settingsObject = this.settingsManager.SettingsObject;

            KeySequence sequence = Store.Get<KeySequence>();
            foreach (Keys key in sequence.keySequence)
            {
                switch (key)
                {
                    case Keys.LControlKey: keyStrg.Checked = true; break;
                    case Keys.LShiftKey: keyShift.Checked = true; break;
                    case ((Keys)164 /* ALT */): keyAlt.Checked = true; break;
                    case Keys.Space: keySpace.Checked = true; break;
                }
            }

            if (settingsObject.ContainsKey("excludePattern"))
            {
                this.txtRegex.Text = settingsObject.GetString("excludePattern");
            }
            if (settingsObject.ContainsKey("repositories"))
            {
                JSONObject repoObj = settingsObject["repositories"];
                if (repoObj is JSONArray) {
                    JSONArray repos = (JSONArray)repoObj;
                    for (int i = 0; i < repos.Length; i++)
                    {
                        JSONObject repo = repos[i];
                        if (repo.ContainsKey("name") && repo.ContainsKey("url"))
                        {
                            repoControl.Add(repo.GetString("name"), repo.GetString("url"), false);
                        }
                    }
                }
            }
            if (settingsObject.ContainsKey("searchPaths"))
            {
                JSONObject pathsObj = settingsObject["searchPaths"];
                if (pathsObj is JSONArray)
                {
                    JSONArray paths = (JSONArray)pathsObj;
                    for (int i = 0; i < paths.Length; i++)
                    {
                        JSONObject path = paths[i];
                        if (path.ContainsKey("path"))
                        {
                            bool recursive = false;
                            if (path.ContainsKey("recursive"))
                                recursive = path.GetBool("recursive");
                            searchPathControl.Add(path.GetString("path"), recursive, false);
                        }
                    }
                }
            }
            if (settingsObject.ContainsKey("proxy"))
            {
                JSONObject proxy = settingsObject["proxy"];

                if (proxy.ContainsKey("host"))
                    txtProxyHost.Text = proxy.GetString("host");
                if (proxy.ContainsKey("port"))
                    numProxyPort.Value = proxy.GetInt("port");
                if (proxy.ContainsKey("active"))
                    chkProxyActive.Checked = proxy.GetBool("active");
            }
            if (settingsObject.ContainsKey("developer"))
            {
                chkDeveloper.Checked = settingsObject.GetBool("developer");
            }

            chkAutostart.Checked = Autostart.IsAutoStarting;
        }

        private void Save()
        {
            JSONArray keys = new JSONArray();
            if (keyStrg.Checked) keys.Add(Enum.GetName(typeof(Keys), Keys.LControlKey));
            if (keyShift.Checked) keys.Add(Enum.GetName(typeof(Keys), Keys.LShiftKey));
            if (keyAlt.Checked) keys.Add("Alt");
            if (keySpace.Checked) keys.Add(Enum.GetName(typeof(Keys), Keys.Space));

            settingsManager.AddSetting("openingSequence", keys);

            repoControl.Save(settingsManager);
            searchPathControl.Save(settingsManager);

            JSONObject proxyObject = new JSONObject();
            proxyObject.Add("host", txtProxyHost.Text);
            proxyObject.Add("port", Convert.ToInt32(numProxyPort.Value));
            proxyObject.Add("active", chkProxyActive.Checked);
            settingsManager.AddSetting("proxy", proxyObject);

            settingsManager.AddSetting("developer", new JSONLiteralNode(chkDeveloper.Checked.ToString().ToLower()));

            settingsManager.Save();

            // Non settings related saves
            Autostart.EnableAutostart(chkAutostart.Checked);
        }
    }
}

﻿using System.Drawing;
using System.Windows.Forms;

namespace JeffreyPlease
{
    public partial class SplashScreen : Form
    {
        private delegate void UpdateStatusCallback(string text);

        public SplashScreen()
        {
            InitializeComponent();
        }

        public void UpdateStatus(string text)
        {
            if (this.InvokeRequired)
            {
                UpdateStatusCallback callback = new UpdateStatusCallback(this.Test);
                this.Invoke(callback, text);
            }
        }
        private void Test(string text)
        {
            this.lblLoadingText.Text = text;
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            base.OnPaintBackground(e);

            e.Graphics.DrawRectangle(Pens.Gray, 5, 5, this.Width - 10, this.Height - 10);
        }
    }
}

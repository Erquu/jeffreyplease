﻿namespace JeffreyPlease
{
    partial class SettingsView
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabGeneral = new System.Windows.Forms.TabPage();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.chkAutostart = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tabConnection = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkProxyActive = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.numProxyPort = new System.Windows.Forms.NumericUpDown();
            this.txtProxyHost = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPlugins = new System.Windows.Forms.TabPage();
            this.tabAdvanced = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblRegexHelp = new System.Windows.Forms.Label();
            this.lblRegex = new System.Windows.Forms.Label();
            this.txtRegex = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.chkDeveloper = new System.Windows.Forms.CheckBox();
            this.keyStrg = new JeffreyPlease.UI.Controls.NoCheckbox();
            this.keySpace = new JeffreyPlease.UI.Controls.NoCheckbox();
            this.keyShift = new JeffreyPlease.UI.Controls.NoCheckbox();
            this.keyAlt = new JeffreyPlease.UI.Controls.NoCheckbox();
            this.pluginViewControl1 = new JeffreyPlease.UI.Controls.PluginViewControl();
            this.repoControl = new JeffreyPlease.UI.Controls.RepoControl();
            this.searchPathControl = new JeffreyPlease.UI.Controls.SearchPathControl();
            this.tabControl1.SuspendLayout();
            this.tabGeneral.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabConnection.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numProxyPort)).BeginInit();
            this.tabPlugins.SuspendLayout();
            this.tabAdvanced.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabGeneral);
            this.tabControl1.Controls.Add(this.tabConnection);
            this.tabControl1.Controls.Add(this.tabPlugins);
            this.tabControl1.Controls.Add(this.tabAdvanced);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(503, 379);
            this.tabControl1.TabIndex = 10;
            // 
            // tabGeneral
            // 
            this.tabGeneral.Controls.Add(this.label6);
            this.tabGeneral.Controls.Add(this.label5);
            this.tabGeneral.Controls.Add(this.label4);
            this.tabGeneral.Controls.Add(this.chkAutostart);
            this.tabGeneral.Controls.Add(this.label3);
            this.tabGeneral.Controls.Add(this.panel2);
            this.tabGeneral.Location = new System.Drawing.Point(4, 22);
            this.tabGeneral.Name = "tabGeneral";
            this.tabGeneral.Size = new System.Drawing.Size(495, 353);
            this.tabGeneral.TabIndex = 3;
            this.tabGeneral.Text = "General";
            this.tabGeneral.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.DimGray;
            this.label6.Location = new System.Drawing.Point(63, 104);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(368, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Select to let JeffreyPlease start automatically after you logged in (to windows)";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.DimGray;
            this.label5.Location = new System.Drawing.Point(63, 58);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(309, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Select the keys you want to press in order to open JeffreyPlease";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(13, 85);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Startup";
            // 
            // chkAutostart
            // 
            this.chkAutostart.AutoSize = true;
            this.chkAutostart.Location = new System.Drawing.Point(66, 84);
            this.chkAutostart.Name = "chkAutostart";
            this.chkAutostart.Size = new System.Drawing.Size(99, 17);
            this.chkAutostart.TabIndex = 12;
            this.chkAutostart.Text = "Launch at login";
            this.chkAutostart.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(13, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Hotkey";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Silver;
            this.panel2.Controls.Add(this.keyStrg);
            this.panel2.Controls.Add(this.keySpace);
            this.panel2.Controls.Add(this.keyShift);
            this.panel2.Controls.Add(this.keyAlt);
            this.panel2.Location = new System.Drawing.Point(66, 10);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(343, 45);
            this.panel2.TabIndex = 10;
            // 
            // tabConnection
            // 
            this.tabConnection.Controls.Add(this.groupBox2);
            this.tabConnection.Location = new System.Drawing.Point(4, 22);
            this.tabConnection.Name = "tabConnection";
            this.tabConnection.Size = new System.Drawing.Size(495, 353);
            this.tabConnection.TabIndex = 2;
            this.tabConnection.Text = "Connection";
            this.tabConnection.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chkProxyActive);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.numProxyPort);
            this.groupBox2.Controls.Add(this.txtProxyHost);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(231, 98);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Proxy";
            // 
            // chkProxyActive
            // 
            this.chkProxyActive.AutoSize = true;
            this.chkProxyActive.Location = new System.Drawing.Point(67, 71);
            this.chkProxyActive.Name = "chkProxyActive";
            this.chkProxyActive.Size = new System.Drawing.Size(55, 17);
            this.chkProxyActive.TabIndex = 4;
            this.chkProxyActive.Text = "active";
            this.chkProxyActive.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(35, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Port";
            // 
            // numProxyPort
            // 
            this.numProxyPort.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numProxyPort.Location = new System.Drawing.Point(67, 45);
            this.numProxyPort.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.numProxyPort.Name = "numProxyPort";
            this.numProxyPort.Size = new System.Drawing.Size(158, 20);
            this.numProxyPort.TabIndex = 2;
            this.numProxyPort.Value = new decimal(new int[] {
            8080,
            0,
            0,
            0});
            // 
            // txtProxyHost
            // 
            this.txtProxyHost.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtProxyHost.Location = new System.Drawing.Point(67, 19);
            this.txtProxyHost.Name = "txtProxyHost";
            this.txtProxyHost.Size = new System.Drawing.Size(158, 20);
            this.txtProxyHost.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Hostname";
            // 
            // tabPlugins
            // 
            this.tabPlugins.Controls.Add(this.pluginViewControl1);
            this.tabPlugins.Location = new System.Drawing.Point(4, 22);
            this.tabPlugins.Name = "tabPlugins";
            this.tabPlugins.Size = new System.Drawing.Size(495, 353);
            this.tabPlugins.TabIndex = 1;
            this.tabPlugins.Text = "Plugins";
            this.tabPlugins.UseVisualStyleBackColor = true;
            // 
            // tabAdvanced
            // 
            this.tabAdvanced.Controls.Add(this.tableLayoutPanel1);
            this.tabAdvanced.Location = new System.Drawing.Point(4, 22);
            this.tabAdvanced.Name = "tabAdvanced";
            this.tabAdvanced.Padding = new System.Windows.Forms.Padding(3);
            this.tabAdvanced.Size = new System.Drawing.Size(495, 353);
            this.tabAdvanced.TabIndex = 0;
            this.tabAdvanced.Text = "Advanced";
            this.tabAdvanced.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.2389F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 51.7611F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.repoControl, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.searchPathControl, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.groupBox1, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 92F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(489, 347);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblRegexHelp);
            this.panel1.Controls.Add(this.lblRegex);
            this.panel1.Controls.Add(this.txtRegex);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(229, 86);
            this.panel1.TabIndex = 6;
            // 
            // lblRegexHelp
            // 
            this.lblRegexHelp.AutoSize = true;
            this.lblRegexHelp.ForeColor = System.Drawing.Color.DimGray;
            this.lblRegexHelp.Location = new System.Drawing.Point(3, 29);
            this.lblRegexHelp.Name = "lblRegexHelp";
            this.lblRegexHelp.Size = new System.Drawing.Size(211, 52);
            this.lblRegexHelp.TabIndex = 5;
            this.lblRegexHelp.Text = "Define a regex pattern which all File-Names\r\nand File-Paths will be tested agains" +
    "t.\r\nIf it matches the file will not be added to\r\nthe list of programms.";
            // 
            // lblRegex
            // 
            this.lblRegex.AutoSize = true;
            this.lblRegex.Location = new System.Drawing.Point(3, 9);
            this.lblRegex.Name = "lblRegex";
            this.lblRegex.Size = new System.Drawing.Size(79, 13);
            this.lblRegex.TabIndex = 3;
            this.lblRegex.Text = "Exclude Regex";
            // 
            // txtRegex
            // 
            this.txtRegex.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtRegex.Location = new System.Drawing.Point(84, 6);
            this.txtRegex.Name = "txtRegex";
            this.txtRegex.Size = new System.Drawing.Size(142, 20);
            this.txtRegex.TabIndex = 4;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.chkDeveloper);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(238, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(248, 86);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Developer Mode";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.DimGray;
            this.label7.Location = new System.Drawing.Point(6, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(218, 39);
            this.label7.TabIndex = 6;
            this.label7.Text = "The developer mode will allow you\r\nto directly load dlls from the jeffrey\r\nplugin" +
    " folder instead of only .jeffreyplugin files";
            // 
            // chkDeveloper
            // 
            this.chkDeveloper.AutoSize = true;
            this.chkDeveloper.Location = new System.Drawing.Point(6, 63);
            this.chkDeveloper.Name = "chkDeveloper";
            this.chkDeveloper.Size = new System.Drawing.Size(59, 17);
            this.chkDeveloper.TabIndex = 0;
            this.chkDeveloper.Text = "Enable";
            this.chkDeveloper.UseVisualStyleBackColor = true;
            // 
            // keyStrg
            // 
            this.keyStrg.BackColor = System.Drawing.Color.Silver;
            this.keyStrg.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.keyStrg.Location = new System.Drawing.Point(5, 6);
            this.keyStrg.Name = "keyStrg";
            this.keyStrg.Size = new System.Drawing.Size(82, 33);
            this.keyStrg.TabIndex = 5;
            this.keyStrg.Text = "STRG";
            this.keyStrg.UseVisualStyleBackColor = false;
            // 
            // keySpace
            // 
            this.keySpace.BackColor = System.Drawing.Color.Silver;
            this.keySpace.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.keySpace.Location = new System.Drawing.Point(243, 6);
            this.keySpace.Name = "keySpace";
            this.keySpace.Size = new System.Drawing.Size(95, 33);
            this.keySpace.TabIndex = 8;
            this.keySpace.Text = "SPACE";
            this.keySpace.UseVisualStyleBackColor = false;
            // 
            // keyShift
            // 
            this.keyShift.BackColor = System.Drawing.Color.Silver;
            this.keyShift.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.keyShift.Location = new System.Drawing.Point(93, 6);
            this.keyShift.Name = "keyShift";
            this.keyShift.Size = new System.Drawing.Size(82, 33);
            this.keyShift.TabIndex = 6;
            this.keyShift.Text = "SHIFT";
            this.keyShift.UseVisualStyleBackColor = false;
            // 
            // keyAlt
            // 
            this.keyAlt.BackColor = System.Drawing.Color.Silver;
            this.keyAlt.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.keyAlt.Location = new System.Drawing.Point(181, 6);
            this.keyAlt.Name = "keyAlt";
            this.keyAlt.Size = new System.Drawing.Size(56, 33);
            this.keyAlt.TabIndex = 7;
            this.keyAlt.Text = "ALT";
            this.keyAlt.UseVisualStyleBackColor = false;
            // 
            // pluginViewControl1
            // 
            this.pluginViewControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pluginViewControl1.Location = new System.Drawing.Point(0, 0);
            this.pluginViewControl1.Name = "pluginViewControl1";
            this.pluginViewControl1.Size = new System.Drawing.Size(495, 353);
            this.pluginViewControl1.TabIndex = 0;
            // 
            // repoControl
            // 
            this.repoControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.repoControl.Location = new System.Drawing.Point(3, 95);
            this.repoControl.Name = "repoControl";
            this.repoControl.Size = new System.Drawing.Size(229, 249);
            this.repoControl.TabIndex = 7;
            // 
            // searchPathControl
            // 
            this.searchPathControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.searchPathControl.Location = new System.Drawing.Point(238, 95);
            this.searchPathControl.Name = "searchPathControl";
            this.searchPathControl.Size = new System.Drawing.Size(248, 249);
            this.searchPathControl.TabIndex = 8;
            // 
            // SettingsView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(527, 403);
            this.Controls.Add(this.tabControl1);
            this.Icon = global::JeffreyPlease.Properties.Resources.JeffreyPlease_white_stroked_64;
            this.MinimumSize = new System.Drawing.Size(543, 441);
            this.Name = "SettingsView";
            this.Text = "Settings";
            this.tabControl1.ResumeLayout(false);
            this.tabGeneral.ResumeLayout(false);
            this.tabGeneral.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.tabConnection.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numProxyPort)).EndInit();
            this.tabPlugins.ResumeLayout(false);
            this.tabAdvanced.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private UI.Controls.NoCheckbox keyStrg;
        private UI.Controls.NoCheckbox keyShift;
        private UI.Controls.NoCheckbox keyAlt;
        private UI.Controls.NoCheckbox keySpace;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabAdvanced;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblRegexHelp;
        private System.Windows.Forms.Label lblRegex;
        private System.Windows.Forms.TextBox txtRegex;
        private JeffreyPlease.UI.Controls.RepoControl repoControl;
        private UI.Controls.SearchPathControl searchPathControl;
        private System.Windows.Forms.TabPage tabPlugins;
        private UI.Controls.PluginViewControl pluginViewControl1;
        private System.Windows.Forms.TabPage tabConnection;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numProxyPort;
        private System.Windows.Forms.TextBox txtProxyHost;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chkProxyActive;
        private System.Windows.Forms.TabPage tabGeneral;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chkAutostart;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox chkDeveloper;
    }
}
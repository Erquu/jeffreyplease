﻿using JeffreyPlease.Properties;
using JeffreyPlease.UI;
using JeffreyPlease.Utils;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace JeffreyPlease
{
    /// <summary>
    /// <para>Form managing the the input form, the tray</para>
    /// and all the positioning stuff
    /// </summary>
    public class JeffreysClub
    {
        private delegate void VoidCallback();

        private NotifyIcon notifyJeffrey;
        private MenuItem callItem,
            settingsItem,
            closeItem;

        private InputForm inputForm;

        /// <summary>The constructor (as if you wouldn't know)</summary>
        public JeffreysClub()
        {
            this.inputForm = new InputForm();
        }

        public void Start()
        {
            this.notifyJeffrey = new NotifyIcon();
            this.notifyJeffrey.Icon = Resources.JeffreyPlease_white_stroked_64;
            this.notifyJeffrey.ContextMenu = CreateContextMenu();
            this.notifyJeffrey.Text = "JeffreyPlease" + Environment.NewLine + "Right click";
            this.notifyJeffrey.Visible = true;
        }

        public void Stop()
        {
            this.notifyJeffrey.Visible = false;
        }

        /// <summary>
        /// Checks if the given coordinate is inside the (open) input form
        /// </summary>
        /// <param name="x">The x coordinate</param>
        /// <param name="y">The y coordinate</param>
        public bool CheckBounds(int x, int y)
        {
            return !this.inputForm.ClientRectangle.Contains(x - this.inputForm.Left, y - this.inputForm.Top);
        }

        /// <summary>Shows and focuses the input form</summary>
        public void CallJeffrey()
        {
            Screen workingScreen = Screen.FromPoint(Tools.GetMousePosition());

            if (!Store.Get<ThemeManager>().IsLiveStyling)
            {
                this.inputForm.Location = new Point(
                    workingScreen.Bounds.X + (int)(((double)workingScreen.Bounds.Width * .5d) - ((double)this.inputForm.ClientSize.Width * .5d)),
                    200);
            }

            if (!this.inputForm.Visible)
            {
                this.inputForm.CallJeffrey();
            }
        }

        public void HideJeffrey()
        {
            this.inputForm.HideJeffrey();
        }

        private ContextMenu CreateContextMenu()
        {
            callItem = new MenuItem("Call Jeffrey", HandleMenuClick);
            settingsItem = new MenuItem("Show Settings", HandleMenuClick);
            closeItem = new MenuItem("Close", HandleMenuClick);

            ContextMenu contextMenu = new ContextMenu();
            contextMenu.MenuItems.Add(callItem);
            contextMenu.MenuItems.Add(settingsItem);
            contextMenu.MenuItems.Add("-"); // Seperator
            contextMenu.MenuItems.Add(closeItem);

            return contextMenu;
        }

        private void HandleMenuClick(object sender, EventArgs args)
        {
            if (sender == callItem)
            {
                this.CallJeffrey();
            }
            else if (sender == settingsItem)
            {
                (new SettingsView()).ShowDialog();
            }
            else if (sender == closeItem)
            {
                Application.Exit();
            }
        }

        private void notifyJeffrey_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
                this.CallJeffrey();
        }
    }
}

﻿using System;

namespace JSONdotNET
{
    /// <summary>
    /// Class representing a json text node
    /// </summary>
    public class JSONTextNode : JSONLiteralNode {
	
	    public JSONTextNode(string content) : base(content) { }

        /// <inheritdoc />
        public override JSONObjectType Type { get { return JSONObjectType.TEXT; } }

        /// <inheritdoc />
	    public override String ToString() {
		    return content;
	    }

        /// <inheritdoc />
	    public override String ToJSONString(int depth, bool minified) {
		    return String.Format("\"{0}\"", ToValidJSONString(content));
	    }
    }
}

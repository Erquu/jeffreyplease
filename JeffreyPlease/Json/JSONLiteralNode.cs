﻿using System;

namespace JSONdotNET
{
    /// <summary>
    /// Class representing a json literal node
    /// </summary>
    public class JSONLiteralNode : JSONObject {
        /// <summary>The content of the literal</summary>
	    protected string content;

        public override bool Empty
        {
            get
            {
                return String.IsNullOrWhiteSpace(content);
            }
        }
	
	    public JSONLiteralNode(string content) {
		    this.content = content;
	    }
	
        /// <summary>Returns the content of the node as a double precision binary floating point number</summary>
        /// <returns>The content of the node as a double precision binary floating point number</returns>
	    public double ToDouble() {
            return Double.Parse(content, NUMBER_FORMAT);
	    }
        /// <summary>Returns the content of the node as a sinlge precision binary floating point number</summary>
        /// <returns>The content of the node as a sinlge precision binary floating point number</returns>
	    public float ToFloat() {
            return float.Parse(content, NUMBER_FORMAT);
	    }
        /// <summary>Returns the content of the node as a boolean</summary>
        /// <returns>The content of the node as a boolean</returns>
	    public bool ToBoolean() {
		    return Boolean.Parse(content);
	    }
        /// <summary>Returns the content of the node as a 8 bit number</summary>
        /// <returns>The content of the node as a 8 bit number</returns>
        public byte ToByte()
        {
            return Byte.Parse(content, NUMBER_FORMAT);
        }
        /// <summary>Returns the content of the node as a 16 bit number</summary>
        /// <returns>The content of the node as a 16 bit number</returns>
        public short ToShort()
        {
            return Int16.Parse(content, NUMBER_FORMAT);
        }
        /// <summary>Returns the content of the node as a 32 bit number</summary>
        /// <returns>The content of the node as a 32 bit number</returns>
	    public int ToInt() {
            return Int32.Parse(content, NUMBER_FORMAT);
	    }
        /// <summary>Returns the content of the node as a 64 bit number</summary>
        /// <returns>The content of the node as a 64 bit number</returns>
        public long ToLong()
        {
            return Int64.Parse(content, NUMBER_FORMAT);
        }

        /// <inheritdoc />
        public override JSONObjectType Type { get { return JSONObjectType.LITERAL; } }

        /// <inheritdoc />
	    public override String ToJSONString(int depth, bool minified) {
		    return content;
	    }
    }
}

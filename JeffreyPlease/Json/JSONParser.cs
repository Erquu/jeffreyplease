﻿using System;
using System.IO;
using System.Text;

namespace JSONdotNET
{
    /// <summary>
    /// To become at least a bit as comfortable as the javascript JSON prototype, this class will allow static calls on JSONObject.Parse :)
    /// </summary>
    public class JSON
    {
        private static JSONParser instance
        {
            get
            {
                return new JSONParser();
            }
        }

        /// <summary>Parses a json document</summary>
        /// <param name="file">The file to parse</param>
        /// <returns>A JSONObject representing the root node</returns>
        public static JSONObject Parse(FileInfo file)
        {
            return instance.Parse(file);
        }
        /// <summary>Parses a json document</summary>
        /// <param name="file">The file to parse</param>
        /// <param name="encoding">The streams encoding</param>
        /// <returns>A JSONObject representing the root node</returns>
        public static JSONObject Parse(FileInfo file, Encoding encoding)
        {
            return instance.Parse(file, encoding);
        }
        /// <summary>Parses a json document</summary>
        /// <param name="stream">Document input stream to read from</param>
        /// <returns>A JSONObject representing the root node</returns>
        public static JSONObject Parse(Stream stream)
        {
            return instance.Parse(stream);
        }

        /// <summary>Parses a json document</summary>
        /// <param name="stream">Document input stream to read from</param>
        /// <param name="encoding">The streams encoding</param>
        /// <returns>A JSONObject representing the root node</returns>
        public static JSONObject Parse(Stream stream, Encoding encoding)
        {
            return instance.Parse(stream, encoding);
        }
        /// <summary>Parses a json document</summary>
        /// <param name="doc">The document to be parsed</param>
        /// <returns>A JSONObject representing the root node</returns>
        public static JSONObject Parse(string doc)
        {
            return instance.Parse(doc);
        }
    }

    /// <summary>
    /// The JSONParser is a simple and very fast parser for well formed json documents
    /// If the document is not valid the result may be different than expected without any exception thrown
    /// </summary>
    public class JSONParser {
        /// <summary>The document to be parsed</summary>
	    private string doc;
        /// <summary>The length of the document</summary>
	    private int length;
        /// <summary>The index pointer to the current char in the document</summary>
	    private int index = 0;

        /// <summary>Parses a json document</summary>
        /// <param name="file">The file to parse</param>
        /// <returns>A JSONObject representing the root node</returns>
        public JSONObject Parse(FileInfo file)
        {
            return Parse(file, Encoding.Default);
        }
        /// <summary>Parses a json document</summary>
        /// <param name="file">The file to parse</param>
        /// <param name="encoding">The file encoding</param>
        /// <returns>A JSONObject representing the root node</returns>
        public JSONObject Parse(FileInfo file, Encoding encoding)
        {
            Stream fileStream = file.OpenRead();
            JSONObject jsonObject = Parse(fileStream);
            fileStream.Close();
            return jsonObject;
        }
        /// <summary>Parses a json document</summary>
        /// <param name="stream">Document input stream to read from</param>
        /// <returns>A JSONObject representing the root node</returns>
        public JSONObject Parse(Stream stream)
        {
            return Parse(stream, Encoding.Default);
	    }
        /// <summary>Parses a json document</summary>
        /// <param name="stream">Document input stream to read from</param>
        /// <param name="encoding">The streams encoding</param>
        /// <returns>A JSONObject representing the root node</returns>
        public JSONObject Parse(Stream stream, Encoding encoding)
        {
            StreamReader reader = new StreamReader(stream, encoding);
		    StringBuilder builder = new StringBuilder();
		    string line;
            while ((line = reader.ReadLine()) != null)
            {
			    builder.Append(line);
		    }
		    return Parse(builder.ToString());
	    }
        /// <summary>Parses a json document</summary>
        /// <param name="doc">The document to be parsed</param>
        /// <returns>A JSONObject representing the root node</returns>
        public JSONObject Parse(string doc)
        {
		    return Parse(doc, null, false);
	    }
        /// <summary>
        /// Parses a json document
        /// </summary>
        /// <param name="doc">The json document to be parsed</param>
        /// <param name="parent">The parent node for the found child-node</param>
        /// <param name="isArray">Should the following node be parsed as an array</param>
        /// <returns>The create JSONObject representing the node</returns>
	    private JSONObject Parse(string doc, JSONObject parent, bool isArray) {
		    this.doc = doc;
		    this.length = doc.Length;
		
		    JSONObject currentObj = isArray ? (new JSONArray()) : (new JSONObject());
		    currentObj.Parent = parent;
		    bool isKey = true;
		    string lastValueKey = null;
		
		    while (index < length) {
			    char current = doc[(index++)];
			    switch (current) {
				    case '/':
					    if (doc[index] == '/') {
						    LineEnd();
					    } else if (doc[index] == '*') {
						    CommentEnd();
					    }
					    break;
				    case '\'':
				    case '"':
					    String nodeValue = StringEnd(current);
					    if (isArray) {
						    ((JSONArray)currentObj).Add(new JSONTextNode(nodeValue));
					    } else {
						    if (isKey) {
							    lastValueKey = nodeValue;
						    } else {
							    currentObj.Add(lastValueKey, new JSONTextNode(nodeValue));
						    }
					    }
					    break;
				    case ':':
					    isKey = !isKey;
					    break;
				    case ',':
					    isKey = true;
					    lastValueKey = null;
					    break;
				    case '[':
                        JSONObject value = Parse(doc, currentObj, true);
                        if (lastValueKey == null)
                            currentObj = value;
                        else
                            currentObj.Add(lastValueKey, value);
					    break;
				    case '{':
					    JSONObject obj = Parse(doc, currentObj, false);
					    if (isArray) {
						    ((JSONArray)currentObj).Add(obj);
					    } else {
                            if (lastValueKey == null)
                                return obj;
						    currentObj.Add(lastValueKey, obj);
					    }
					    break;
				    case ']':
				    case '}':
					    return currentObj;
				    default:
					    if (!Char.IsWhiteSpace(current)) {
                            if (isArray)
                            {
                                ((JSONArray)currentObj).Add(new JSONLiteralNode(LiteralEnd()));
                            }
                            else
                            {
                                if (!isKey)
                                {
                                    if (currentObj.ContainsKey(lastValueKey))
                                    {
                                        currentObj.Remove(lastValueKey);
                                    }
                                    currentObj.Add(lastValueKey, new JSONLiteralNode(LiteralEnd()));
                                }
                            }
					    }
					    break;
			    }
		    }
		
		    return currentObj;
	    }
	
        /// <summary>
        /// Goes to the end of a literal and returns it as a string
        /// Literals can be true, false, null or numbers and decimals like 1, 123, 3.1314
        /// </summary>
        /// <returns>The literal as a string</returns>
	    private string LiteralEnd() {
		    StringBuilder builder = new StringBuilder();
		    index--;
		    while (index < length) {
			    char currentChar = doc[index];
			    if (!IsLiteralChar(currentChar))
				    break;
			    builder.Append(currentChar);
			    index++;
		    }
		    return builder.ToString();
	    }

        /// <summary>Checks if a character is a valid literal json character</summary>
        /// <param name="c">The character to be checked</param>
        /// <returns>true if the character is valid</returns>
        private bool IsLiteralChar(char c)
        {
            char[] validChars = new char[] { '+', '-', '.' };
            return (Char.IsLetterOrDigit(c) || Array.BinarySearch(validChars, c) > -1);
        }
	
        /// <summary>Goes to the end of a string and returns the string content</summary>
        /// <param name="initChar">The character of the string start identifier like " or '</param>
        /// <returns>The content of the string without quotes</returns>
	    private string StringEnd(char initChar) {
		    StringBuilder builder = new StringBuilder();
		    while (index < length) {
			    char currentChar = doc[(index++)];
			    if (currentChar == '\\') {
				    builder.Append(doc[index]);
				    index++;
				    continue;
			    }
			    if (currentChar == initChar) {
				    break;
			    }
			    builder.Append(currentChar);
		    }
		    return builder.ToString();
	    }
	
        /// <summary>Sets the index pointer to the end of the current line</summary>
	    private void LineEnd() {
		    while (index < length) {
			    char currentChar = doc[(index++)];
			    if (currentChar == '\n') {
				    break;
			    }
		    }
	    }
	
        /// <summary>Sets the index pointer to the end of a multiline comment</summary>
	    private void CommentEnd() {
		    while (index < length) {
			    char currentChar = doc[(index++)];
			    if (currentChar == '*' && doc[index] == '/') {
				    index ++;
				    break;
			    }
		    }
	    }
    }
}

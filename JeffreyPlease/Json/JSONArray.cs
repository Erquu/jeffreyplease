﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JSONdotNET
{
    /// <summary>
    /// Class representing a json array
    /// </summary>
    public class JSONArray : JSONObject
    {
        /// <summary>A list of JSON Objects</summary>
        private List<JSONObject> objects = new List<JSONObject>();
        /// <summary>Gets the number of objects in this array</summary>
        public int Length { get { return objects.Count; } }

        public override bool Empty { get { return Length == 0; } }

        /// <summary>
        /// Returns the object at the given index
        /// </summary>
        /// <param name="index">The zero based index</param>
        /// <returns>The value at the given index</returns>
        public JSONObject this[int index] { get { return objects[index]; } }

        /// <inheritdoc />
        public override JSONObjectType Type { get { return JSONObjectType.ARRAY; } }

        /// <summary>Adds a JSONObject to the array</summary>
        /// <param name="obj">The object to be added</param>
        /// <returns>The instance of this JSONArray</returns>
        public JSONArray Add(JSONObject obj)
        {
            objects.Add(obj);
            return this;
        }
        /// <summary>Adds a string to the array</summary>
        /// <param name="value">The value</param>
        /// <returns>The instance of this JSONArray</returns>
        public JSONArray Add(string value)
        {
            objects.Add(new JSONTextNode(value));
            return this;
        }
        /// <summary>Adds a boolean to the array</summary>
        /// <param name="value">The value</param>
        /// <returns>The instance of this JSONArray</returns>
        public JSONArray Add(bool value)
        {
            objects.Add(new JSONLiteralNode(value.ToString().ToLower()));
            return this;
        }
        /// <summary>Adds a 8 bit number to the array</summary>
        /// <param name="value">The value</param>
        /// <returns>The instance of this JSONArray</returns>
        public JSONArray Add(byte value)
        {
            objects.Add(new JSONLiteralNode(value.ToString()));
            return this;
        }
        /// <summary>Adds a 16 bit number to the array</summary>
        /// <param name="value">The value</param>
        /// <returns>The instance of this JSONArray</returns>
        public JSONArray Add(short value)
        {
            objects.Add(new JSONLiteralNode(value.ToString()));
            return this;
        }
        /// <summary>Adds an unsigned 16 bit number to the array</summary>
        /// <param name="value">The value</param>
        /// <returns>The instance of this JSONArray</returns>
        public JSONArray Add(ushort value)
        {
            objects.Add(new JSONLiteralNode(value.ToString()));
            return this;
        }
        /// <summary>Adds a 32 bit number to the array</summary>
        /// <param name="value">The value</param>
        /// <returns>The instance of this JSONArray</returns>
        public JSONArray Add(int value)
        {
            objects.Add(new JSONLiteralNode(value.ToString()));
            return this;
        }
        /// <summary>Adds an unsigned 32 bit number to the array</summary>
        /// <param name="value">The value</param>
        /// <returns>The instance of this JSONArray</returns>
        public JSONArray Add(uint value)
        {
            objects.Add(new JSONLiteralNode(value.ToString()));
            return this;
        }
        /// <summary>Adds a 64 bit number to the array</summary>
        /// <param name="value">The value</param>
        /// <returns>The instance of this JSONArray</returns>
        public JSONArray Add(long value)
        {
            objects.Add(new JSONLiteralNode(value.ToString()));
            return this;
        }
        /// <summary>Adds an unsigned 64 bit number to the array</summary>
        /// <param name="value">The value</param>
        /// <returns>The instance of this JSONArray</returns>
        public JSONArray Add(ulong value)
        {
            objects.Add(new JSONLiteralNode(value.ToString()));
            return this;
        }
        /// <summary>Adds a single precision binary floating point number to the array</summary>
        /// <param name="value">The value</param>
        /// <returns>The instance of this JSONArray</returns>
        public JSONArray Add(float value)
        {
            objects.Add(new JSONLiteralNode(value.ToString()));
            return this;
        }
        /// <summary>Adds a double precision binary floating point number to the array</summary>
        /// <param name="value">The value</param>
        /// <returns>The instance of this JSONArray</returns>
        public JSONArray Add(double value)
        {
            objects.Add(new JSONLiteralNode(value.ToString()));
            return this;
        }

        /// <summary>
        /// Returns an enumerator to iterate over all items in the array
        /// </summary>
        /// <returns>The enumerator</returns>
        public IEnumerator<JSONObject> GetEnumerator()
        {
            return objects.GetEnumerator();
        }

        /// <inheritdoc />
        public override string ToJSONString(int depth, bool minified)
        {
            string depthString = GetDepthString(depth);
            StringBuilder builder = new StringBuilder();

            builder.Append("[");
            if (!minified)
            {
                builder.Append(Environment.NewLine);
                builder.Append(depthString);
            }
            int i = 0;
            foreach (JSONObject obj in objects)
            {
                builder.Append(obj.ToJSONString(depth + 1, minified));
                if (++i < objects.Count)
                    builder.Append(",");
                if (!minified)
                {
                    builder.Append(Environment.NewLine);
                    if (i < objects.Count)
                    {
                        builder.Append(depthString);
                    }
                }
            }
            if (!minified)
                builder.Append(GetDepthString(depth - 1));
            builder.Append("]");
            return builder.ToString();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace JSONdotNET
{
    /// <summary>
    /// Class representing a json object
    /// </summary>
    public class JSONObject {
        /// <summary>The indent sequence when converting a JSONObject to a string</summary>
        protected static string INDENT_SEQUENCE = "  ";
        protected static IFormatProvider NUMBER_FORMAT = CultureInfo.InvariantCulture;

        /// <summary>The parent node</summary>
        public JSONObject Parent { get; set; }
	    /// <summary>The key-value pairs/content of the object</summary>
	    private Dictionary<string, JSONObject> values = new Dictionary<string, JSONObject>();
	
	    public JSONObject() { }

        /// <summary>Inidicates if the JSONObject contains any values</summary>
        public virtual bool Empty { get { return values.Count == 0; } }
        /// <summary>The type of the object</summary>
        public virtual JSONObjectType Type { get { return JSONObjectType.OBJECT; } }
        /// <summary>Retreives the value for a given key from this object</summary>
        /// <param name="key">The key for the object</param>
        /// <returns>The value for the given key</returns>
        public virtual JSONObject this[string key] { get { return values[key]; } }

        /// <summary>
        /// Retrieves an array of all keys in this json object
        /// </summary>
        public virtual string[] Keys
        {
            get
            {
                string[] keys = new string[values.Count];
                values.Keys.CopyTo(keys, 0);
                return keys;
            }
        }

        /// <summary>Creates a well formed json string</summary>
        /// <returns>A well formed json string</returns>
        public override string ToString()
        {
            return ToJSONString(1, false);
        }
	
        /// <summary>Creates a well formed or minified json string</summary>
        /// <param name="minified">true if output should be minified</param>
        /// <returns>A json string</returns>
	    public virtual string ToString(bool minified) {
		    return ToJSONString(1, minified);
	    }

        /// <summary>
        /// Checks if the objects contains a pair with the given key
        /// </summary>
        /// <param name="key">The key to check</param>
        /// <returns>True if containing key</returns>
        public virtual bool ContainsKey(string key)
        {
            return values.ContainsKey(key);
        }

        /// <summary>
        /// Removes a pair by the given key
        /// </summary>
        /// <param name="key">The key for the entry to remove</param>
        public virtual void Remove(string key)
        {
            values.Remove(key);
        }

        /// <summary>Retreives the value for a given key from this object</summary>
        /// <param name="key">The key for the object</param>
        /// <returns>The value for the given key</returns>
	    public JSONObject Get(string key) {
		    return values[key];
	    }
        /// <summary>
        /// <para>Retreives the value for a given key</para>
        /// You can pass multiple keys to directly access sub-nodes
        /// </summary>
        /// <param name="keys">The keys for the node and subnodes</param>
        /// <returns>The value of the requested node</returns>
        public JSONObject Get(params string[] keys)
        {
		    JSONObject returnValue = this;
		    for (int i = 0; i < keys.Length; i++) {
			    returnValue = returnValue.Get(keys[i]);
			    if (returnValue == null) {
				    break;
			    }
		    }
		    return returnValue;
	    }

        /// <summary>
        /// <para>Tries to retreive the value for a given key</para>
        /// <para>If the key does not exist for this JSONObject, it will return false</para>
        /// And the out parameter will be null
        /// </summary>
        /// <param name="key">The key for the object</param>
        /// <param name="obj">Will give out the found JSONObject for the given key or null</param>
        /// <returns>True if the value for the given key could be found</returns>
        public bool TryGet(string key, out JSONObject obj) {
            JSONObject outObject = null;
            try
            {
                outObject = Get(key);
            }
            catch (KeyNotFoundException) { }

            obj = outObject;
            return obj != null;
	    }

        /// <summary>
        /// <para>Retreives the value as a string for a given key</para>
        /// You can pass multiple keys to directly access sub-nodes
        /// </summary>
        /// <param name="keys">The keys for the node and subnodes</param>
        /// <returns>The value of the requested node</returns>
	    public string GetString(string key) {
		    return Get(key).ToString();
	    }
        /// <summary>
        /// <para>Retreives the value as a string for a given key</para>
        /// You can pass multiple keys to directly access sub-nodes
        /// </summary>
        /// <param name="keys">The keys for the node and subnodes</param>
        /// <returns>The value of the requested node</returns>
	    public string GetString(params string[] key) {
		    JSONObject obj = Get(key);
		    if (obj != null)
			    return obj.ToString();
		    return null;
        }

        /// <summary>
        /// <para>Retreives the value as a 8 bit number for a given key</para>
        /// You can pass multiple keys to directly access sub-nodes
        /// </summary>
        /// <param name="keys">The keys for the node and subnodes</param>
        /// <returns>The value of the requested node</returns>
        public short GetByte(string key)
        {
            return Byte.Parse(GetString(key), NUMBER_FORMAT);
        }
        /// <summary>
        /// <para>Retreives the value as a 8 bit number for a given key</para>
        /// You can pass multiple keys to directly access sub-nodes
        /// </summary>
        /// <param name="keys">The keys for the node and subnodes</param>
        /// <returns>The value of the requested node</returns>
        public short GetByte(params string[] key)
        {
            return GetByte(GetString(key));
        }

        /// <summary>
        /// <para>Retreives the value as a 16 bit number for a given key</para>
        /// You can pass multiple keys to directly access sub-nodes
        /// </summary>
        /// <param name="keys">The keys for the node and subnodes</param>
        /// <returns>The value of the requested node</returns>
        public short GetShort(string key)
        {
            return Int16.Parse(GetString(key), NUMBER_FORMAT);
        }
        /// <summary>
        /// <para>Retreives the value as a 16 bit number for a given key</para>
        /// You can pass multiple keys to directly access sub-nodes
        /// </summary>
        /// <param name="keys">The keys for the node and subnodes</param>
        /// <returns>The value of the requested node</returns>
        public short GetShort(params string[] key)
        {
            return GetShort(GetString(key));
        }

        /// <summary>
        /// <para>Retreives the value as a 32 bit number for a given key</para>
        /// You can pass multiple keys to directly access sub-nodes
        /// </summary>
        /// <param name="keys">The keys for the node and subnodes</param>
        /// <returns>The value of the requested node</returns>
	    public int GetInt(string key) {
            return Int32.Parse(GetString(key), NUMBER_FORMAT);
	    }
        /// <summary>
        /// <para>Retreives the value as a 32 bit number for a given key</para>
        /// You can pass multiple keys to directly access sub-nodes
        /// </summary>
        /// <param name="keys">The keys for the node and subnodes</param>
        /// <returns>The value of the requested node</returns>
        public int GetInt(params string[] key)
        {
            return GetInt(GetString(key));
	    }

        /// <summary>
        /// <para>Retreives the value as a 64 bit number for a given key</para>
        /// You can pass multiple keys to directly access sub-nodes
        /// </summary>
        /// <param name="keys">The keys for the node and subnodes</param>
        /// <returns>The value of the requested node</returns>
	    public long GetLong(string key) {
            return Int64.Parse(GetString(key), NUMBER_FORMAT);
	    }
        /// <summary>
        /// <para>Retreives the value as a 64 bit number for a given key</para>
        /// You can pass multiple keys to directly access sub-nodes
        /// </summary>
        /// <param name="keys">The keys for the node and subnodes</param>
        /// <returns>The value of the requested node</returns>
        public long GetLong(params string[] key)
        {
            return GetLong(GetString(key));
	    }

        /// <summary>
        /// <para>Retreives the value as a single precision binary floating point number for a given key</para>
        /// You can pass multiple keys to directly access sub-nodes
        /// </summary>
        /// <param name="keys">The keys for the node and subnodes</param>
        /// <returns>The value of the requested node</returns>
	    public float GetFloat(string key) {
            return float.Parse(GetString(key), NUMBER_FORMAT);
	    }
        /// <summary>
        /// <para>Retreives the value as a single precision binary floating point number for a given key</para>
        /// You can pass multiple keys to directly access sub-nodes
        /// </summary>
        /// <param name="keys">The keys for the node and subnodes</param>
        /// <returns>The value of the requested node</returns>
        public float GetFloat(params string[] key)
        {
            return GetFloat(GetString(key));
	    }

        /// <summary>
        /// <para>Retreives the value as a double precision binary floating point number for a given key</para>
        /// You can pass multiple keys to directly access sub-nodes
        /// </summary>
        /// <param name="keys">The keys for the node and subnodes</param>
        /// <returns>The value of the requested node</returns>
	    public double GetDouble(string key) {
            return Double.Parse(GetString(key), NUMBER_FORMAT);
	    }
        /// <summary>
        /// <para>Retreives the value as a double precision binary floating point number for a given key</para>
        /// You can pass multiple keys to directly access sub-nodes
        /// </summary>
        /// <param name="keys">The keys for the node and subnodes</param>
        /// <returns>The value of the requested node</returns>
        public double GetDouble(params string[] key)
        {
		    return GetDouble(GetString(key));
	    }

        /// <summary>
        /// <para>Retreives the value as a boolean for a given key</para>
        /// You can pass multiple keys to directly access sub-nodes
        /// </summary>
        /// <param name="keys">The keys for the node and subnodes</param>
        /// <returns>The value of the requested node</returns>
	    public bool GetBool(string key) {
		    return Boolean.Parse(GetString(key));
	    }
        /// <summary>
        /// <para>Retreives the value as a boolean for a given key</para>
        /// You can pass multiple keys to directly access sub-nodes
        /// </summary>
        /// <param name="keys">The keys for the node and subnodes</param>
        /// <returns>The value of the requested node</returns>
        public bool GetBool(params string[] key)
        {
		    return Boolean.Parse(GetString(key));
	    }

        /// <summary>
        /// <para>Retreives the value as a JSONArray for a given key</para>
        /// You can pass multiple keys to directly access sub-nodes
        /// </summary>
        /// <param name="keys">The keys for the node and subnodes</param>
        /// <returns>The value of the requested node</returns>
        public JSONArray GetArray(string key)
        {
            return Get(key) as JSONArray;
        }
        /// <summary>
        /// <para>Retreives the value as a JSONArray for a given key</para>
        /// You can pass multiple keys to directly access sub-nodes
        /// </summary>
        /// <param name="keys">The keys for the node and subnodes</param>
        /// <returns>The value of the requested node</returns>
        public JSONArray GetArray(params string[] key)
        {
            return Get(key) as JSONArray;
        }

        /// <summary>Adds a JSONObject with the given key to the object/node</summary>
        /// <param name="key">The key for the value</param>
        /// <param name="value">The instance of the JSONObject</param>
        /// <returns>The current JSONObject instance</returns>
	    public JSONObject Add(string key, JSONObject value) {
            if (values.ContainsKey(key))
                values[key] = value;
            else
		        values.Add(key, value);
		    return this;
	    }
        /// <summary>
        /// <para>Adds a string with the given key to the object/node</para>
        /// Can be null
        /// </summary>
        /// <param name="key">The key for the value</param>
        /// <param name="value">The value</param>
        /// <returns>The current JSONObject instance</returns>
	    public JSONObject Add(string key, string value) {
		    if (value == null)
			    return Add(key, new JSONLiteralNode("null"));
		    return Add(key, new JSONTextNode(value));
	    }
        /// <summary>Adds a boolean with the given key to the object/node</summary>
        /// <param name="key">The key for the value</param>
        /// <param name="value">The value</param>
        /// <returns>The current JSONObject instance</returns>
	    public JSONObject Add(string key, bool value) {
		    return Add(key, new JSONLiteralNode(value.ToString().ToLower()));
	    }
        /// <summary>Adds a 8 bit number with the given key to the object/node</summary>
        /// <param name="key">The key for the value</param>
        /// <param name="value">The value</param>
        /// <returns>The current JSONObject instance</returns>
        public JSONObject Add(string key, byte value)
        {
            return Add(key, new JSONLiteralNode(value.ToString()));
        }
        /// <summary>Adds a 16 bit number with the given key to the object/node</summary>
        /// <param name="key">The key for the value</param>
        /// <param name="value">The value</param>
        /// <returns>The current JSONObject instance</returns>
        public JSONObject Add(string key, short value)
        {
            return Add(key, new JSONLiteralNode(value.ToString()));
        }
        /// <summary>Adds an unsigned 16 bit number with the given key to the object/node</summary>
        /// <param name="key">The key for the value</param>
        /// <param name="value">The value</param>
        /// <returns>The current JSONObject instance</returns>
        public JSONObject Add(string key, ushort value)
        {
            return Add(key, new JSONLiteralNode(value.ToString()));
        }
        /// <summary>Adds a 32 bit number with the given key to the object/node</summary>
        /// <param name="key">The key for the value</param>
        /// <param name="value">The value</param>
        /// <returns>The current JSONObject instance</returns>
	    public JSONObject Add(string key, int value) {
            return Add(key, new JSONLiteralNode(value.ToString()));
	    }
        /// <summary>Adds an unsigned 32 bit number with the given key to the object/node</summary>
        /// <param name="key">The key for the value</param>
        /// <param name="value">The value</param>
        /// <returns>The current JSONObject instance</returns>
        public JSONObject Add(string key, uint value)
        {
            return Add(key, new JSONLiteralNode(value.ToString()));
        }
        /// <summary>Adds a 64 bit number with the given key to the object/node</summary>
        /// <param name="key">The key for the value</param>
        /// <param name="value">The value</param>
        /// <returns>The current JSONObject instance</returns>
	    public JSONObject Add(string key, long value) {
            return Add(key, new JSONLiteralNode(value.ToString()));
	    }
        /// <summary>Adds an unsigned 64 bit number with the given key to the object/node</summary>
        /// <param name="key">The key for the value</param>
        /// <param name="value">The value</param>
        /// <returns>The current JSONObject instance</returns>
        public JSONObject Add(string key, ulong value)
        {
            return Add(key, new JSONLiteralNode(value.ToString()));
	    }
        /// <summary>Adds a double precision binary floating point number with the given key to the object/node</summary>
        /// <param name="key">The key for the value</param>
        /// <param name="value">The value</param>
        /// <returns>The current JSONObject instance</returns>
	    public JSONObject Add(string key, double value) {
            return Add(key, new JSONLiteralNode(value.ToString()));
	    }
        /// <summary>Adds a single precision binary floating point number with the given key to the object/node</summary>
        /// <param name="key">The key for the value</param>
        /// <param name="value">The value</param>
        /// <returns>The current JSONObject instance</returns>
	    public JSONObject Add(string key, float value) {
		    return Add(key, new JSONLiteralNode(value.ToString()));
	    }
	
        /// <summary>Creates a well formed or minified JSON string</summary>
        /// <param name="depth">The base indent depth</param>
        /// <param name="minified">Indicates if the document should be minified</param>
        /// <returns>A valid json document</returns>
	    public virtual string ToJSONString(int depth, bool minified) {
		    string depthString = GetDepthString(depth);
		    StringBuilder builder = new StringBuilder();
		
		    builder.Append("{");
		    if (!minified) {
			    builder.Append(Environment.NewLine);
			    builder.Append(depthString);
		    }
		    IEnumerator<string> keys = values.Keys.GetEnumerator();
		    int i = 0;
		    while (keys.MoveNext()) {
			    string key = keys.Current;
			    builder.Append(String.Format("\"{0}\": ", ToValidJSONString(key)));
			    builder.Append(values[key].ToJSONString(depth + 1, minified));
			    if (++i < values.Count)
				    builder.Append(",");
			    if (!minified) {
				    builder.Append(Environment.NewLine);
				    if (i < values.Count) {
					    builder.Append(depthString);
				    }
			    }
		    }
		    if (!minified)
			    builder.Append(GetDepthString(depth - 1));
		    builder.Append("}");
		    return builder.ToString();
	    }
	
        /// <summary>Returns the indent string for a given depth</summary>
        /// <param name="depth">The indent depth</param>
        /// <returns>The indent string for the given depth</returns>
	    protected string GetDepthString(int depth) {
		    StringBuilder depthString = new StringBuilder();
		    for (int i = 0; i < depth; i++) {
			    depthString.Append(INDENT_SEQUENCE);
		    }
		    return depthString.ToString();
	    }
	
        /// <summary>Fixes a string if it is not a valid json string</summary>
        /// <param name="str">The string to be fixed</param>
        /// <returns>The fixed string</returns>
	    protected string ToValidJSONString(string str) {
		    if (str == null)
			    return str;
            return str.Replace("\\", "\\\\").Replace("\"", "\\\"");
	    }
    }
}

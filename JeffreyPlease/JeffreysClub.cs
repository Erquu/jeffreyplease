﻿//#define NOHOOK_NOAPPLOAD

using JeffreyPlease.Logic;
using JeffreyPlease.Logic.Apps.SearchContexts;
using JeffreyPlease.Logic.Loaders;
using JeffreyPlease.Logic.Search;
using JeffreyPlease.Plugin;
using JeffreyPlease.Properties;
using JeffreyPlease.Settings;
using JeffreyPlease.UI;
using JeffreyPlease.UI.Themes;
using JeffreyPlease.Utils;
using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using Utilities;

namespace JeffreyPlease
{
    public delegate void LoadEventHandler();

    /// <summary>
    /// <para>Form managing the the input form, the tray</para>
    /// and all the positioning stuff
    /// </summary>
    public class JeffreysClub
    {
        private delegate void VoidCallback(SplashScreen sp);

        private NotifyIcon notifyJeffrey;
        private MenuItem callItem,
            settingsItem,
            closeItem;

        private InputForm inputForm;

        /// <summary>Instance of StatisticsManager</summary>
        private StatisticsManager statisticsManager;
        /// <summary>Manages the key strokes to open "Jeffrey"</summary>
        private KeySequence keySeq;
        private GlobalKeyboardHook hook;
        private InterceptMouse mouseHook;

        private bool running = false;
        private bool eastereggActive = false;

        public event LoadEventHandler Load;

        /// <summary>The constructor (as if you wouldn't know)</summary>
        public JeffreysClub()
        {
        }

        public void Start()
        {
            this.notifyJeffrey = new NotifyIcon();
            this.notifyJeffrey.Icon = Resources.JeffreyPlease_64;
            this.notifyJeffrey.Text = "JeffreyPlease" + Environment.NewLine + "Right click";
            this.notifyJeffrey.Visible = true;

            SplashScreen splashScreen = new SplashScreen();
            splashScreen.UpdateStatus("Initialize");
            splashScreen.Show();
            splashScreen.BringToFront();

            Thread loadingThread = new Thread(new ParameterizedThreadStart(Loading));
            loadingThread.Name = "Jeffreys Personal Loading Thread";
            loadingThread.Start(splashScreen);

            this.running = true;
        }

        public void Stop()
        {
            if (this.running)
            {
                this.running = false;

                this.notifyJeffrey.Visible = false;

                // Reset all hooks and unload program
                hook.unhook();
                mouseHook.Unhook();

                SettingsManager manager = Store.Get<SettingsManager>();
                if (manager != null)
                    manager.Save();

                AppStore store = Store.Get<AppStore>();
                if (store != null)
                    store.QuitClean();
            }
        }

        /// <summary>
        /// Checks if the given coordinate is inside the (open) input form
        /// </summary>
        /// <param name="x">The x coordinate</param>
        /// <param name="y">The y coordinate</param>
        public bool CheckBounds(int x, int y)
        {
            return !this.inputForm.ClientRectangle.Contains(x - this.inputForm.Left, y - this.inputForm.Top);
        }

        /// <summary>Shows and focuses the input form</summary>
        public void CallJeffrey()
        {
            if (!Store.Get<ThemeManager>().IsLiveStyling)
            {
                Screen workingScreen = Screen.FromPoint(Tools.GetMousePosition());
                this.inputForm.Location = new Point(
                    workingScreen.Bounds.X + (int)(((double)workingScreen.Bounds.Width * .5d) - ((double)this.inputForm.ClientSize.Width * .5d)),
                    200);
            }

            if (!this.inputForm.Visible)
            {
                this.inputForm.CallJeffrey();
            }
        }

        public void HideJeffrey()
        {
            this.inputForm.HideJeffrey();
        }

        public bool ToggleJeffrey()
        {
            if (this.inputForm.Visible)
            {
                this.HideJeffrey();
                return false;
            }
            else
            {
                this.CallJeffrey();
                return true;
            }
        }

        private ContextMenu CreateContextMenu()
        {
            callItem = new MenuItem("Call Jeffrey", HandleMenuClick);
            settingsItem = new MenuItem("Show Settings", HandleMenuClick);
            closeItem = new MenuItem("Close", HandleMenuClick);

            ContextMenu contextMenu = new ContextMenu();
            contextMenu.MenuItems.Add(callItem);
            contextMenu.MenuItems.Add(settingsItem);
            contextMenu.MenuItems.Add("-"); // Seperator
            contextMenu.MenuItems.Add(closeItem);

            return contextMenu;
        }

        private void HandleMenuClick(object sender, EventArgs args)
        {
            if (sender == callItem)
            {
                this.CallJeffrey();
            }
            else if (sender == settingsItem)
            {
                (new SettingsView()).ShowDialog();
            }
            else if (sender == closeItem)
            {
                Application.Exit();
            }
        }

        private void notifyJeffrey_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
                this.CallJeffrey();
        }

        private void Loading(object splashScreen)
        {
            SplashScreen sp = (SplashScreen)splashScreen;

            sp.UpdateStatus("Initializing Search Engine");

            // Setup Search Engine Options and Manager
            SearchOptions customOptions = SearchOptions.DEFAULT;
            ISearchContext.SearchOptions = customOptions;
            Store.Put<SearchEngineManager>(new SearchEngineManager());

            sp.UpdateStatus("Loading Settings");

            // Create SettingsManager and load settings (from file)
            SettingsManager settingsManager = Store.Put<SettingsManager>(new SettingsManager());
            settingsManager.Load();
            
            sp.UpdateStatus("Loading Themes");

            ThemeManager themeManager = Store.Put<ThemeManager>(new ThemeManager(settingsManager.ThemesDir));

            ErrorLog error = Store.Put<ErrorLog>(new ErrorLog());

            sp.UpdateStatus("Searching for programs");

            // Setup store and Contextmanager
            AppStore store = Store.Put<AppStore>(new AppStore());
            Store.Put<ContextManager>(new ContextManager(store));

            // Load Apps and create index
            AppLoaderConfig appLoaderConfig = AppLoaderConfig.CreateConfig(settingsManager);
            AppLoader loader = new AppLoader(store, appLoaderConfig);
#if !NOHOOK_NOAPPLOAD
            loader.Load();
#endif

            // Load statistics and user related data
            statisticsManager = Store.Put<StatisticsManager>(new StatisticsManager(settingsManager));
            
            sp.UpdateStatus("Loading Plugins");

            // Initialize Pluginsystem and load plugins
            PluginLoader pluginLoader = Store.Put<PluginLoader>(new PluginLoader());
            pluginLoader.Load(false);

            settingsManager.Post_Load();

            sp.UpdateStatus("Loading UI");

            // Load the input form so it is loaded
            // when the keyboard hook becomes active
            this.inputForm = new InputForm();
            FormInvoker.inputForm = this.inputForm;

            GC.Collect();

            VoidCallback c = new VoidCallback(LoadFinish);
            if (sp.InvokeRequired)
            {
                sp.Invoke(c, sp);
            }
            else
            {
                c.Invoke(sp);
            }
        }

        private void LoadFinish(SplashScreen splashScreen)
        {
            splashScreen.UpdateStatus("Registering shortcut");

            SettingsManager settingsManager = Store.Get<SettingsManager>();

            #region KeyboardHook
            
#if !NOHOOK_NOAPPLOAD

            // Initialize the global key and mouse hook and register the opening-keysequence
            this.hook = Store.Put<GlobalKeyboardHook>(new GlobalKeyboardHook());
            this.keySeq = Store.Put<KeySequence>(new KeySequence(this.hook, settingsManager));

            this.hook.KeyDown += new KeyEventHandler(this.hook_KeyDown);
            this.hook.KeyUp += new KeyEventHandler(this.hook_KeyUp);

            this.mouseHook = new InterceptMouse();
            this.mouseHook.MouseClick += new InterceptMouse.MouseClickCallback(this.mouseHook_MouseClick);

            this.mouseHook.Hook();
            this.hook.hook();

#endif
            
            #endregion

            this.notifyJeffrey.Icon = Resources.JeffreyPlease_white_stroked_64;
            this.notifyJeffrey.ContextMenu = CreateContextMenu();

            if (Load != null)
                Load.Invoke();

            splashScreen.Hide();
            splashScreen.Dispose();
            splashScreen = null;

            Notification.Show(Resources.JeffreyPlease_white_stroked_64, "Jeffrey is ready to serve you", "Press Alt+Space to open", 3000);
        }

        /// <summary>
        /// If the input mask is visible, checks if a mouseclick
        /// was made outside of it, if so, closes the mask
        /// </summary>
        /// <param name="x">Horizontal Mouse Position</param>
        /// <param name="y">Vertical Mouse Position</param>
        private void mouseHook_MouseClick(int x, int y)
        {
            if (this.CheckBounds(x, y))
                this.HideJeffrey();
        }

        /// <summary>
        /// Adds the pressed key from the keysequence watcher and
        /// checks if the keysequence was pressed
        /// </summary>
        /// <param name="sender">Not needed</param>
        /// <param name="e">Key event to register the pressed key</param>
        private void hook_KeyDown(object sender, KeyEventArgs e)
        {
            if (keySeq.KeyDown(e.KeyCode))
            {
                if (this.ToggleJeffrey())
                {
                    statisticsManager.AddOpen();
                }
                e.Handled = true;
            }
        }

        /// <summary>
        /// Remove the released key from the keysequence watcher
        /// </summary>
        /// <param name="sender">Not needed</param>
        /// <param name="e">Key event to unregister the pressed key</param>
        private void hook_KeyUp(object sender, KeyEventArgs e)
        {
            keySeq.KeyUp(e.KeyCode);
        }
    }
}

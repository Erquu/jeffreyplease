﻿using JeffreyPlease.Plugin;
using JSONdotNET;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JeffreyPluginify
{
    internal class PlFile
    {
        public FileInfo File { get; set; }
        public JeffreyExtension Extension { get; set; }
        public JSONObject settings { get; set; }

        public string Path
        {
            get
            {
                return this.File.FullName;
            }
            set
            {
                FileInfo f = new FileInfo(value);
                if (f.Exists)
                {
                    this.File = f;
                }
            }
        }

        public override string ToString()
        {
            return this.File.Name;
        }
    }
}

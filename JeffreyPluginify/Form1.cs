﻿using JeffreyPlease;
using JeffreyPlease.Logic;
using JeffreyPlease.Plugin;
using JeffreyPlease.Plugin.Container;
using JeffreyPlease.Settings;
using JSONdotNET;
using System;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace JeffreyPluginify
{
    public partial class Form1 : Form
    {
        private SettingsManager settings;
        private PluginLoader loader;
        private JSONObject config;
        private string pluginName = "Unnamed_Plugin";
        private PlFile selectedFile = null;

        public Form1()
        {
            InitializeComponent();

            AppStore store = Store.Put<AppStore>(new AppStore());
            Store.Put<ContextManager>(new ContextManager(store));

            settings = Store.Put<SettingsManager>(new SettingsManager());
            settings.Load();

            Store.Put<ErrorLog>(new ErrorLog());

            loader = new PluginLoader();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Multiselect = true;
            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                foreach (string fileName in ofd.FileNames)
                {
                    PlFile f = new PlFile();
                    f.Path = fileName;
                    if (LoadFile(f))
                    {
                        checkedListBox1.Items.Add(f, true);
                    }
                }
            }
        }

        private bool LoadFile(PlFile file)
        {
            JeffreyExtension ext = loader.LoadDll(file.File);

            if (ext != null)
            {
                file.Extension = ext;
                file.settings = (new JSONObject()).Add(ext.Name, settings.GetSetting(ext.Name));
                pluginName = ext.Name;
            }
            else
            {
                file.Extension = null;
            }

            return true;
        }

        private void btnConvert_Click(object sender, EventArgs e)
        {
            config = new JSONObject();
            config.Add("config", new JSONArray());
            JSONArray autoLoad = new JSONArray();

            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                FileInfo saveFile = null;
                int i = 0;
                do
                {
                    saveFile = new FileInfo(Path.Combine(fbd.SelectedPath, String.Format("{0}{1}.jeffreyplugin", pluginName, i++ == 0 ? "" : "_" + i.ToString())));
                } while (saveFile.Exists);

                PluginContainer plugin = new PluginContainer(saveFile);
                plugin.Open();

                foreach (object obj in checkedListBox1.Items)
                {
                    PlFile file = (PlFile)obj;
                    if (checkedListBox1.CheckedItems.Contains(obj))
                    {
                        autoLoad.Add(file.File.Name);
                    }
                    plugin.AddFile(file.File.Name, file.File);

                    if (file.settings != null)
                        config.GetArray("config").Add(file.settings);
                }

                if (autoLoad.Length > 0)
                {
                    config.Add("autoload", autoLoad);
                }

                plugin.AddFile("mf", new MemoryStream(Encoding.Default.GetBytes(config.ToString(true))));

                plugin.Close();
            }
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedFile = (PlFile)checkedListBox1.SelectedItem;

            if (selectedFile != null && selectedFile.settings != null)
                richTextBox1.Text = selectedFile.settings.ToString(false);
            else
                richTextBox1.Text = "";
        }

        private void richTextBox1_Leave(object sender, EventArgs e)
        {
            if (richTextBox1.Text.Trim().Length > 0)
                selectedFile.settings = JSON.Parse(richTextBox1.Text);
            else
                selectedFile.settings = null;
        }
    }
}
